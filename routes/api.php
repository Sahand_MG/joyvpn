<?php

use App\Http\Controllers\AffiliateController;
use App\Http\Controllers\NotifController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('send-notif-exp', [NotifController::class, 'sendNotifExp']);
Route::post('send-notif-vol', [NotifController::class, 'sendNotifVol']);
Route::post('send-notif-payment', [NotifController::class, 'sendNotifPayment']);
//Route::post('send-extra-inbounds', [NotifController::class, 'sendNotifExtraInbounds']);
Route::post('send-server-status', [NotifController::class, 'sendServerStatusNotif']);
Route::post('send-server-introduce', [NotifController::class, 'sendServerIntroduceNotif']);
Route::get('send-account-registration', [NotifController::class, 'accountRegistrationNotif']);
Route::post('send-group-message', [NotifController::class, 'sendGroupMessage']);
Route::post('send-private-message', [NotifController::class, 'sendPrivateMessage']);
Route::post('report-issue', [NotifController::class, 'reportIssue']);
Route::post('referral-credit', [AffiliateController::class, 'referralCredit']);
Route::get('check-master', [AffiliateController::class, 'checkMaster']);
