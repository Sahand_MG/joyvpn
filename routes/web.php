<?php

use App\Classes\AdminNotifier;
use App\Classes\Constants;
use App\Classes\ReceiptMaker;
use App\Classes\RemarkDB;
use App\Http\Controllers\ChannelController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\PaymentController;
use App\Jobs\SendTelegramNotif;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Morilog\Jalali\Jalalian;
use Symfony\Component\Console\Output\BufferedOutput;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('tg/webhook', [MainController::class, 'AcceptTelegramMessageAction'])->name('webhook');
Route::get('payment/callback', [PaymentController::class, 'paymentCallback'])->name('paymentCallback');
Route::get('payment/wallet/callback', [PaymentController::class, 'paymentWalletCallback'])->name('paymentWalletCallback');
Route::get('payment/renew/callback', [PaymentController::class, 'paymentRenewCallback'])->name('paymentRenewCallback');
Route::any('payment/renew/naps/zarin/callback', [PaymentController::class, 'paymentRenewNapsternZarinCallback'])->name('paymentRenewNapsternZarinCallback');
Route::any('payment/renew/naps/paystar/callback', [PaymentController::class, 'paymentRenewNapsternPaystarCallback'])->name('paymentRenewNapsternPaystartCallback');
Route::get('payment/success/{transid}', [PaymentController::class, 'successPayment'])->name('RemotePaymentSuccess');
Route::get('payment/canceled/{transid}', [PaymentController::class, 'FailedPayment'])->name('RemotePaymentCanceled');
Route::get('channel/msg', [ChannelController::class, 'PostChannelAction']);
Route::get('get-sub-qr/{id}', function($id) {
    return response()->file(storage_path('app/qr/'.$id.'.png'));
})->name('sub.getUrl');

Route::get('t', function () {
    $trans = Transaction::query()->find(24547);
    \App\Services\AffiliateService::addAffiliateCredit($trans);
//    $trans = Transaction::find(19642);
//    $expiry_date = Jalalian::now();
//    $date = Jalalian::now();
//    $receipt = (new ReceiptMaker($trans->trans_id, $trans->amount, $date, $trans->remark, $expiry_date, 60, $trans->status))
//        ->makeForUser()
//        ->addExpDate()
//        ->addStatus()
//        ->addRemark()
//        ->addTotalVol()
//        ->getMsg();
//    $msg = [
//        'chat_id' => $trans->chat_id,
//        'text' => $receipt,
//        'parse_mode' => 'html',
//    ];
//    (SendTelegramNotif::dispatch($msg));
//    AdminNotifier::orderAcceptedRenew($trans);
});
