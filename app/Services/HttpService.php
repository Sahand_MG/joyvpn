<?php

namespace App\Services;

use App\Jobs\SendTelegramNotif;
use Illuminate\Support\Facades\Log;

class HttpService
{
    protected static $counter = 0;

    public static function sendHttp($method = 'GET', $url, array $data = [], array $headers = [])
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
        ));
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            info(json_encode($response));
            info(curl_error($curl));
            if (self::$counter < 3 ) {
                sleep(1);
                self::$counter += 1;
                self::sendHttp($method, $url, $data, $headers);
            } else {
                self::_sendTgNotif();
                exit();
            }
        }
        curl_close($curl);
        Log::info($response);
        return json_decode($response, true);
    }

    private static function _sendTgNotif()
    {
        $msg = [
            'chat_id' => app('extractor')->chat_id,
            'text' => 'خطای node',
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }

}