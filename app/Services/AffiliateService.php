<?php

namespace App\Services;

use App\Classes\PlusDB;
use App\Classes\UserDB;
use App\Classes\WalletDB;
use App\Jobs\SendTelegramNotif;
use Illuminate\Support\Facades\DB;

class AffiliateService
{
    public static function addAffiliateCredit($trans)
    {
        $aff = PlusDB::getAffiliate($trans->remark, $trans->chat_id);
        if (!is_null($aff)) {
            info('Adding aff credit to '. $aff->master_username);
            $w = WalletDB::getUserWallet($aff->master_username);
            $credit = $aff->ratio / 100 * $trans->amount;
            if (!is_null($w)) {
                $new = $w->amount + $credit;
                $w->update(['amount' =>  $new]);
            } else {
                $w = WalletDB::createWalletRecord(UserDB::getUserByChatId($aff->master_username));
                $new = $w->amount + $credit;
                $w->update(['amount' =>  $new]);
            }
            $msg = [
                'chat_id' => $aff->master_username,
                'text' => "🔥مبلغ $credit تومان به کیف پول شما واریز شد. موجودی جدید: ".$new,
                'parse_mode' => 'html',
            ];
            SendTelegramNotif::dispatch($msg);
        } else {
//            info("account ". $trans->remark. ' belongs to a same person '. $aff->master_username);
        }
    }

    public static function addAffiliateCreditByAdmin($remark, $amount)
    {
        $aff = DB::connection('mysql_plus')
            ->table('affiliates')
            ->where('referral_username', $remark)
            ->first();
        if (!is_null($aff)) {
            info('Adding aff credit to '. $aff->master_username);
            $w = WalletDB::getUserWallet($aff->master_username);
            $credit = $aff->ratio / 100 * $amount;
            if (!is_null($w)) {
                $new = $w->amount + $credit;
                $w->update(['amount' =>  $new]);
            } else {
                $w = WalletDB::createWalletRecord(UserDB::getUserByChatId($aff->master_username));
                $new = $w->amount + $credit;
                $w->update(['amount' =>  $new]);
            }
            $msg = [
                'chat_id' => $aff->master_username,
                'text' => "🔥مبلغ $credit تومان به کیف پول شما واریز شد. موجودی جدید: ".$new,
                'parse_mode' => 'html',
            ];
            SendTelegramNotif::dispatch($msg);
        } else {
            info("account ". $remark. ' belongs to a same person '. $aff->master_username);
        }
    }
}
