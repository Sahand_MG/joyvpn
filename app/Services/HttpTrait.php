<?php


namespace App\Services;


use App\Jobs\SendTelegramNotif;

trait HttpTrait
{
    protected  $counter = 0;

    public function sendHttp($server_address, array $data)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $server_address,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ));
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            info(json_encode($response));
            if ($this->counter < 3 ) {
                sleep(1);
                info('connection failed. trying again ...'.$this->counter);
                $this->counter += 1;
                $this->sendHttp($server_address, $data);
            } else {
                $this->msg['text'] = 'خطای node';
                SendTelegramNotif::dispatch($this->msg);
                exit();
            }
        }
        curl_close($curl);
        info(json_encode($response));
        return json_decode($response);
    }
}