<?php


namespace App\Services;


use App\Classes\Constants;
use App\Models\Plan;

class GeneralService
{
    public static function iosPlanPriceCalcByVol($vol)
    {
        if ($vol < 65 && $vol >= 60) {
            $amount = Plan::query()->where('name', '1MIP60GB')->first()->price;
        } elseif ($vol < 35 && $vol >= 30) {
            $amount = Plan::query()->where('name', '1MIP30GB')->first()->price;
        } elseif ($vol < 15 && $vol >= 10) {
            $amount = Plan::query()->where('name', '1MIP10GB')->first()->price;
        } else {
            $amount = Plan::query()->where('name', '1MIP100GB')->first()->price;
        }
        return $amount;
    }

    public static function androidPlanPriceCalcByVol($vol)
    {
        if ($vol < 65 && $vol >= 60) {
            $amount = Plan::query()->where('name', '1MAND60GB')->first()->price;
        } elseif ($vol < 35 && $vol >= 30) {
            $amount = Plan::query()->where('name', '1MAND30GB')->first()->price;
        } elseif ($vol < 15 && $vol >= 10) {
            $amount = Plan::query()->where('name', '1MAND10GB')->first()->price;
        } else {
            $amount = Plan::query()->where('name', '1MAND100GB')->first()->price;
        }
        return $amount;
    }

    public static function planPriceCalcByVolAndOs($vol, $os)
    {
        if ($vol < 65 && $vol >= 60) {
            $amount = $os == Constants::ANDROID
                ? Plan::query()->where('name', '1MAND60GB')->first()->price
                : Plan::query()->where('name', '1MIP60GB')->first()->price;
        } elseif ($vol < 35 && $vol >= 30) {
            $amount = $os == Constants::ANDROID
                ? Plan::query()->where('name', '1MAND30GB')->first()->price
                : Plan::query()->where('name', '1MIP30GB')->first()->price;
        } elseif ($vol < 15 && $vol >= 10) {
            $amount = $os == Constants::ANDROID
                ? Plan::query()->where('name', '1MAND10GB')->first()->price
                : Plan::query()->where('name', '1MIP10GB')->first()->price;
        } else {
            $amount = $os == Constants::ANDROID
                ? Plan::query()->where('name', '1MAND100GB')->first()->price
                : Plan::query()->where('name', '1MIP100GB')->first()->price;
        }
        return $amount;
    }
}