<?php

namespace App\Services;


use App\Classes\Constants;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Morilog\Jalali\Jalalian;

trait HasVnode
{
    public $fqdn;
    public $node;

    public function getFqdn($remark)
    {
        if (str_contains($remark, 'ir')) {
            $this->fqdn = '0.0.0.0';
            return $this->fqdn;
        }elseif (str_contains($remark, 'ip')) {
            $this->fqdn = '1.1.1.1';
            return $this->fqdn;
        } elseif (str_contains($remark, 'br')) {
            $fqdn = $this->_extractServerAddress($remark, 'br');
            $this->fqdn = $fqdn;
            return $this->fqdn;
        } elseif (str_contains($remark, 'no')) {
            $fqdn = $this->_extractServerAddress($remark, 'no');
            $this->fqdn = $fqdn;
            return $this->fqdn;
        } elseif (str_contains($remark, 's')) {
            $fqdn = $this->_extractServerAddress($remark, 's');
            $this->fqdn = $fqdn;
            return $this->fqdn;
        } elseif (str_contains($remark, 'm')) {
            $fqdn = $this->_extractServerAddress($remark, 'm');
            $this->fqdn = $fqdn;
            return $this->fqdn;
        } elseif (str_contains($remark, 'fe')) {
            $fqdn = $this->_extractServerAddress2($remark, 'fe');
            $this->fqdn = $fqdn;
            return $this->fqdn;
        } elseif (str_contains($remark, 'cu')) {
            $fqdn = $this->_extractServerAddress2($remark, 'cu');
            $this->fqdn = $fqdn;
            return $this->fqdn;
        } else {
            Log::info('Account not found');
        }
    }

    public function getNodeServer($remark)
    {
        return $this->_getDataFromRemoteServer($this->fqdn, $remark);
    }

    private function _extractServerAddress($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        $server_id = $matches[0][0];
        $user_id = $matches[0][1];
        try {
            $fqdn = Constants::IRAN_SERVERS[$server_identifier . $server_id];
        } catch (\Exception $exception) {
            Log::info($exception->getMessage());
        }
        return $fqdn;
    }

    private function _extractServerAddress2($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        $server_id = $matches[0][0];
        try {
            $fqdn = Constants::IRAN_SERVERS[$server_identifier];
        } catch (\Exception $exception) {
            Log::info($exception->getMessage());
        }
        return $fqdn;
    }

    private function _getDataFromRemoteServer($fqdn, $remark)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/inbound' : $fqdn . '/api/inbound';
        $resp = Http::sendHttp($address, ['remark' => $remark]);
        info(json_encode($resp));
        if (is_null($resp) || $resp->status != 200) {
            Log::info('Account not found');
        }
        $user_data = $resp->data;
        $usage = $user_data->up + $user_data->down;
        $tmp = $usage;
        $gb = pow(10, 9);
        $mb = pow(10, 6);
        $kb = pow(10, 3);
        if ($usage < 1000) {
            $unit = 'B';
        }
        if ($usage > $kb) {
            $tmp = $usage / $kb;
            $unit = 'KB';
        }
        if ($usage > $mb) {
            $tmp = $usage / $mb;
            $unit = 'MB';
        }
        if ($usage > $gb) {
            $tmp = $usage / $gb;
            $unit = 'GB';
        }
        $usage = $tmp;
        if ($user_data->expiry_time == 0) {
            $exp = 'ندارد';
        } else {
            $exp = Jalalian::fromCarbon(Carbon::createFromTimestampMs($user_data->expiry_time))->format('d-m-Y');
        }
        $enable = $user_data->enable;
        $total = $user_data->total / (1000 * pow(10, 6));
        $stream = json_decode($user_data->stream_settings);
        return compact('usage', 'unit', 'exp', 'enable', 'total', 'remark', 'stream');
    }

    public function sendDisconnectRequest($fqdn, $remark)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/inbound/disconnect' : $fqdn . '/api/inbound/disconnect';
        $resp = HttpService::sendHttp('POST', $address, ['remark' => $remark]);
        return $resp;
    }

    public function sendReconnectRequest($fqdn, $remark)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/inbound/reconnect' : $fqdn . '/api/inbound/reconnect';
        $resp = HttpService::sendHttp('POST', $address, ['remark' => $remark]);
        return $resp;
    }
}