<?php

namespace App\Http\Middleware;

use App\Classes\Constants;
use App\Jobs\SendTelegramNotif;
use App\Models\User;
use App\Services\HttpService;
use Closure;
use Illuminate\Http\Request;

class ChannelSubscriptionMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        try {
            $user_id = app('extractor')->chat_id;
            $data = app('extractor');
            $user = User::where('chat_id', $user_id)->first();
            $this->_saveNewUserData($user, $data);
            $url = 'https://api.telegram.org/bot' . env('BOT_TOKEN') . '/getChatMember?chat_id=@joyvpn&user_id=' . $user_id;
            $resp = HttpService::sendHttp('POST', $url);
            $admins = config('bot.admin_ids');
            if (in_array($user_id, $admins)) {
                return $next($request);
            }
            if (!$resp['ok'] || $resp['result']['status'] != 'member') {
                $keyboard = [
                    [app('bot')->buildInlineKeyBoardButton('عضویت در کانال', 'https://t.me/joyvpn')],
                    [app('bot')->buildInlineKeyBoardButton('عضو شدم', '', Constants::RESTART)],
                ];
                $msg = [
                    'chat_id' => $user_id,
                    'text' => 'برای استفاده از امکانات ربات، ابتدا در کانال ما عضو شوید. پس از عضویت روی دکمه عضو شدم بزنید',
                    'reply_markup' => app('bot')->buildInlineKeyBoard($keyboard)
                ];
                SendTelegramNotif::dispatch($msg);
                exit();
            }
            return $next($request);
        }catch (\Exception $e) {
            info($e->getMessage().' '.$e->getFile().' @'. $e->getLine());
            exit();
        }
    }

    private function _saveNewUserData($user, $data):void
    {
        if (is_null($user)) {
            $user = new User();
            $user->name = $data->fname ?? 'no fname';
            $user->username = $data->username ?? 'no username';
            $user->chat_id = $data->chat_id;
            $user->save();
        }
    }
}
