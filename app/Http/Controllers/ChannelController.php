<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\Components\Channel\PostChannelAction;
use Illuminate\Http\Request;

class ChannelController extends Controller
{
    public function PostChannelAction()
    {
        return app()->make(PostChannelAction::class)->execute();
    }
}
