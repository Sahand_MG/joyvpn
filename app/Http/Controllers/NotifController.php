<?php

namespace App\Http\Controllers;

use App\Classes\AdminNotifier;
use App\Classes\CacheDB;
use App\Classes\Constants;
use App\Classes\RemarkDB;
use App\Jobs\SendBulkNotif;
use App\Jobs\SendTelegramNotif;
use App\Models\Remark;
use App\Models\User;
use App\Services\HttpService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Morilog\Jalali\Jalalian;

class NotifController extends Controller
{
    public function sendNotifExp(Request $request)
    {
        $data = $_POST;
        if (end($data) == 'Hey Delain!') {
            unset($data[count($data) - 1]);
            info('EXP Req: ');
            info($data);
            foreach ($data as $remark) {
                $record = DB::table('remarks')->where('remark', $remark)->first();
                if (!is_null($record)) {
                    $msg = [
                        'chat_id' => $record->chat_id,
                        'text' => ' تنها ۱ روز به انقضای ' . $remark . ' باقی مانده است ',
                        'parse_mode' => 'HTML',
                    ];
                    $msg2 = [
                        'chat_id' => Constants::ADMIN_CHAT_ID,
                        'text' => 'Account ' . $remark . ' reminded ',
                        'parse_mode' => 'HTML',
                    ];
                    SendTelegramNotif::dispatch($msg);
                    SendTelegramNotif::dispatch($msg2);
                }
            }

        }
    }

    public function sendNotifVol(Request $request)
    {
        $data = $_POST;
        if (end($data) == 'Hey Delain!') {
            unset($data[count($data) - 1]);
            info('Vol Req: ');
            info($data);
            foreach ($data as $remark) {
                $record = DB::table('remarks')->where('remark', $remark)->first();
                if (!is_null($record)) {
                    $msg = [
                        'chat_id' => $record->chat_id,
                        'text' => ' شما ۹۵ درصد از حجم اکانت ' . $remark . ' را مصرف کرده‌اید ',
                        'parse_mode' => 'HTML',
                    ];
                    $msg2 = [
                        'chat_id' => Constants::ADMIN_CHAT_ID,
                        'text' => 'Vol ' . $remark . ' reminded ',
                    ];
                    SendTelegramNotif::dispatch($msg);
                    SendTelegramNotif::dispatch($msg2);
                }
            }

        }
    }

    public function sendNotifExtraInbounds()
    {
        $data = $_POST;
        if (end($data) == 'Hey Delain!') {
            unset($data[0]);
            info('Extra Req: ');
            info($data);
            $string = '';
            foreach ($data as $remark => $counter) {
                $string .= 'Extra connections ' . $remark . ' => ' . $counter . "\n";

            }
            $msg = [
                'chat_id' => Constants::ADMIN_CHAT_ID,
                'text' => $string
            ];
            SendTelegramNotif::dispatch($msg);

        }
    }

    public function sendNotifPayment()
    {
        $trans = $_POST;
        AdminNotifier::paymentStatus($trans);
    }

    public function sendServerStatusNotif(Request $request)
    {
        AdminNotifier::serverStatus($request->get('msg'));
        return response()->json('ok', 200);
    }

    public function sendServerIntroduceNotif(Request $request)
    {
        $serverId = $request->get('server');
        CacheDB::setMalformedServer($serverId);
        return response()->json(['status' => 200 ,'data' => 'ok'], 200);
    }

    public function accountRegistrationNotif(Request $request)
    {
        if (!isset($_GET['remark'])) {
            return response()->json(['status' => 400 ,'data' => 'پارامتر ارسالی نامعتبر است'], 400);
        }
        $remark = $_GET['remark'];
        $record = RemarkDB::getAccountByRemark($remark);
        if (is_null($record)) {
            return response()->json(['status' => 400 ,'data' => 'حساب یافت نشد'], 400);
        }
        return response()->json(['status' => 200 ,'data' => $remark], 200);
    }

    public function sendGroupMessage(Request $request)
    {
        $v = Validator::make($request->all(), ['msg' => 'required']);
        if ($v->fails()) {
            return response()->json(['status' => 400 ,'data' => 'فیلد پیام خالی است'], 400);
        }
        $msg = $request->get('msg');
        $chatIds = array_values(array_unique(User::query()->orderBy('id', 'desc')->get()->pluck('chat_id')->toArray()));
        foreach ($chatIds as $chatId) {
            info('sending message to '.$chatId);
            SendBulkNotif::dispatch($msg, $chatId);
//            SendBulkNotif::dispatch($msg, '83525910');
//            SendBulkNotif::dispatch($msg, '525926585');
        }
        return response()->json(['status' => 200 ,'data' => 'درحال ارسال پیام'], 200);
    }

//    public function sendPrivateMessage(Request $request)
//    {
//        $v = Validator::make($request->all(), ['msg' => 'required', 'users' => 'required']);
//        if ($v->fails()) {
//            return response()->json(['status' => 400 ,'data' => 'فیلد پیام خالی است'], 400);
//        }
//        $msg = $request->get('msg');
//        $users = ($request->get('users'));
//        $remarks = [];
//        foreach ($users as $user) {
//            $remarks[] = $user['remark'];
//        }
//        $registered_remarks = Remark::query()->whereIn('remark', $remarks)->get()->pluck('chat_id')->toArray();
//        $chatIds = array_values(array_unique($registered_remarks));
//        return $chatIds;
//        foreach ($chatIds as $chatId) {
//            info('sending message to '.$chatId);
////            SendBulkNotif::dispatch($msg, $chatId);
////            SendBulkNotif::dispatch($msg, '83525910');
////            SendBulkNotif::dispatch($msg, '525926585');
//        }
//        return response()->json(['status' => 200 ,'data' => 'درحال ارسال پیام'], 200);
//    }

    public function reportIssue(Request $request)
    {
        $msg = $request->get('msg');
        AdminNotifier::serverStatus($msg);
        return response()->json(['status' => 200 ,'data' => 'پیام ارسال شد'], 200);
    }
}
