<?php

namespace App\Http\Controllers;

use App\Classes\AdminNotifier;
use App\Classes\UserDB;
use App\Classes\WalletDB;
use App\Jobs\SendTelegramNotif;
use Illuminate\Http\Request;

class AffiliateController extends Controller
{
    public function referralCredit(Request $request)
    {
        list($master_username, $credit) = json_decode(base64_decode($request->get('token')), true);
        $wallet = WalletDB::getUserWallet($master_username);
        if (is_null($wallet)) {
            $txt = " برای کاربر $master_username کیف پولی یافت نشد ";
            AdminNotifier::reportError($txt);
            return response()->json(['status' => 404,'error' => 'Wallet not found'], 200);
        }
        $new = $wallet->amount + $credit;
        $wallet->update(['amount' => $new]);
        $msg = [
            'chat_id' => $master_username,
            'text' => "🔥مبلغ $credit تومان به کیف پول شما واریز شد. موجودی جدید: ".$new,
            'parse_mode' => 'html',
        ];
        SendTelegramNotif::dispatch($msg);
        return response()->json(['status' => '200'], 200);
    }

    public function checkMaster()
    {
        $master = \request()->get('master');
        $a = UserDB::getUserByChatId($master);
        if (is_null($a)) {
            return response()->json(['status' => 404, 'error' => 'user not found']);
        }
        return response()->json(['status' => 200]);
    }
}
