<?php

namespace App\Http\Controllers\Api\Components\Report;

use App\Classes\AppDB;
use App\Classes\CacheDB;
use App\Classes\Constants;
use App\Classes\Keyboards;
use App\Classes\PlusDB;
use App\Classes\RemarkDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Services\HttpTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Morilog\Jalali\Jalalian;

class PostReportAccountAction extends AbstractComponent
{
    use HttpTrait;
    public $msg = [];

    public function execute($arguments = null)
    {
        $remark = strtolower(app('extractor')->command);
        $chat_id = app('extractor')->chat_id;
        $this->msg = [
            'chat_id' => $chat_id,
            'parse_mode' => 'markdown',
        ];
        if (str_contains($remark, 'ir')) {
            $usages = AppDB::reportUserName($remark);
            $msg['chat_id'] = $chat_id;
            $msg['text'] = '';
            $msg['parse_mode'] = 'markdown';
            if (count($usages) == 0) {
                $msg['text'] = 'حساب یافت نشد';
                SendTelegramNotif::dispatch($msg);
                exit();
            }
            $pass = in_array($chat_id, config('bot.admin_ids')) ? $usages[0]->code : '';
            $exp = Jalalian::fromCarbon(Carbon::parse($usages[0]->base_exp))->format('d-m-Y H:i');
            $state = $usages[0]->user_active == 1 ? 'فعال' : 'غیر فعال';
            $total_usage = $usages->sum('vol');
            $total = $usages[0]->base_vol;
            $msg['text'] .= "🦸نام کاربری : ". '`'.$remark.'`' . "\n" .
                "🔑کلمه عبور : $pass" . "\n".
                "✅ وضعیت اکانت : $state " . "\n".
                "📆 تاریخ انقضا : $exp" . "\n" .
                " 🔋حجم کل : $total گیگ" ."\n".
                " 💶 میزان مصرف کل: $total_usage" . "\n" .
                "\n -------------------------- \n" ;
            foreach ($usages as $usage) {
                $usage = (array)$usage;
                $version = $usage['app_version'];
                $enable = $usage['device_active']== 1 ? 'فعال' : 'غیر فعال';
                $is_owner = $usage['is_owner'] == 1 ? 'مالک' : 'کاربر';
                $vol = $usage['vol'];
                $agent = $usage['agent'];
                $pass = in_array($chat_id, config('bot.admin_ids')) ? $pass : '';
                $msg['text'] .=
                    "✅ وضعیت دستگاه : $enable" . "\n" .
                    "👤 وضعیت مالکیت : $is_owner" . "\n" .
                    " 💶 میزان مصرف : $vol" . "\n" .
                    "📱 سیستم عامل : $agent" . "\n" .
                    "🔸 نسخه : $version" . "\n -------------------------- \n";
            }
            SendTelegramNotif::dispatch($msg);
            CacheDB::clearStack($chat_id);
            exit();
        }
        elseif (str_contains($remark, 'ip')) {
            $agent = in_array($chat_id, config('bot.admin_ids')) ? 'admin' : 'user';
            $resp = PlusDB::getAccountReport($remark, $agent);
            if (is_null($resp)) {
                $msg = [
                    'chat_id' => $chat_id,
                    'text' => 'خطا در دریافت اطلاعات',
                    'parse_mode' => 'HTML',
                ];
            } else {
                $msg = [
                    'chat_id' => $chat_id,
                    'text' => $resp['msg'] ?? $resp['error'],
                    'parse_mode' => 'markdown',
                ];
            }

            SendTelegramNotif::dispatch($msg);
            CacheDB::clearStack($chat_id);
            exit();
        }
        elseif (str_contains($remark, 'br')) {
            $fqdm = $this->_extractServerAddress($remark, 'br');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } elseif (str_contains($remark, 'no')) {
            $fqdm = $this->_extractServerAddress($remark, 'no');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } elseif (str_contains($remark, 's')) {
            $fqdm = $this->_extractServerAddress($remark, 's');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } elseif (str_contains($remark, 'm')) {
            $fqdm = $this->_extractServerAddress($remark, 'm');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } elseif (str_contains($remark, 'pc')) {
            $fqdm = $this->_extractServerAddress2($remark, 'pc');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } elseif (str_contains($remark, 'cu')) {
            $fqdm = $this->_extractServerAddress2($remark, 'cu');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } else {
            $this->msg['text'] = 'شناسه اکانت نادرست است';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $enable = $data['enable'] == 1 ? 'فعال' : 'غیر فعال';
        $usage = $data['usage'];
        $unit = $data['unit'];
        $exp = $data['exp'];
        $total = $data['total'];
        $remark = '`'.$data['remark'].'`';
        $transmission = $data['stream']->network;
        if (str_contains($data['remark'], 'pc')) {
            $os = 'pc';
        } else {
            if ($transmission == 'ws') {
                $os = 'ios';
            } else {
                $os = 'android';
            }
        }
        $this->msg['text'] = "✅ وضعیت : $enable
💶 میزان مصرف :  $unit $usage
📆 تاریخ انقضا: $exp
🔋حجم کل: $total گیگ
📱 سیستم عامل: $os
    🦸نام کاربری : $remark
                ";
        $keyboard = in_array($chat_id, config('bot.admin_ids')) ? Keyboards::adminGlassyKeyboard() : Keyboards::glassyKeyboard();
        $this->msg['reply_markup'] = app('bot')->buildKeyboard($keyboard);
        SendTelegramNotif::dispatch($this->msg);
    }

    private function _extractServerAddress($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        if (count($matches[0]) != 2) {
            $this->msg['text'] = 'لطفا نام کاربری را صحیح وارد کنید مثلا br4.1';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $server_id = $matches[0][0];
        $user_id = $matches[0][1];
        try {
            if (in_array($this->msg['chat_id'], config('bot.admin_ids'))) {
                $fqdm = Constants::ADMIN_IRAN_SERVERS[$server_identifier . $server_id];
            } else {
                $fqdm = Constants::IRAN_SERVERS[$server_identifier . $server_id];
            }
        } catch (\Exception $exception) {
            info($exception->getMessage());
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        return $fqdm;
    }

    private function _extractServerAddress2($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        if (count($matches[0]) != 1) {
            $this->msg['text'] = 'لطفا نام کاربری را صحیح وارد کنید مثلا pc12';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $server_id = $matches[0][0];
        try {
            if (in_array($this->msg['chat_id'], config('bot.admin_ids'))) {
                $fqdm = Constants::ADMIN_IRAN_SERVERS[$server_identifier];
            } else {
                $fqdm = Constants::IRAN_SERVERS[$server_identifier];
            }
        } catch (\Exception $exception) {
            info($exception->getMessage());
            $this->msg['text'] = 'حساب یافت نشد. لطفا به پشتیبانی پیام دهید';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        return $fqdm;
    }

    private function _getDataFromRemoteServer($fqdm, $remark)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/inbound' : $fqdm . '/api/inbound';
        $resp = $this->sendHttp($address, ['remark' => $remark]);
        info(json_encode($resp));
        if (is_null($resp) || $resp->status != 200) {
            $this->msg['text'] = 'حساب یافت نشد. لطفا به پشتیبانی پیام دهید';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $user_data = $resp->data;
        $usage = $user_data->up + $user_data->down;
        $tmp = $usage;
        $gb = pow(10, 9);
        $mb = pow(10, 6);
        $kb = pow(10, 3);
        if ($usage < 1000) {
            $unit = 'B';
        }
        if ($usage > $kb) {
            $tmp = $usage / $kb;
            $unit = 'KB';
        }
        if ($usage > $mb) {
            $tmp = $usage / $mb;
            $unit = 'MB';
        }
        if ($usage > $gb) {
            $tmp = $usage / $gb;
            $unit = 'GB';
        }
        $usage = $tmp;
        if ($user_data->expiry_time == 0) {
            $exp = 'ندارد';
        } else {
            $exp = Jalalian::fromCarbon(Carbon::createFromTimestampMs($user_data->expiry_time))->format('d-m-Y H:i');
        }
        $enable = $user_data->enable;
        $total = $user_data->total / (1000 * pow(10, 6));
        $stream = json_decode($user_data->stream_settings);
        return compact('usage', 'unit', 'exp', 'enable', 'total', 'remark', 'stream');
    }
}
