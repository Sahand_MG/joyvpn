<?php

namespace App\Http\Controllers\Api\Components\Report;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostReportAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
//        Remark::query()->updateOrCreate(['chat_id' => $data->chat_id], ['os' => Constants::ANDROID ? 'android' : 'ios']);
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'لطفا نام کاربری خود را ارسال کنید',
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }
}