<?php

namespace App\Http\Controllers\Api\Components\Not;

use App\Classes\Commands;
use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostNotFoundAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $keyboard = in_array($data->chat_id, config('bot.admin_ids')) ? Keyboards::adminGlassyKeyboard() : Keyboards::glassyKeyboard();
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'لطفا از دستورات مجاز استفاده کنید',
            'reply_markup' => app('bot')->buildKeyBoard($keyboard),
        ];
        SendTelegramNotif::dispatch($msg);
    }
}