<?php

namespace App\Http\Controllers\Api\Components\Not;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostNotAllowedAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'امکان ارسال فایل وجود ندارد'
        ];
        SendTelegramNotif::dispatch($msg);
    }
}