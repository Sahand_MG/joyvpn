<?php

namespace App\Http\Controllers\Api\Components\Reconnect;

use App\Classes\AppDB;
use App\Classes\Constants;
use App\Classes\PlusDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use Carbon\Carbon;
use Morilog\Jalali\Jalalian;

class PostReconnectRequestAction extends AbstractComponent
{
    public $msg;

    public function execute($arguments = null)
    {
        $remark = strtolower(app('extractor')->command);
        $chat_id = app('extractor')->chat_id;
        $this->msg = [
            'chat_id' => $chat_id,
            'parse_mode' => 'HTML',
        ];
        if (str_contains($remark, 'ir')) {
            AppDB::activateUserName($remark);
            $this->msg['text'] = 'درخواست فعالسازی ثبت شد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        } elseif (str_contains($remark, 'ip')) {
            $has_error = PlusDB::activateUserName($remark);
            if ($has_error == 0) {
                $this->msg['text'] = 'درخواست فعالسازی ثبت شد';
            } else {
                $this->msg['text'] = 'خطا در اجرای دستور';
            }
            SendTelegramNotif::dispatch($this->msg);
            exit();
        } elseif (str_contains($remark, 'br')) {
            $fqdn = $this->_extractServerAddress($remark, 'br');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } elseif (str_contains($remark, 'no')) {
            $fqdn = $this->_extractServerAddress($remark, 'no');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } elseif (str_contains($remark, 's')) {
            $fqdn = $this->_extractServerAddress($remark, 's');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } elseif (str_contains($remark, 'm')) {
            $fqdn = $this->_extractServerAddress($remark, 'm');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } elseif (str_contains($remark, 'fe')) {
            $fqdn = $this->_extractServerAddress2($remark, 'fe');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } elseif (str_contains($remark, 'cu')) {
            $fqdn = $this->_extractServerAddress2($remark, 'cu');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } else {
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $resp = $this->_sendReconnectRequest($fqdn, $remark);
        if ($resp->status == 200) {
            $this->msg['text'] = 'درخواست فعالسازی ثبت شد';
            SendTelegramNotif::dispatch($this->msg);
        }
    }

    private function _extractServerAddress($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        if (count($matches[0]) != 2) {
            $this->msg['text'] = 'لطفا نام کاربری را صحیح وارد کنید مثلا br4.1';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $server_id = $matches[0][0];
        $user_id = $matches[0][1];
        try {
            $fqdn = Constants::IRAN_SERVERS[$server_identifier . $server_id];
        } catch (\Exception $exception) {
            info($exception->getMessage());
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        return $fqdn;
    }

    private function _extractServerAddress2($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        if (count($matches[0]) != 1) {
            $this->msg['text'] = 'لطفا نام کاربری را صحیح وارد کنید مثلا br4.1';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $server_id = $matches[0][0];
        try {
            $fqdn = Constants::IRAN_SERVERS[$server_identifier];
        } catch (\Exception $exception) {
            info($exception->getMessage());
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        return $fqdn;
    }

    private function _sendReconnectRequest($fqdn, $remark)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/inbound/reconnect' : $fqdn . '/api/inbound/reconnect';
        $resp = $this->sendHttp($address, ['remark' => $remark]);
        return $resp;
    }

    private function _getDataFromRemoteServer($fqdn, $remark)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/inbound' : $fqdn . '/api/inbound';
        $resp = $this->sendHttp($address, ['remark' => $remark]);
        info(json_encode($resp));
        if (is_null($resp) || $resp->status != 200) {
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $user_data = $resp->data;
        $usage = $user_data->up + $user_data->down;
        $tmp = $usage;
        $gb = pow(10, 9);
        $mb = pow(10, 6);
        $kb = pow(10, 3);
        if ($usage < 1000) {
            $unit = 'B';
        }
        if ($usage > $kb) {
            $tmp = $usage / $kb;
            $unit = 'KB';
        }
        if ($usage > $mb) {
            $tmp = $usage / $mb;
            $unit = 'MB';
        }
        if ($usage > $gb) {
            $tmp = $usage / $gb;
            $unit = 'GB';
        }
        $usage = $tmp;
        if ($user_data->expiry_time == 0) {
            $exp = 'ندارد';
        } else {
            $exp = Jalalian::fromCarbon(Carbon::createFromTimestampMs($user_data->expiry_time))->format('d-m-Y');
        }
        $enable = $user_data->enable;
        $total = $user_data->total / (1000 * pow(10, 6));
        return compact('usage', 'unit', 'exp', 'enable', 'total', 'remark');
    }

    private function sendHttp($server_address, array $data)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $server_address,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ));
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            info(json_encode($response));
            $this->msg['text'] = 'خطای node';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        curl_close($curl);
        info(json_encode($response));
        return json_decode($response);
    }
}