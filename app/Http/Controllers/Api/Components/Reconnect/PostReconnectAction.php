<?php

namespace App\Http\Controllers\Api\Components\Reconnect;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostReconnectAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'لطفا نام کاربری خود را ارسال کنید. دقت کنید که نام کاربری رو بدون پسوند وارد کنید، مثلا br12.3. برای پیدا کردن نام‌کاربری، در برنامه نپسترن، به قسمت تنظیمات یا config مراجعه کنید',
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }
}