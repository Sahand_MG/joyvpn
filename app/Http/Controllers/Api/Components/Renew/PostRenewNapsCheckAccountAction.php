<?php

namespace App\Http\Controllers\Api\Components\Renew;

use App\Classes\AdminNotifier;
use App\Classes\AppDB;
use App\Classes\CacheDB;
use App\Classes\Constants;
use App\Classes\Keyboards;
use App\Classes\Payment\ZarinpalPayment;
use App\Classes\Payment\ZarinPaymentRenew;
use App\Classes\PlusDB;
use App\Classes\RemarkDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Plan;
use App\Classes\Payment\Paystar;
use App\Services\GeneralService;
use App\Services\HttpTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Morilog\Jalali\Jalalian;
use Spatie\Emoji\Emoji;

class PostRenewNapsCheckAccountAction extends AbstractComponent
{
    use HttpTrait;
    public $msg;
    public $banned_server = [];

    public function execute($arguments = null)
    {
        $gates = config('bot.gates');
        $gate = $gates[rand(0, 1)];
        $remark = strtolower(app('extractor')->command);
        $chat_id = app('extractor')->chat_id;
        $this->msg = [
            'chat_id' => $chat_id,
            'parse_mode' => 'HTML',
        ];
        foreach ($this->banned_server as $server) {
            if (str_contains($remark, $server)) {
                $this->msg['text'] = '⚠️ به دلیل اختلالات گسترده روی این سرور، قادر به ارائه خدمات در بستر آن نیستیم. لطفا برای دریافت اکانت جدید به @Joyvpn_Support پیام بدید.';
                SendTelegramNotif::dispatch($this->msg);
                CacheDB::clearStack($chat_id);
                exit();
            }
        }
        if (str_contains($remark, 'ir')) {
            $this->_sendExpiryUpdateRequestToApp($remark);
            exit();
        }
        elseif (str_contains($remark, 'ip')) {
            $this->_sendExpiryUpdateRequestToPlus($remark);
            exit();
        }
//        elseif (str_contains($remark, 'br')) {
//            $fqdn = $this->_extractServerAddress($remark, 'br');
//            $remote_data = $this->_getDataFromRemoteServer($fqdn, $remark);
//        } elseif (str_contains($remark, 'no')) {
//            $fqdn = $this->_extractServerAddress($remark, 'no');
//            $remote_data = $this->_getDataFromRemoteServer($fqdn, $remark);
//        } elseif (str_contains($remark, 's')) {
//            $fqdn = $this->_extractServerAddress($remark, 's');
//            $remote_data = $this->_getDataFromRemoteServer($fqdn, $remark);
//        } elseif (str_contains($remark, 'm')) {
//            $fqdn = $this->_extractServerAddress($remark, 'm');
//            $remote_data = $this->_getDataFromRemoteServer($fqdn, $remark);
//        } elseif (str_contains($remark, 'fe')) {
//            $fqdn = $this->_extractServerAddress2($remark, 'fe');
//            $remote_data = $this->_getDataFromRemoteServer($fqdn, $remark);
//        } elseif (str_contains($remark, 'cu')) {
//            $fqdn = $this->_extractServerAddress2($remark, 'cu');
//            $remote_data = $this->_getDataFromRemoteServer($fqdn, $remark);
//        }
        else {
            $this->msg['text'] = 'حساب یافت نشد. لطفا به پشتیبانی پیام دهید';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $remark_record = RemarkDB::getAccountByRemark($remark);
        $transmission = $remote_data['stream']->network;
//        if ($transmission != 'ws') {
//            $this->msg['text'] =' امکان تمدید اکانت اندروید روی نپستنرن از ۱ آذر غیرفعال شده است، لطفا جهت تهییه اکانت برنامه نرم افزار جدید به پشتیبانی پیام دهید';
//            SendTelegramNotif::dispatch($this->msg);
//            exit();
//        }
        if (is_null($remark_record)) {
            $msg = [
                'chat_id' => $chat_id,
                'text' => 'لطفا ابتدا اکانت خود را در ربات ثبت کنید و سپس مجدد گزینه تمدید اکانت را بزنید' . Emoji::backhandIndexPointingDown(),
                'reply_markup' => app('bot')->buildInlineKeyBoard(Keyboards::renewNapsternKeyBoard()),
            ];
            SendTelegramNotif::dispatch($msg);
            exit();
        }
        if (($transmission == 'ws' && $remark_record->os == Constants::IOS) || ($transmission == 'tcp' && $remark_record->os == Constants::ANDROID) || ($transmission == 'ws' && $remark_record->os == Constants::ANDROID && in_array($fqdn, array_values(Constants::ANDROID_DIRECT_SERVERS)))) {
            $data = app('extractor');
            $msg = [
                'chat_id' => $data->chat_id,
                'action' => 'typing'
            ];
            SendTelegramNotif::dispatch($msg, 'chat_action');
            $amount = GeneralService::planPriceCalcByVolAndOs($remote_data['total'], $remark_record->os);
            $msg = [
                'chat_id' => $chat_id,
                'photo' => Constants::JVPAY,
            ];
            if ($gate == 'zarin') {
                $trans = (new ZarinPaymentRenew($amount, $remark, Constants::EXP, $fqdn))->initiatePayment();
                $msg['caption'] = 'مبلغ قابل پرداخت (بدون احتساب کارمزد درگاه): ' . $trans->amount . ' تومان. درصورت وارد نشدن به درگاه لطفا vpn را خاموش کنید و مجدد وارد لینک پرداخت شوید. ';
                $msg['reply_markup'] = app('bot')->buildInlineKeyBoard(Keyboards::paymentKeyboard(config('bot.zarin_path') . $trans->authority));
            } elseif ($gate == 'pay') {
                list($data, $trans) = (new Paystar($amount, $remark, Constants::EXP, $fqdn))->initiatePayment();
                $msg['caption'] = 'مبلغ قابل پرداخت (بدون احتساب کارمزد درگاه) : ' . $trans->amount / 10 . ' تومان. درصورت وارد نشدن به درگاه لطفا vpn را خاموش کنید و مجدد وارد لینک پرداخت شوید. ';
                $msg['reply_markup'] = app('bot')->buildInlineKeyBoard(Keyboards::paymentKeyboard(config('bot.paystar_path') . '?token=' . $data['token']));
            }
            SendTelegramNotif::dispatch($msg, 'photo');
        } else {
            $msg = [
                'chat_id' => $chat_id,
                'text' => 'اکانت مورد نظر با سیستم عامل دستگاه شما تطابق ندارد. لطفا به پشتیبانی پیام دهید',
            ];
            SendTelegramNotif::dispatch($msg);
            $msg2 = [
                'chat_id' => Constants::ADMIN_CHAT_ID,
                'text' => 'inconsistent account ' . $remark,
            ];
            SendTelegramNotif::dispatch($msg2);
            CacheDB::clearStack($chat_id);
            exit();
        }

    }

    private function _sendExpiryUpdateRequestToApp($remark)
    {
        $acc = AppDB::checkAccountAvailability($remark);
        if (is_null($acc)) {
            $this->msg['text'] = 'حساب یافت نشد. لطفا به پشتیبانی پیام دهید';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $gates = config('bot.gates');
        $gate = $gates[rand(0, 1)];
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'action' => 'typing'
        ];
        SendTelegramNotif::dispatch($msg, 'chat_action');
        $amount = GeneralService::androidPlanPriceCalcByVol($acc->base_vol);
        $msg = [
            'chat_id' => $data->chat_id,
            'photo' => Constants::JVPAY,
        ];
        if ($gate == 'zarin') {
            $trans = (new ZarinPaymentRenew($amount, $remark, Constants::EXP, '0.0.0.0'))->initiatePayment();
            $msg['caption'] = 'مبلغ قابل پرداخت (بدون احتساب کارمزد درگاه): ' . $trans->amount . ' تومان.  درصورت وارد نشدن به درگاه لطفا vpn را خاموش کنید و مجدد وارد لینک پرداخت شوید.';
            $msg['reply_markup'] = app('bot')->buildInlineKeyBoard(Keyboards::paymentKeyboard(config('bot.zarin_path') . $trans->authority));
        } elseif ($gate == 'pay') {
            list($data, $trans) = (new Paystar($amount, $remark, Constants::EXP, '0.0.0.0'))->initiatePayment();
            $msg['caption'] = 'مبلغ قابل پرداخت (بدون احتساب کارمزد درگاه) : ' . $trans->amount / 10 . ' تومان.  درصورت وارد نشدن به درگاه لطفا vpn را خاموش کنید و مجدد وارد لینک پرداخت شوید.';
            $msg['reply_markup'] = app('bot')->buildInlineKeyBoard(Keyboards::paymentKeyboard(config('bot.paystar_path') . '?token=' . $data['token']));
        }
        SendTelegramNotif::dispatch($msg, 'photo');
    }

    private function _sendExpiryUpdateRequestToPlus($remark)
    {
        $subAccount = PlusDB::getSubAccount($remark);
        if(is_null($subAccount) || $subAccount['status'] != 200) {
            $this->msg['text'] = 'خطا در دریافت اطلاعات';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $remark_record = RemarkDB::getAccountByRemark($remark);
        if (is_null($remark_record)) {
            $msg = [
                'chat_id' => app('extractor')->chat_id,
                'text' => 'لطفا ابتدا اکانت خود را در ربات ثبت کنید و سپس مجدد گزینه تمدید اکانت را بزنید' . Emoji::backhandIndexPointingDown(),
                'reply_markup' => app('bot')->buildInlineKeyBoard(Keyboards::renewNapsternKeyBoard()),
            ];
            SendTelegramNotif::dispatch($msg);
            exit();
        }
        if ($remark_record->os != Constants::IOS) {
            $chat_id = app('extractor')->chat_id;
            $msg = [
                'chat_id' => $chat_id,
                'text' => 'اکانت مورد نظر با سیستم عامل دستگاه شما تطابق ندارد. لطفا به پشتیبانی پیام دهید',
            ];
            SendTelegramNotif::dispatch($msg);
            $msg2 = [
                'chat_id' => Constants::ADMIN_CHAT_ID,
                'text' => 'inconsistent account ' . $remark,
            ];
            SendTelegramNotif::dispatch($msg2);
            CacheDB::clearStack($chat_id);
            exit();
        }
        $fqdn_map = collect($subAccount['msg']['maps'])->pluck('remote_remark')->toArray();
        $total = $subAccount['msg']['total'];
        $gate = config('bot.ip_gate'); // dont change it
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'action' => 'typing'
        ];
        SendTelegramNotif::dispatch($msg, 'chat_action');
        $amount = GeneralService::iosPlanPriceCalcByVol($total);
        $msg = [
            'chat_id' => $data->chat_id,
            'photo' => Constants::JVPAY,
        ];
        if ($gate == 'zarin') {
            $trans = (new ZarinPaymentRenew($amount, $remark, Constants::EXP, json_encode($fqdn_map), 0, 1))->initiatePayment();
            $msg['caption'] = 'مبلغ قابل پرداخت (بدون احتساب کارمزد درگاه): ' . $trans->amount . ' تومان.  درصورت وارد نشدن به درگاه لطفا vpn را خاموش کنید و مجدد وارد لینک پرداخت شوید.';
            $msg['reply_markup'] = app('bot')->buildInlineKeyBoard(Keyboards::paymentKeyboard(config('bot.zarin_path') . $trans->authority));
        } elseif ($gate == 'pay') {
            list($data, $trans) = (new Paystar($amount, $remark, Constants::EXP, json_encode($fqdn_map), 0, 1))->initiatePayment();
            $msg['caption'] = 'مبلغ قابل پرداخت (بدون احتساب کارمزد درگاه) : ' . $trans->amount / 10 . ' تومان.  درصورت وارد نشدن به درگاه لطفا vpn را خاموش کنید و مجدد وارد لینک پرداخت شوید.';
            $msg['reply_markup'] = app('bot')->buildInlineKeyBoard(Keyboards::paymentKeyboard(config('bot.back_gateway_path') . '?token=' . $data['token']));
        }
        SendTelegramNotif::dispatch($msg, 'photo');

    }

    private function _extractServerAddress($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        if (count($matches[0]) != 2) {
            $this->msg['text'] = 'لطفا نام کاربری را صحیح وارد کنید مثلا br4.1';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $server_id = $matches[0][0];
        $user_id = $matches[0][1];
        try {
            $fqdn = Constants::IRAN_SERVERS[$server_identifier . $server_id];
        } catch (\Exception $exception) {
            info($exception->getMessage());
            $this->msg['text'] = 'حساب یافت نشد. لطفا به پشتیبانی پیام دهید';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        return $fqdn;
    }

    private function _extractServerAddress2($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        if (count($matches[0]) != 1) {
            $this->msg['text'] = 'لطفا نام کاربری را صحیح وارد کنید مثلا br4.1';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $server_id = $matches[0][0];
        try {
            $fqdn = Constants::IRAN_SERVERS[$server_identifier];
        } catch (\Exception $exception) {
            info($exception->getMessage());
            $this->msg['text'] = 'حساب یافت نشد. لطفا به پشتیبانی پیام دهید';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        return $fqdn;
    }

    private function _getDataFromRemoteServer($fqdn, $remark)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/inbound' : $fqdn . '/api/inbound';
        $resp = $this->sendHttp($address, ['remark' => $remark]);
        info(json_encode($resp));
        if (is_null($resp) || $resp->status != 200) {
            $this->msg['text'] = 'حساب یافت نشد. لطفا به پشتیبانی پیام دهید';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $user_data = $resp->data;
        $usage = $user_data->up + $user_data->down;
        $tmp = $usage;
        $gb = pow(10, 9);
        $mb = pow(10, 6);
        $kb = pow(10, 3);
        if ($usage < 1000) {
            $unit = 'B';
        }
        if ($usage > $kb) {
            $tmp = $usage / $kb;
            $unit = 'KB';
        }
        if ($usage > $mb) {
            $tmp = $usage / $mb;
            $unit = 'MB';
        }
        if ($usage > $gb) {
            $tmp = $usage / $gb;
            $unit = 'GB';
        }
        $usage = $tmp;
        if ($user_data->expiry_time == 0) {
            $exp = 'ندارد';
        } else {
            $exp_carbon_time = Carbon::createFromTimestampMs($user_data->expiry_time);
//            if (Carbon::now()->diffInDays($exp_carbon_time) > 2 && Carbon::now()->lessThan($exp_carbon_time)) {
//                $this->msg['text'] = ' امکان تمدید پیش از موعد وجود ندارد 💁‍♂️';
//                SendTelegramNotif::dispatch($this->msg);
//                exit();
//            }
            $exp = Jalalian::fromCarbon($exp_carbon_time)->format('d-m-Y');
        }
        $enable = $user_data->enable;
        $total = $user_data->total / (1000 * pow(10, 6));
        $stream = json_decode($user_data->stream_settings);
        return compact('usage', 'unit', 'exp', 'enable', 'total', 'remark', 'stream');
    }
}
