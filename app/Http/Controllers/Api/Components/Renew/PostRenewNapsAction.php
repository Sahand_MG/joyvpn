<?php

namespace App\Http\Controllers\Api\Components\Renew;

use App\Classes\Constants;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostRenewNapsAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'photo' => Constants::JV_APP_BANNER,
            'caption' => 'لطفا نام کاربری خود را ارسال کنید',
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg, 'photo');
    }
}
