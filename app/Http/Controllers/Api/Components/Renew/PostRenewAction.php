<?php

namespace App\Http\Controllers\Api\Components\Renew;

use App\Classes\Builders\RenewAccount;
use App\Classes\CacheKeys;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Accounts;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class PostRenewAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $accounts = $this->_getUserAccounts($data->chat_id);
        if($accounts->isEmpty()) {
            $msg = [
                'chat_id' => $data->chat_id,
                'text'  => ' حساب فعالی جهت تمدید یافت نشد',
            ];
            Cache::forget(CacheKeys::getRenewKey());
            SendTelegramNotif::dispatch($msg);
        }else{
            Cache::forever(CacheKeys::getRenewKey(), new RenewAccount());
            $buttons = [];
            foreach ($accounts as $account){
                array_push($buttons, [app('bot')->buildInlineKeyboardButton($account->username, '', $account->username)]);
            }
            $msg = [
                'chat_id' => $data->chat_id,
                'text'  => ' حساب خود را انتخاب کنید',
                'reply_markup' => app('bot')->buildInlineKeyBoard($buttons)
            ];
            SendTelegramNotif::dispatch($msg);
        }
    }
    private function _getUserAccounts($chat_id)
    {
        return Accounts::where('user_id', $chat_id)
            ->where('expires_at', '>', Carbon::now())
            ->where('used', 1)
            ->where('username', 'not like', 'free%')
            ->get();
    }
}