<?php

namespace App\Http\Controllers\Api\Components\Renew;

use App\Classes\Builders\RenewAccount;
use App\Classes\CacheKeys;
use App\Classes\Commands;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Accounts;
use Illuminate\Support\Facades\Cache;

class PostRenewSelectAccountAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $username = $arguments;
        $account = Accounts::where('username', $username)->first();
        $plan = $account->plan;
        $private_commands = Commands::getAllPrivates();
        /**
         * @var $command_class AbstractComponent
         * @var $renew_inst RenewAccount
         */
        $renew_inst = Cache::get(CacheKeys::getRenewKey());
        $renew_inst->setAccount($account);
        $renew_inst->setUserName($username);
        Cache::put(CacheKeys::getRenewKey(), $renew_inst, 60);
        $command_class = new $private_commands['/'.$plan->name];
        $command_class->execute();
    }
}