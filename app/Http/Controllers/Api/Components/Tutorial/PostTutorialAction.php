<?php

namespace App\Http\Controllers\Api\Components\Tutorial;

use App\Classes\Constants;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostTutorialAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => Constants::JOYVPN_GUIDE
        ];
        SendTelegramNotif::dispatch($msg);
    }
}