<?php

namespace App\Http\Controllers\Api\Components\Contact;

use App\Classes\CacheKeys;
use App\Classes\Constants;
use App\Classes\Extractor\AbstractExtractor;
use App\Classes\Keyboards;
use App\Classes\Payment\AbstractPayment;
use App\Classes\Payment\PaymentDirector;
use App\Classes\Payment\PaystarPayment;
use App\Classes\Payment\ZarinpalPayment;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class PostContactDataAction extends AbstractComponent
{
    public function execute($arguments = [])
    {
        /**
         * @var $data AbstractExtractor
         */
        $data = app('extractor');
        if($arguments[0] == 'phone') {
            User::where('chat_id', $data->chat_id)->first()->update(['phone' => $arguments[1]]);

        }elseif ($arguments[0] == 'email') {
            User::where('chat_id', $data->chat_id)->first()->update(['email' => $arguments[1]]);
        }
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'action'  => 'typing'
        ];
        SendTelegramNotif::dispatch($msg, 'chat_action');
        /**
         * @var $director PaymentDirector
         */
        $director = Cache::pull(CacheKeys::getPaymentKey());
        if(gettype($director->pay()) == 'string') {
            $msg = [
                'chat_id' => $data->chat_id,
                'text' => $director->pay()
            ];
            SendTelegramNotif::dispatch($msg);
        }else{
            $trans = $director->getTransaction();
            $msg = [
                'chat_id' => $data->chat_id,
                'photo' => Constants::JVPAY,
                'caption' => 'مبلغ قابل پرداخت : '.$director->getPlan()->price.' تومان ',
                'reply_markup' => app('bot')->buildInlineKeyBoard(Keyboards::paymentKeyboard($trans)),
            ];
            SendTelegramNotif::dispatch($msg, 'photo');
        }


    }
}