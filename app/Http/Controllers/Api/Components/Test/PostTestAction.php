<?php

namespace App\Http\Controllers\Api\Components\Test;

use App\Classes\Constants;
use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Spatie\Emoji\Emoji;


class PostTestAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        //        Payment is cancel!
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'photo'  => 'https://store-images.s-microsoft.com/image/apps.50495.14292032345918157.506ac205-0628-43fe-a413-4dec79f809c3.4eed59a3-7f42-4269-a839-67f413de8711',
            'reply_markup' => app('bot')->buildInlineKeyboard(Keyboards::mainKeyboard())
        ];
        SendTelegramNotif::dispatch($msg, 'photo');
    }
}