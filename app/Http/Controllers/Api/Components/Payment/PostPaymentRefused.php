<?php

namespace App\Http\Controllers\Api\Components\Payment;

use App\Classes\Constants;
use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Transaction;

class PostPaymentRefused extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $state = Transaction::where('chat_id', $data->chat_id)
            ->where('status', Constants::PAYMENT_UNPAID)
            ->orderBy('id', 'desc')->first();
        if(!is_null($state)) {
            $state->update(['status' => Constants::PAYMENT_CANCELED]);
            $text = trans('dict.canceled');
        }else{
            $text = trans('dict.no_unpaid_transaction');
        }
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => $text,
            'reply_markup' => app('bot')->buildKeyBoard(Keyboards::glassyKeyboard()),
        ];
        SendTelegramNotif::dispatch($msg);

        $d = [
            'chat_id' => $data->chat_id,
            'message_id' => $data->message_id
        ];
        SendTelegramNotif::dispatch($d, 'delete_message');
    }
}