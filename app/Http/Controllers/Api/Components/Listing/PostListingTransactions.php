<?php

namespace App\Http\Controllers\Api\Components\Listing;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Transaction;
use Carbon\Carbon;
use Spatie\Emoji\Emoji;

class PostListingTransactions extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $transactions = Transaction::where('chat_id', $data->chat_id)->where('status', 'paid')->orderBy('id', 'desc')->take(10)->get();
        if($transactions->isEmpty()) {
            $msg = [
                'chat_id' => $data->chat_id,
                'text' => Emoji::crossMark().' تراکنشی یافت نشد '.Emoji::crossMark(),
                'parse_mode' => 'Html',
            ];
            SendTelegramNotif::dispatch($msg);
        }else{
            $string = Emoji::blueCircle().Emoji::blueCircle()."  اطلاعات ۱۰ تراکنش اخیر ".Emoji::blueCircle().Emoji::blueCircle()."\n\n";
            foreach ($transactions as $trans){
                $amount = $trans->amount;
                $date = \Morilog\Jalali\Jalalian::fromCarbon(Carbon::parse($trans->created_at))->format('%B %d، %Y');
                $username = is_null($trans->account) ? $trans->remark :  $trans->account->username;
                $plan = trans('dict.'.$trans->plan->name);
                $status = $trans->status == 'paid' ? 'موفق' : 'ناموفق';
                $string = $string."✅ کد تراکنش : $trans->trans_id
💶 مبلغ پرداخت شده :  تومان $amount
📆 تاریخ : $date
🦸نام کاربری : $username
🔅 وضعیت پرداخت :$status "."\n"." ================= \n";
            }
            $msg = [
                'chat_id' => $data->chat_id,
                'text' => $string,
                'parse_mode' => 'Html',
            ];
            SendTelegramNotif::dispatch($msg);

        }
    }
}