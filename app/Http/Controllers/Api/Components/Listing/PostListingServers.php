<?php

namespace App\Http\Controllers\Api\Components\Listing;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Server;

class PostListingServers extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => $this->_getServerList(),
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }

    private function _getServerList()
    {
        $t = '';
        $servers = Server::up()->get();
        foreach ($servers as $key => $server){
            $k = $key+1;
            $t = $t."Server$k : ".$server->region."\n";
        }
        return $t;
    }
}