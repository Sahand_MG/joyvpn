<?php

namespace App\Http\Controllers\Api\Components\Poll;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Accounts;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class PostPollGroupAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        if(Cache::has('grouppoll')) {

        }else{
            $chat_ids = array_values(array_filter(array_unique(Accounts::get()->pluck('user_id')->toArray())));
            foreach ($chat_ids as $chat_id){
                $msg = [
                    'chat_id' => $chat_id,
                    'question' => "عملکرد سرویس Joy VPN را چه طور ارزیابی می‌کنید؟",
                    'options'    => json_encode(['خوبه،کار راه بندازه','نه به درد نمیخوره','هنوز استفاده نکردم']),
                    'is_anonymous'  => false,
                    'type'  => 'regular'
                ];
                SendTelegramNotif::dispatch($msg, 'poll');
                sleep(1);
            }
            Cache::put('grouppoll', 1, 324000);
        }
    }
}