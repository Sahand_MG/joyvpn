<?php

namespace App\Http\Controllers\Api\Components\Poll;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use Illuminate\Support\Facades\Log;

class PostPollAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'question' => "عملکرد سرویس Joy VPN را چه طور ارزیابی می‌کنید؟",
            'options'    => json_encode(['خوبه،کار راه بندازه','نه به درد نمیخوره','هنوز استفاده نکردم']),
            'is_anonymous'  => false,
            'type'  => 'regular'
        ];
        SendTelegramNotif::dispatch($msg, 'poll');
    }
}