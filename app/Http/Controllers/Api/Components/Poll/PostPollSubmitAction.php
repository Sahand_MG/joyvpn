<?php

namespace App\Http\Controllers\Api\Components\Poll;

use App\Classes\AdminNotifier;
use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Poll;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class PostPollSubmitAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $answers = [0 => 1, 1 => -1 , 2 => 0];
        $user = User::where('chat_id', $data->chat_id)->first();
        if(is_null($user)) {
            $user = new User();
            $user->username = $data->username;
            $user->chat_id = $data->chat_id;
            $user->save();
        }
        $poll = new Poll();
        $poll->user_id = $user->id;
        $poll->poll_result = $answers[$data->answer];
        $poll->save();
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'از این که در نظر سنجی شرکت کردید،'."\n".'سپاس گزاریم',
            'reply_markup' => app('bot')->buildKeyBoard(Keyboards::glassyKeyboard()),
        ];
        SendTelegramNotif::dispatch($msg);

        AdminNotifier::pollAnswered($data, $answers[$data->answer]);
    }
}