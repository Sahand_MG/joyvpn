<?php

namespace App\Http\Controllers\Api\Components\Disconnect;

use App\Classes\CacheDB;
use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Accounts;
use App\Models\Server;

class PostDisconnectAccountAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $servers = Server::all();
        $username = app('extractor')->command;
        $u = Accounts::query()->where('username', $username)->first();
        $data = app('extractor');
        if (is_null($u)) {
            $msg = [
                'chat_id' => $data->chat_id,
                'text' => 'کاربر یافت نشد',
                'reply_markup' => app('bot')->buildInlineKeyBoard(Keyboards::mainKeyboard()),
            ];
            SendTelegramNotif::dispatch($msg);
            exit();
        }
        foreach ($servers as $server) {
            $this->sendHttp($server->region, $username);
        }
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'اتصال قطع شد',
            'parse_mode' => 'HTML',
        ];
        CacheDB::clearStack($data->chat_id);
        SendTelegramNotif::dispatch($msg);
    }

    private function sendHttp($region, $username)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $region.':9085?id='.$username,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
    }
}