<?php

namespace App\Http\Controllers\Api\Components\Chat;

use App\Classes\CacheDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostChatIdAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => $data->chat_id
        ];
        SendTelegramNotif::dispatch($msg);
        CacheDB::clearStack($data->chat_id);
        exit();
    }
}