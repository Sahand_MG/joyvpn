<?php

namespace App\Http\Controllers\Api\Components\Plan;

use App\Classes\AdminNotifier;
use App\Classes\Constants;
use App\Classes\Payment\FillPaymentData;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Plan;

class PostPlanACCOUNT3M5UAction extends AbstractComponent
{
    use FillPaymentData, HasSameMessage;
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $plan = Plan::where('name', Constants::ACCOUNT3M5U_TEXT)->first();
        AdminNotifier::newUserOrder($data);
        $this->fillPaymentData($plan, $data, $plan->price, 0);
        $this->sendMessage($data, $plan);
    }
}