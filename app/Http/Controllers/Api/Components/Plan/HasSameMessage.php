<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/11/21
 * Time: 10:22 PM
 */

namespace App\Http\Controllers\Api\Components\Plan;


use App\Classes\Constants;
use App\Jobs\SendTelegramNotif;

trait HasSameMessage
{
    private function sendMessage($data,$plan)
    {
        $msg = [
            'chat_id' => $data->chat_id,
            'photo'  => Constants::RANDOM_IMG[rand(0, count(Constants::RANDOM_IMG) - 1)],
            'caption' => trans('dict.'.$plan->name)
                ."\n".
                'قیمت : '.$plan->price.' تومان '
                ."\n".
                "لطفا جهت ادامه فرایند، شماره موبایل خود را به انگلیسی وارد کنید"
        //            'reply_markup' => app('bot')->buildInlineKeyBoard(Keyboards::mainKeyboard()),
        ];
        SendTelegramNotif::dispatch($msg, 'photo');
    }
}