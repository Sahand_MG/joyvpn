<?php

namespace App\Http\Controllers\Api\Components\Plan;

use App\Classes\AdminNotifier;
use App\Classes\Constants;
use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Accounts;
use Carbon\Carbon;

class PostPlanACCOUNTFreeAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $freeAccount = Accounts::where('user_id', $data->chat_id)->where('used', 1)->where('plan_id', 3)->first();
        if(is_null($freeAccount) || $data->chat_id == 83525910 || $data->chat_id == 974855939) {
            $account = Accounts::where('plan_id', 3)->where('used', 0)->first();
            $account->update(['used' => 1, 'user_id' => $data->chat_id, 'expires_at' => Carbon::now()->addDays(Constants::FREE_ACCOUNT_DAYS)]);
            $msg_text = ' username: ' . $account->username
                . "\n" .
                'password : ' . $account->password
                . "\n" .
                'این اکانت تنها '.Constants::FREE_ACCOUNT_DAYS.' روز اعتبار دارد '
            ;
            $msg = [
                'chat_id' => $data->chat_id,
                'text' => $msg_text,
                'reply_markup' => app('bot')->buildKeyBoard(Keyboards::glassyKeyboard()),
            ];
            SendTelegramNotif::dispatch($msg);
            AdminNotifier::freeAccount();
        }else{
            $msg = [
                'chat_id' => $data->chat_id,
                'text' => 'شما پیش از این اکانت رایگان دریافت کرده‌اید',
                'reply_markup' => app('bot')->buildKeyBoard(Keyboards::glassyKeyboard()),
            ];
            SendTelegramNotif::dispatch($msg);
        }
    }
}