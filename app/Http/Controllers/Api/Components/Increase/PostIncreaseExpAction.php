<?php

namespace App\Http\Controllers\Api\Components\Increase;

use App\Classes\AdminNotifier;
use App\Classes\AppDB;
use App\Classes\CacheDB;
use App\Classes\PlusDB;
use App\Classes\ReceiptMaker;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use Carbon\Carbon;
use Morilog\Jalali\Jalalian;

class PostIncreaseExpAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $remark = strtolower(app('extractor')->command);
        $chat_id = app('extractor')->chat_id;
        $extra_days = config('bot.extra_days');
        $cp = CacheDB::getCompensated();
        if (!is_null($cp) && in_array($remark, $cp)) {
            $this->_sendNotOkNotif($chat_id, '⚠️ پیش از این اعتبار جبرانی به این اکانت تعلق گرفته است');
        }
        if (str_contains($remark, 'ir')) {
            $user = AppDB::getUser($remark);
            if (is_null($user)) {
                $this->_sendNotOkNotif($chat_id, 'حساب یافت نشد');
            } else {
                $gDate = Carbon::parse($user->base_exp)->addDays($extra_days);
                AppDB::setDateExtra($remark, $gDate);
                CacheDB::setCompensatedAccounts($remark);
                $this->_sendOkNotif($chat_id, $extra_days, $remark, $gDate->format('Y-m-d H:i'));
            }
        } elseif (str_contains($remark, 'ip')) {
            $user = PlusDB::getSubAccount2($remark);
            if (is_null($user)) {
                $this->_sendNotOkNotif($chat_id, 'حساب یافت نشد');
            } else {
                $gDate = Carbon::parse($user->exp)->addDays($extra_days);
                PlusDB::setDateExtra($remark, $gDate);
                CacheDB::setCompensatedAccounts($remark);
                $this->_sendOkNotif($chat_id, $extra_days, $remark, $gDate->format('Y-m-d H:i'));
            }
        } else {
            $this->_sendNotOkNotif($chat_id, '⚠️ حساب کاربری باید با ir و یا ip شروع شده باشد');
        }
    }

    private function _sendNotOkNotif($chat_id, $msg)
    {
        $msg = [
            'chat_id' => $chat_id,
            'text' => $msg,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
        exit();
    }

    private function _sendOkNotif($chat_id, $days, $remark, $expiry_date)
    {
        $expiry_date = Jalalian::fromCarbon(Carbon::parse($expiry_date))->format('d-m-Y H:i');
        $receipt = (new ReceiptMaker('', $days, '', $remark, $expiry_date, '', '', '', '', ''))
            ->makeForExpExtra()
            ->addRemark()
            ->getMsg();
        $msg = [
            'chat_id' => $chat_id,
            'text' => $receipt,
            'parse_mode' => 'markdown',
        ];
        SendTelegramNotif::dispatch($msg);
        AdminNotifier::serverStatus($remark. ' got extra days');
    }
}
