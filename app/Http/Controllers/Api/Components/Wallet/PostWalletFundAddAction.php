<?php

namespace App\Http\Controllers\Api\Components\Wallet;

use App\Classes\Constants;
use App\Classes\Keyboards;
use App\Classes\Payment\ZarinpalPayment;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostWalletFundAddAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        // check if input is numerical
        $amount = $arguments;
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'action' => 'typing'
        ];
        SendTelegramNotif::dispatch($msg, 'chat_action');
        $trans = (new ZarinpalPayment($amount))->initiatePayment();
        $msg = [
            'chat_id' => $data->chat_id,
            'photo' => Constants::JVPAY,
            'caption' => 'مبلغ قابل پرداخت (بدون احتساب کارمزد درگاه): ' . $trans->amount . ' تومان ',
//                'reply_markup' => app('bot')->buildInlineKeyBoard(Keyboards::paymentKeyboard( config('bot.paystar_path').'?token='.$data['token'])),
            'reply_markup' => app('bot')->buildInlineKeyBoard(Keyboards::walletChargeKeyboard( config('bot.zarin_path').$trans->authority)),
        ];
        SendTelegramNotif::dispatch($msg, 'photo');
    }
}