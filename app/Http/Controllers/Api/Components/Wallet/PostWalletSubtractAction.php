<?php

namespace App\Http\Controllers\Api\Components\Wallet;

use App\Classes\AdminNotifier;
use App\Classes\AppDB;
use App\Classes\CacheDB;
use App\Classes\PlusDB;
use App\Classes\ReceiptMaker;
use App\Classes\UserDB;
use App\Classes\WalletDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Transaction;
use App\Models\User;
use App\Services\HasVnode;
use App\Services\HttpService;
use Carbon\Carbon;
use Morilog\Jalali\Jalalian;

class PostWalletSubtractAction extends AbstractComponent
{
    use HasVnode;

    public function execute($arguments = null)
    {
        $chat_id = app('extractor')->chat_id;
        $trans = Transaction::query()
            ->where('chat_id', $chat_id)
            ->where('status', 'unpaid')
            ->orderBy('id', 'desc')
            ->first();
        if (is_null($trans)) {
            $msg = [
                'chat_id' => $chat_id,
                'text' => 'تراکنشی جهت پرداخت یافت نشد',
            ];
            SendTelegramNotif::dispatch($msg);
            exit();
        }
        $user = UserDB::getUserByChatId($chat_id);
        if (is_null($user)) {
            $msg = [
                'chat_id' => $chat_id,
                'text' => 'اطلاعات یافت نشد. لطفا دکمه شروع مجدد را بزنید',
            ];
            SendTelegramNotif::dispatch($msg);
            CacheDB::clearStack($chat_id);
            exit();
        }
        $wallet = $user->wallet;
        if (is_null($wallet)) {
            $wallet = WalletDB::createWalletRecord($user);
        }
        $trans_amount = rialsCheck($trans->amount, $trans->trans_id);
        if ($wallet->amount >= $trans_amount) {
            $this->_confirmPayment($trans);
            $wallet->update(['amount' => $wallet->amount - $trans_amount]); // trans amount has no wage
            CacheDB::clearStack($chat_id);
            exit();
        } else {
            $msg = [
                'chat_id' => $chat_id,
                'text' => 'متاسفانه موجودی کیف پول کافی نیست' . "\n" . ' موجودی: ' . $wallet->amount . ' تومان ',
            ];
            SendTelegramNotif::dispatch($msg);
            CacheDB::clearStack($chat_id);
            exit();
        }
    }

    private function _confirmPayment($trans)
    {
        $fqdn = $this->getFqdn($trans->remark);
        if ($fqdn == '0.0.0.0') {
            $this->_sendExpiryAppUpdateRequest($trans->remark);
            $trans->update([
                'status' => 'paid'
            ]);
            $expiry_date = Jalalian::now()->addMonths(1)->format('%B %d، %Y');
            $total_vol = 60;
            $date = Jalalian::fromCarbon(Carbon::parse($trans->created_at))->format('%B %d، %Y');
            $receipt = (new ReceiptMaker($trans->trans_id, $trans->amount, $date, $trans->remark, $expiry_date, $total_vol, $trans->status))
                ->makeForUser()
                ->addExpDate()
                ->addStatus()
                ->addRemark()
                ->addTotalVol()
                ->getMsg();
            $msg = [
                'chat_id' => $trans->chat_id,
                'text' => $receipt,
                'parse_mode' => 'HTML',
            ];
            SendTelegramNotif::dispatch($msg);
            AdminNotifier::orderAcceptedRenewWallet($trans);

        } elseif ($fqdn == '1.1.1.1') {
            $acc = $this->_sendExpiryPlusUpdateRequest($trans);
            $trans->update([
                'status' => 'paid'
            ]);
            $expiry_date = Jalalian::now()->addMonths(1)->format('%B %d، %Y');
            $total_vol = $acc['total'];
            $date = Jalalian::fromCarbon(Carbon::parse($trans->created_at))->format('%B %d، %Y');
            $receipt = (new ReceiptMaker($trans->trans_id, $trans->amount, $date, $trans->remark, $expiry_date, $total_vol, $trans->status))
                ->makeForUser()
                ->addExpDate()
                ->addStatus()
                ->addRemark()
                ->addTotalVol()
                ->getMsg();
            $msg = [
                'chat_id' => $trans->chat_id,
                'text' => $receipt,
                'parse_mode' => 'HTML',
            ];
            SendTelegramNotif::dispatch($msg);
            AdminNotifier::orderAcceptedRenewWallet($trans);
        } else {
            $resp = $this->_sendExpiryUpdateRequest($trans->remark, $fqdn);
            $expiry_date = Jalalian::fromCarbon(Carbon::createFromTimestampMs($resp->data['expiry_time']))->format('%B %d، %Y');
            $trans->update([
                'status' => 'paid'
            ]);
            $total_vol = $resp->data['total'] / pow(10, 9);
            $date = Jalalian::fromCarbon(Carbon::parse($trans->created_at))->format('%B %d، %Y');
            $receipt = (new ReceiptMaker($trans->trans_id, $trans->amount, $date, $trans->remark, $expiry_date, $total_vol, $trans->status))
                ->makeForUser()
                ->addExpDate()
                ->addStatus()
                ->addRemark()
                ->addTotalVol()
                ->getMsg();
            $msg = [
                'chat_id' => $trans->chat_id,
                'text' => $receipt,
                'parse_mode' => 'HTML',
            ];
            SendTelegramNotif::dispatch($msg);
            AdminNotifier::orderAcceptedRenewWallet($trans);
        }
    }

    private function _sendExpiryUpdateRequest($remark, $fqdn)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/expiry' : $fqdn . '/api/expiry';
        $resp = HttpService::sendHttp('POST', $address, ['remark' => $remark, 'agent' => 'user']);
        $std_resp = (object)$resp;
        if ($std_resp->status != 200) {
            $this->msg = 'حساب یافت نشد';
        } else {
            $this->msg = 'Done';
        }
        return $std_resp;
    }

    private function _sendExpiryPlusUpdateRequest($trans)
    {
        $acc = PlusDB::getSubAccount($trans->remark);
        PlusDB::sendUpdateRequest($trans->remark);
        PlusDB::renewIpDateUsageAndExp($trans->remark);
        return $acc['msg'];
    }

    private function _sendExpiryAppUpdateRequest($remark)
    {
        AppDB::renewUserName($remark);
    }
}