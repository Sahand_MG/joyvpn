<?php

namespace App\Http\Controllers\Api\Components\Wallet;

use App\Classes\WalletDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\User;

class PostWalletCheckAction extends AbstractComponent
{
    public $msg;

    public function execute($arguments = null)
    {
        $chat_id = app('extractor')->chat_id;
        $this->msg['chat_id'] = $chat_id;
        $user = User::query()->where('chat_id', $chat_id)->first();
        if (is_null($user)) {
            $this->msg['text'] = 'کاربر یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $wallet = $user->wallet;
        if (is_null($wallet)) {
            $this->_createWalletRecord($user);
            $this->msg['text'] = 'موجودی کیف پول شما: 0 تومان ';
        }else {
            $this->msg['text'] = 'موجودی کیف پول شما: '.$wallet->amount.' تومان ';
        }
        SendTelegramNotif::dispatch($this->msg);
        exit();
    }

    private function _createWalletRecord($user)
    {
        WalletDB::createWalletRecord($user);
    }
}