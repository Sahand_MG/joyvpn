<?php

namespace App\Http\Controllers\Api\Components\Wallet;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostWalletFundAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $chat_id = app('extractor')->chat_id;
        $msg = [
            'chat_id' => $chat_id,
            'text' => 'مبلغ مورد نظر را به تومان و انگلیسی وارد کنید'
        ];
        SendTelegramNotif::dispatch($msg);
    }
}