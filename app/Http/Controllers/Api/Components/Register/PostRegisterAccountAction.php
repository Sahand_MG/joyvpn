<?php

namespace App\Http\Controllers\Api\Components\Register;

use App\Classes\CacheDB;
use App\Classes\Constants;
use App\Classes\PlusDB;
use App\Classes\RemarkDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Remark;
use App\Services\HttpTrait;
use Carbon\Carbon;
use Morilog\Jalali\Jalalian;

class PostRegisterAccountAction extends AbstractComponent
{
    use HttpTrait;
    public $msg = [];

    public function execute($arguments = null)
    {
        $remark = strtolower(app('extractor')->command);
        $chat_id = app('extractor')->chat_id;
        $this->msg = [
            'chat_id' => $chat_id,
            'parse_mode' => 'HTML',
        ];
        if (str_contains($remark, 'ip')) {
            $ac = PlusDB::getSubAccount($remark);
            if(is_null($ac) || $ac['status'] != 200) {
                $this->msg['text'] = 'حساب یافت نشد';
                SendTelegramNotif::dispatch($this->msg);
                exit();
            }
        }
        elseif (str_contains($remark, 'br')) {
            $fqdm = $this->_extractServerAddress($remark, 'br');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } elseif (str_contains($remark, 'no')) {
            $fqdm = $this->_extractServerAddress($remark, 'no');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } elseif (str_contains($remark, 's')) {
            $fqdm = $this->_extractServerAddress($remark, 's');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } elseif (str_contains($remark, 'm')) {
            $fqdm = $this->_extractServerAddress($remark, 'm');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } elseif (str_contains($remark, 'fe')) {
            $fqdm = $this->_extractServerAddress2($remark, 'fe');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } elseif (str_contains($remark, 'cu')) {
            $fqdm = $this->_extractServerAddress2($remark, 'cu');
            $data = $this->_getDataFromRemoteServer($fqdm, $remark);
        } else {
            $this->msg['text'] = 'شناسه اکانت نادرست است';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $remark_record = RemarkDB::getAccountByRemark($remark);
        if (!is_null($remark_record)) {
            $this->msg['text'] = 'این اکانت پیش از این، ثبت شده است ';
            SendTelegramNotif::dispatch($this->msg);
            CacheDB::clearStack($chat_id);
            exit();
        }
        $this->msg['text'] = ' لطفا شماره تماس خود را به انگلیسی وارد کنید ';
        SendTelegramNotif::dispatch($this->msg);
    }

    private function _extractServerAddress($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        if (count($matches[0]) != 2) {
            $this->msg['text'] = 'لطفا نام کاربری را صحیح وارد کنید مثلا br4.1';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $server_id = $matches[0][0];
        $user_id = $matches[0][1];
        try {
            $fqdm = Constants::IRAN_SERVERS[$server_identifier . $server_id];
        } catch (\Exception $exception) {
            info($exception->getMessage());
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        return $fqdm;
    }

    private function _extractServerAddress2($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        if (count($matches[0]) != 1) {
            $this->msg['text'] = 'لطفا نام کاربری را صحیح وارد کنید مثلا br4.1';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $server_id = $matches[0][0];
        try {
            $fqdm = Constants::IRAN_SERVERS[$server_identifier];
        } catch (\Exception $exception) {
            info($exception->getMessage());
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        return $fqdm;
    }

    private function _getDataFromRemoteServer($fqdm, $remark)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/inbound' : $fqdm . '/api/inbound';
        $resp = $this->sendHttp($address, ['remark' => $remark]);
        info(json_encode($resp));
        if (is_null($resp) || $resp->status != 200) {
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $user_data = $resp->data;
        $usage = $user_data->up + $user_data->down;
        $tmp = $usage;
        $gb = pow(10, 9);
        $mb = pow(10, 6);
        $kb = pow(10, 3);
        if ($usage < 1000) {
            $unit = 'B';
        }
        if ($usage > $kb) {
            $tmp = $usage / $kb;
            $unit = 'KB';
        }
        if ($usage > $mb) {
            $tmp = $usage / $mb;
            $unit = 'MB';
        }
        if ($usage > $gb) {
            $tmp = $usage / $gb;
            $unit = 'GB';
        }
        $usage = $tmp;
        if ($user_data->expiry_time == 0) {
            $exp = 'ندارد';
        } else {
            $exp = Jalalian::fromCarbon(Carbon::createFromTimestampMs($user_data->expiry_time))->format('d-m-Y');
        }
        $enable = $user_data->enable;
        $total = $user_data->total / (1000 * pow(10, 6));
        return compact('usage', 'unit', 'exp', 'enable', 'total', 'remark');
    }
}