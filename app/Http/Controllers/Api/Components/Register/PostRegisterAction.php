<?php

namespace App\Http\Controllers\Api\Components\Register;

use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Remark;

class PostRegisterAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
//        Remark::query()->updateOrCreate(['chat_id' => $data->chat_id], ['chat_id' => $data->chat_id]);
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'سیستم‌عامل گوشی خود را انتخاب کنید',
            'reply_markup' => app('bot')->buildInlineKeyBoard(Keyboards::registerKeyboard()),
        ];
        SendTelegramNotif::dispatch($msg);
    }
}