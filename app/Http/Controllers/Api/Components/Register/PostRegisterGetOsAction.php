<?php

namespace App\Http\Controllers\Api\Components\Register;

use App\Classes\Constants;
use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Remark;

class PostRegisterGetOsAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
//        Remark::query()->updateOrCreate(['chat_id' => $data->chat_id], ['os' => Constants::ANDROID ? 'android' : 'ios']);
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'لطفا نام فایل یا اکانتی را که از پشتیبانی خریداری کرده‌اید ارسال کنید',
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }
}