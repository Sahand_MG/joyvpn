<?php

namespace App\Http\Controllers\Api\Components\Register;

use App\Classes\AdminNotifier;
use App\Classes\CacheDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostRegisterPhoneAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
//        Remark::query()->updateOrCreate(['chat_id' => $data->chat_id], ['chat_id' => $data->chat_id]);
        $stack = CacheDB::getStack($data->chat_id);
        info(json_encode($stack));
        $remark = $stack[2];
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'نام‌کاربری '.$remark. ' ثبت شد ',
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
        AdminNotifier::newRegister($remark);
    }
}