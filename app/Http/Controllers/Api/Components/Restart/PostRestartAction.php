<?php

namespace App\Http\Controllers\Api\Components\Restart;

use App\Classes\Extractor\AbstractExtractor;
use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostRestartAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        /**
         * @var $data AbstractExtractor
         */
        $data = app('extractor');
        $keyboard = in_array($data->chat_id, config('bot.admin_ids')) ? Keyboards::adminGlassyKeyboard() : Keyboards::glassyKeyboard();
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => trans('dict.choose'),
            'reply_markup' => app('bot')->buildKeyboard($keyboard),
        ];
        SendTelegramNotif::dispatch($msg);
    }
}