<?php

namespace App\Http\Controllers\Api\Components\Channel;

use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\User;

class PostChannelAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        if(isset($_GET['pass']) && $_GET['pass'] != '9218243') {
            return 'aborted';
        }
        $data = app('extractor');
        try{
            User::query()->whereNotNull('chat_id')->chunk(20, function ($users) {
                foreach ($users as $user) {
                    $this->sendMsg($user->chat_id);
                    info("Sending to ".$user->username);
                }
            });
        }catch (\Exception $exception) {

        }
    }

    private function sendMsg($chatId)
    {
        $msg = [
            'chat_id' => $chatId,
            'text' => "لطفا برای اطلاع از آخرین اخبار و سرورها به لینک کانال مراجعه فرمایید"."\n"."https://t.me/joyvpn",
            'reply_markup' => app('bot')->buildInlineKeyBoard(Keyboards::mainKeyboard()),
        ];
        SendTelegramNotif::dispatch($msg);
    }
}