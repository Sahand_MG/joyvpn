<?php

namespace App\Http\Controllers\Api\Components\Dice;

use App\Classes\Constants;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use Illuminate\Support\Facades\Log;

class PostDiceAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        //        User should send dice emoji to execute this function
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'text'  => Constants::INSPIRING[$data->dice_value]
        ];
        SendTelegramNotif::dispatch($msg);
    }
}