<?php

namespace App\Http\Controllers\Api\Components\Start;

use App\Classes\AdminNotifier;
use App\Classes\Extractor\AbstractExtractor;
use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\User;
use Spatie\Emoji\Emoji;

class PostStartAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        /**
         * @var $data AbstractExtractor
         */
        $data = app('extractor');
        $user = User::where('chat_id', $data->chat_id)->first();
        $this->_saveNewUserData($user, $data);
        AdminNotifier::newUser($data);
        $keyboard = in_array($data->chat_id, config('bot.admin_ids')) ? Keyboards::adminGlassyKeyboard() : Keyboards::glassyKeyboard();
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => trans('dict.welcome'),
            'reply_markup' => app('bot')->buildKeyBoard($keyboard),
        ];
        SendTelegramNotif::dispatch($msg);
    }

    private function _saveNewUserData($user, $data):void
    {
        if (is_null($user)) {
            $user = new User();
            $user->name = $data->fname;
            $user->username = $data->username;
            $user->chat_id = $data->chat_id;
            $user->save();
        }
    }
}