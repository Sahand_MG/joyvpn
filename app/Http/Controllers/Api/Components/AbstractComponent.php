<?php

namespace App\Http\Controllers\Api\Components;

abstract class AbstractComponent
{
    abstract function execute($arguments = null);
}