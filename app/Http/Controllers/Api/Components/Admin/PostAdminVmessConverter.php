<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminVmessConverter extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'لطفا نام کاربری را ارسال کنید',
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }
}