<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\AdminNotifier;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Services\HttpService;

class PostAdminGetSublinkAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $resp = HttpService::sendHttp('GET', config('bot.sub_get_link_url') . '?remark=' . $data->command);
        $msg = [
            'chat_id' => $data->chat_id,
            'parse_mode' => 'markdown',
        ];
        if (is_null($resp) || $resp['status'] != 200) {
            $msg['text'] = 'اکانت یافت نشد';
            SendTelegramNotif::dispatch($msg);
        } else {
            try {
                $qr_url = config('bot.qrcode_url') . $resp['msg'];
                $tmp = file_get_contents($qr_url);
                file_put_contents(storage_path('app/qr/' . $data->command . '.png'), $tmp);
                $msg['photo'] = route('sub.getUrl', ['id' => $data->command]);
                $msg['caption'] = '``` ' . $resp['msg'] . ' ```';
                SendTelegramNotif::dispatch($msg, 'photo');
            } catch (\Exception $exception) {
                AdminNotifier::reportError($exception->getMessage().' '.$exception->getFile().' @'. $exception->getLine());
                $msg['text'] = '``` ' . $resp['msg'] . ' ```';
                SendTelegramNotif::dispatch($msg);
            }
        }
    }
}
