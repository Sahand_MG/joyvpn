<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\AppDB;
use App\Classes\Keyboards;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminDisconnectAppGetDevices extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $username = $data->command;
        $devices = AppDB::getDevices($username);
        $msg = [
            'chat_id' => $data->chat_id,
            'parse_mode' => 'HTML',
        ];
        if (count($devices) == 0) {
            $msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($msg);
        }else {
            info($devices);
            $keyboard = [];
            foreach ($devices as $device) {
                $keyboard[] = [app('bot')->buildInlineKeyBoardButton($device->agent,'', $device->device_id.'_'.$username )];
            }
            $msg['text'] = 'گوشی مورد نظر را انتخاب کنید';
            $msg['reply_markup'] = app('bot')->buildInlineKeyboard($keyboard);
            SendTelegramNotif::dispatch($msg);
        }
    }
}