<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\AppDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminDisconnectAppDeviceAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $t = explode('_', $data->command);
        $device_id = $t[0];
        $username = $t[1];
        $state = AppDB::disconnectDevice($device_id, $username);
        if ($state == 1) {
            $msg['chat_id'] = $data->chat_id;
            $msg['text'] = ' غیر فعال شد '. $device_id;
            SendTelegramNotif::dispatch($msg);
        } else {
            $msg['chat_id'] = $data->chat_id;
            $msg['text'] = 'خطایی پیش آمده. مجدد تلاش کنید';
            SendTelegramNotif::dispatch($msg);
        }
    }
}