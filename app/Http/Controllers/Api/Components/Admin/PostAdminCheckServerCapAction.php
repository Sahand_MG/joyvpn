<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\AppDB;
use App\Classes\Constants;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminCheckServerCapAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $ips = Constants::IRAN_SERVERS;
        $txt = '';
        foreach ($ips as $name => $address) {
            $url = $address.'/sessions';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));
            $response = curl_exec($curl);
            $r = json_decode($response, true);
            if (!is_null($r)) {
                $txt .= $name.': '.$r['sessions'] ."\n";
            }
            curl_close($curl);
        }

        $url = config('bot.app_servers_list_path') ;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        $r = json_decode($response, true);
        if (!is_null($r)) {
            $tmp = [];
            $direct_servers = $r['data']['public_servers'];
            foreach ($direct_servers as $server) {
                if (!in_array($server['address'], $tmp)) {
                    $txt .= $server['name_en'].': '.'('.$server['isp'].') '.$server['current_connections'] ."\n";
                    $tmp[] = $server['address'];
                }
            }
            $msg = [
                'chat_id' => app('extractor')->chat_id,
                'text' => $txt,
            ];
            SendTelegramNotif::dispatch($msg);
        } else {
            $msg = [
                'chat_id' => app('extractor')->chat_id,
                'text' => 'خطا در بررسی لیست سرورها',
            ];
            SendTelegramNotif::dispatch($msg);
        }
    }
}