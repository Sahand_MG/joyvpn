<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\AdminNotifier;
use App\Classes\CacheDB;
use App\Classes\CacheKeys;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Services\HttpService;
use Illuminate\Support\Facades\Cache;

class PostAdminNewSublinkAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $stack = CacheDB::getStack($data->chat_id);
        $resp = HttpService::sendHttp('POST', config('bot.sub_get_new_account_url') , ['username' => $stack[1],'num' => $stack[2]]);
        if (is_null($resp) || $resp['status'] != 200) {
            $msg = [
                'chat_id' => $data->chat_id,
                'text'  => ' حساب یافت نشد',
            ];
            SendTelegramNotif::dispatch($msg);
            exit();
        } else {
            $accounts = $resp['data'];
            $msg = [
                'chat_id' => $data->chat_id,
                'parse_mode' => 'markdown',
            ];
            $msg2 = [
                'chat_id' => $data->chat_id,
                'parse_mode' => 'HTML',
            ];
            foreach ($accounts as $ip => $link)
            {
                try {
                    $qr_url = config('bot.qrcode_url') . $link;
                    $tmp = file_get_contents($qr_url);
                    file_put_contents(storage_path('app/qr/' . $ip . '.png'), $tmp);
                    $msg['photo'] = route('sub.getUrl', ['id' => $ip]);
                    $msg['caption'] = '``` ' . $link . ' ```';
                    $msg2['text'] = $ip;
                    SendTelegramNotif::dispatch($msg2);
                    SendTelegramNotif::dispatch($msg, 'photo');
                } catch (\Exception $exception) {
                    AdminNotifier::reportError($exception->getMessage().' '.$exception->getFile().' @'. $exception->getLine());
                    $msg['text'] = '``` ' . $link . ' ```';
                    SendTelegramNotif::dispatch($msg);
                }
            }
        }
    }
}
