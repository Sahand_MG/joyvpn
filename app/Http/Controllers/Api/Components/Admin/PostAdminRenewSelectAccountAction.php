<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\AdminNotifier;
use App\Classes\AppDB;
use App\Classes\Constants;
use App\Classes\Keyboards;
use App\Classes\PlusDB;
use App\Classes\RemarkDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Models\Plan;
use App\Services\AffiliateService;
use App\Services\GeneralService;
use App\Services\HttpTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\Jalalian;
use Spatie\Emoji\Emoji;

class PostAdminRenewSelectAccountAction extends AbstractComponent
{
    use HttpTrait;
    public $msg;

    public function execute($arguments = null)
    {
        // check if account is registered before
        // check if account is available
        // create transaction record
        // send payment links

        $remark = strtolower(app('extractor')->command);
        $chat_id = app('extractor')->chat_id;
        $this->msg = [
            'chat_id' => $chat_id,
            'parse_mode' => 'HTML',
        ];
        if (str_contains($remark, 'ir')) {
            $this->_sendExpiryUpdateRequestToApp($remark);
            exit();
        }
        elseif (str_contains($remark, 'ip')) {
            $this->_sendExpiryUpdateRequestToPlus($remark);
            exit();
        }
        elseif (str_contains($remark, 'br')) {
            $fqdn = $this->_extractServerAddress($remark, 'br');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } elseif (str_contains($remark, 'no')) {
            $fqdn = $this->_extractServerAddress($remark, 'no');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } elseif (str_contains($remark, 's')) {
            $fqdn = $this->_extractServerAddress($remark, 's');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } elseif (str_contains($remark, 'm')) {
            $fqdn = $this->_extractServerAddress($remark, 'm');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } elseif (str_contains($remark, 'fe')) {
            $fqdn = $this->_extractServerAddress2($remark, 'fe');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } elseif (str_contains($remark, 'cu')) {
            $fqdn = $this->_extractServerAddress2($remark, 'cu');
            $data = $this->_getDataFromRemoteServer($fqdn, $remark);
        } else {
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $resp = $this->_sendExpiryUpdateRequest($remark, $fqdn);
        $expiry_date = Jalalian::fromCarbon(Carbon::createFromTimestampMs($resp->data->expiry_time))->format('%B %d، %Y');
        $total_vol = $resp->data->total/pow(10, 9);
        $date = Jalalian::now()->format('%B %d، %Y');
        $transmission = $data['stream']->network;
        $os = $transmission == 'ws' ? 'ios' : 'android';
        // adding aff credit to master
        $amount = GeneralService::planPriceCalcByVolAndOs($total_vol, '/'.$os);
        AffiliateService::addAffiliateCreditByAdmin($remark, $amount);
        AdminNotifier::orderAcceptedRenewByAdmin($date, $expiry_date, $total_vol, $remark, $os);
    }

    private function _sendExpiryUpdateRequestToApp($remark)
    {
        $acc = AppDB::checkAccountAvailability($remark);
        if (is_null($acc)) {
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $resp = AppDB::renewUserName($remark);
        if ($resp['status'] != 200) {
            $msg = [
                'chat_id' => app('extractor')->chat_id,
                'text' => 'عملیات با خطا مواجه شد',
                'parse_mode' => 'HTML',
            ];
            SendTelegramNotif::dispatch($msg);
            exit();
        }
        $expiry_date = Jalalian::now()->addMonths(1)->format('%B %d، %Y');
        $total_vol = $acc->base_vol;
        $date = Jalalian::now()->format('%B %d، %Y');
        $os = 'android';
        $amount = GeneralService::androidPlanPriceCalcByVol($total_vol);
        AffiliateService::addAffiliateCreditByAdmin($remark, $amount);
        AdminNotifier::orderAcceptedRenewByAdmin($date, $expiry_date, $total_vol, $remark, $os);
    }

    private function _sendExpiryUpdateRequestToPlus($remark)
    {
        $subAccount = PlusDB::getSubAccount($remark);
        if(is_null($subAccount)) {
            $this->msg['text'] = 'کاربر یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $resp = PlusDB::sendUpdateRequest($remark);
        if (isset($resp['status']) && $resp['status'] != 200) {
            AdminNotifier::serverStatus($resp['error']);
            exit();
        } elseif($resp['status'] == 200) {
            $expiry_date = Jalalian::fromCarbon(Carbon::now())->addMonths()->format('%B %d، %Y');
            $total_vol = $subAccount['msg']['total'];
            $date = Jalalian::now()->format('%B %d، %Y');
            $os = 'ios';
            $remark_map = collect($subAccount['msg']['maps'])->pluck('remote_remark')->toArray();
            $amount = GeneralService::iosPlanPriceCalcByVol($total_vol);
            AffiliateService::addAffiliateCreditByAdmin($remark, $amount);
            AdminNotifier::orderAcceptedRenewIPByAdmin($date, $expiry_date, $total_vol, $remark, $os, json_encode($remark_map));
        } else {
            AdminNotifier::serverStatus('خطا در تمدید');
        }
//        PlusDB::renewIpDateUsageAndExp($remark);
    }

    private function _sendExpiryUpdateRequest($remark, $fqdn)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/expiry' : $fqdn . '/api/expiry';
        $resp = $this->sendHttp($address, ['remark' => $remark, 'agent' => 'bot']);
        if ($resp->status != 200) {
            $this->msg = 'حساب یافت نشد';
        } else {
            $this->msg = 'Done';
        }
        return $resp;
    }
    private function _extractServerAddress($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        if (count($matches[0]) != 2) {
            $this->msg['text'] = 'لطفا نام کاربری را صحیح وارد کنید مثلا br4.1';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $server_id = $matches[0][0];
        $user_id = $matches[0][1];
        try {
            $fqdn = Constants::ADMIN_IRAN_SERVERS[$server_identifier . $server_id];
        } catch (\Exception $exception) {
            info($exception->getMessage());
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        return $fqdn;
    }

    private function _extractServerAddress2($remark, $server_identifier)
    {
        preg_match_all('!\d+!', $remark, $matches);
        if (count($matches[0]) != 1) {
            $this->msg['text'] = 'لطفا نام کاربری را صحیح وارد کنید مثلا br4.1';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $server_id = $matches[0][0];
        try {
            $fqdn = Constants::ADMIN_IRAN_SERVERS[$server_identifier];
        } catch (\Exception $exception) {
            info($exception->getMessage());
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        return $fqdn;
    }

    private function _getDataFromRemoteServer($fqdn, $remark)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/inbound' : $fqdn . '/api/inbound';
        $resp = $this->sendHttp($address, ['remark' => $remark]);
        info(json_encode($resp));
        if (is_null($resp) || $resp->status != 200) {
            $this->msg['text'] = 'حساب یافت نشد';
            SendTelegramNotif::dispatch($this->msg);
            exit();
        }
        $user_data = $resp->data;
        $usage = $user_data->up + $user_data->down;
        $tmp = $usage;
        $gb = pow(10, 9);
        $mb = pow(10, 6);
        $kb = pow(10, 3);
        if ($usage < 1000) {
            $unit = 'B';
        }
        if ($usage > $kb) {
            $tmp = $usage / $kb;
            $unit = 'KB';
        }
        if ($usage > $mb) {
            $tmp = $usage / $mb;
            $unit = 'MB';
        }
        if ($usage > $gb) {
            $tmp = $usage / $gb;
            $unit = 'GB';
        }
        $usage = $tmp;
        if ($user_data->expiry_time == 0) {
            $exp = 'ندارد';
        } else {
            $exp = Jalalian::fromCarbon(Carbon::createFromTimestampMs($user_data->expiry_time))->format('d-m-Y');
        }
        $enable = $user_data->enable;
        $total = $user_data->total / (1000 * pow(10, 6));
        $stream = json_decode($user_data->stream_settings);
        return compact('usage', 'unit', 'exp', 'enable', 'total', 'remark', 'stream');
    }
}
