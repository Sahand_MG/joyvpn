<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminNewSublinkGetNum extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'حالا تعداد اکانت را ارسال کنید',
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }
}
