<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\AdminNotifier;
use App\Classes\AppDB;
use App\Classes\CacheDB;
use App\Classes\PlusDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use Morilog\Jalali\Jalalian;

class PostAdminChangeDateAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $remark = strtolower(app('extractor')->command);
        $chat_id = app('extractor')->chat_id;
        $this->msg = [
            'chat_id' => $chat_id,
            'parse_mode' => 'HTML',
        ];
        $date = CacheDB::getStack($chat_id)[1];
        if (str_contains($remark, 'ir')) {
            $acc = AppDB::checkAccountAvailability($remark);
            if (is_null($acc)) {
                $this->msg['text'] = 'حساب یافت نشد';
                SendTelegramNotif::dispatch($this->msg);
                exit();
            }
            AppDB::setDate($remark, $date);
            AdminNotifier::dateChangeByAdmin($date, $remark);
            exit();
        } elseif (str_contains($remark, 'ip')) {
            $subAccount = PlusDB::getSubAccount($remark);
            if (is_null($subAccount) || $subAccount['status'] != 200) {
                $this->msg['text'] = 'خطا در دریافت اطلاعات';
                SendTelegramNotif::dispatch($this->msg);
                exit();
            }
            PlusDB::setDate($remark, $date);
            $date .= ' 00:00 ';
            AdminNotifier::dateChangeByAdmin($date, $remark);
            exit();
        }
    }
}