<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\LotteryDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminCreateLotteryAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        LotteryDB::createLottery($data->command);
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'قرعه‌کشی ساخته شد'
        ];
        SendTelegramNotif::dispatch($msg);
    }
}