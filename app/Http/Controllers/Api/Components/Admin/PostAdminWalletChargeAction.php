<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\AdminNotifier;
use App\Classes\CacheDB;
use App\Classes\RemarkDB;
use App\Classes\UserDB;
use App\Classes\WalletDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminWalletChargeAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $stack = CacheDB::getStack($data->chat_id);
        $amount = $stack[1];
        $mix = $stack[2];
        $chat_id = substr($mix, 2);
        $user = UserDB::getUserByChatId($chat_id);
        if (is_null($user)) {
            $msg = [
                'chat_id' => $user->chat_id,
                'text' => 'کاربر یافت نشد',
                'parse_mode' => 'HTML',
            ];
            SendTelegramNotif::dispatch($msg);
            exit();
        }
        $wallet = $user->wallet;
        if (is_null($wallet)) {
            $wallet = WalletDB::createWalletRecord($user);
        }
        $to_charge = 0;
        if (config('bot.combo_wallet')) {
            $ratio = config('bot.combo_wallet_ratio');
            $to_charge = $amount * $ratio;
            $wallet->update(['amount' => $wallet->amount + $to_charge]);
        } else {
            $to_charge = $amount;
            $wallet->update(['amount' => $wallet->amount + $to_charge]);
        }
        AdminNotifier::adminWalletCharge($to_charge);
        $txt = 'کیف پول شما به مبلغ '.$to_charge.' شارژ شد.';
        $msg = [
            'chat_id' => $user->chat_id,
            'text' => $txt,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }
}