<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\Constants;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use Spatie\Emoji\Emoji;

class PostAdminServerState extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $ips = Constants::IRAN_SERVERS;
        $txt = '';
        $redE = Emoji::redCircle();
        $greenE = Emoji::greenCircle();
        $orangeE = Emoji::orangeCircle();
        $c = 1;
        foreach ($ips as $name => $address) {
            $url = $address.'/status';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));
            $response = curl_exec($curl);
            $r = json_decode($response, true);
            if (!is_null($r)) {
                $txt .= $r['active'] < 40 ? $greenE.' '.$name.': '.$r['active'] :($r['active'] < 50 ? $orangeE.' '.$name.': '.$r['active']: $redE.' '.$name.': '.$r['active']);
                if ($c % 3 == 0) {
                    $txt .= "\n";
                } else {
                    $txt .= ' | ';
                }
            }
            curl_close($curl);
            $c += 1;
        }
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID,
            'text' => $txt,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }
}