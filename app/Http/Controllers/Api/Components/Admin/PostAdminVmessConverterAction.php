<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\CacheDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminVmessConverterAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $username = CacheDB::getStack($data->chat_id)[1];
        $vmess_config = CacheDB::getStack($data->chat_id)[2];
        $jv = 'jv://'.base64_encode($username.'_'.$vmess_config);
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => $jv,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }
}