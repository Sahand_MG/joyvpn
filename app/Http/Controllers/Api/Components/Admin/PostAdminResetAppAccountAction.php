<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\AppDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminResetAppAccountAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $device_id = $data->command;
        $resp = AppDB::resetAccount($device_id);
        if ($resp['status'] != 200) {
            $msg = [
                'chat_id' => $data->chat_id,
                'text' => 'عملیات با خطا مواجه شد',
                'parse_mode' => 'HTML',
            ];
            SendTelegramNotif::dispatch($msg);
            exit();
        }
        $msg['chat_id'] = $data->chat_id;
        $msg['text'] = 'اکانت ریست شد';
        SendTelegramNotif::dispatch($msg);
    }
}