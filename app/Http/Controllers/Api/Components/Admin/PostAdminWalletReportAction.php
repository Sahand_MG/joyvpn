<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\UserDB;
use App\Classes\WalletDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminWalletReportAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $user = UserDB::getUserByChatId($data->command);
        $msg['chat_id'] = $data->chat_id;
        if (is_null($user)) {
            $msg['text'] = 'کاربر یافت نشد';
        } else {
            $wallet = $user->wallet;
            if (is_null($user->wallet)) {
                $msg['text'] = 'کیف پول یافت نشد';
            } else {
                $msg['text'] = $wallet->amount . ' تومان ';
            }
        }
        SendTelegramNotif::dispatch($msg);
    }
}
