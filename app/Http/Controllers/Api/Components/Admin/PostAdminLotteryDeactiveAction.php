<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\LotteryDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminLotteryDeactiveAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $lot = LotteryDB::deactiveLastLottery();
        $lot_name = $lot->name;
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => " قرعه‌کشی $lot_name غیر فعال شد "
        ];
        SendTelegramNotif::dispatch($msg);
    }
}