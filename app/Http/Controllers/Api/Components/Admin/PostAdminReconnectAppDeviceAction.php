<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\AppDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostAdminReconnectAppDeviceAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $t = explode('_', $data->command);
        $device_id = $t[0];
        $username = $t[1];
        if (AppDB::checkIfDeviceIsActive($device_id, $username)) {
            $msg['chat_id'] = $data->chat_id;
            $msg['text'] = 'این دستگاه روی اکانت دیگری فعال است';
            SendTelegramNotif::dispatch($msg);
            exit();
        }
        $state = AppDB::reconnectDevice($device_id, $username);
        if ($state == 1) {
            $msg['chat_id'] = $data->chat_id;
            $msg['text'] = ' فعال شد '. $device_id;
            SendTelegramNotif::dispatch($msg);
        } else {
            $msg['chat_id'] = $data->chat_id;
            $msg['text'] = 'خطایی پیش آمده. مجدد تلاش کنید';
            SendTelegramNotif::dispatch($msg);
        }
    }
}