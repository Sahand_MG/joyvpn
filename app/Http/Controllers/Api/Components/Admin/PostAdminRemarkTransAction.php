<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\RemarkDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use Carbon\Carbon;
use Spatie\Emoji\Emoji;

class PostAdminRemarkTransAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $remark = strtolower(app('extractor')->command);
        $transactions = RemarkDB::getRemarkTransactions($remark);
        $chat_id = app('extractor')->chat_id;
        if($transactions->isEmpty()) {
            $msg = [
                'chat_id' => $chat_id,
                'text' => Emoji::crossMark().' تراکنشی یافت نشد '.Emoji::crossMark(),
                'parse_mode' => 'Html',
            ];
            SendTelegramNotif::dispatch($msg);
        }else{
            $string = Emoji::blueCircle().Emoji::blueCircle()."  اطلاعات تراکنش ها ".Emoji::blueCircle().Emoji::blueCircle()."\n\n";
            foreach ($transactions as $trans){
                $amount = $trans->amount;
                $date = \Morilog\Jalali\Jalalian::fromCarbon(Carbon::parse($trans->created_at))->format('%B %d، %Y، H:i');
                $username = is_null($trans->account) ? $trans->remark :  $trans->account->username;
                $plan = trans('dict.'.$trans->plan->name);
                $string = $string."✅ کد تراکنش : $trans->trans_id
💶 مبلغ پرداخت شده :  تومان $amount
🔅 وضعیت پرداخت:  $trans->status
📆 تاریخ : $date
🦸نام کاربری : $username "."\n"." ================= \n";
            }
            $msg = [
                'chat_id' => $chat_id,
                'text' => $string,
                'parse_mode' => 'Html',
            ];
            SendTelegramNotif::dispatch($msg);

        }
    }
}