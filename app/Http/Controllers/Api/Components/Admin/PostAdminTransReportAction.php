<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Services\HttpService;

class PostAdminTransReportAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $card_number = p2e(app('extractor')->command);
        $url = config('bot.pay_trans_url') . "?page=1&query[card_number]=$card_number&perpage=10";
        $headers = ['Authorization: Bearer ' . env('PAY_BEARER'), 'Content-Type' => 'application/json', 'Accept' => 'application/json'];
        $resp = HttpService::sendHttp('GET', $url, [], $headers);
        if ($resp['status'] != 'ok') {
            $this->_sendTgNotif($resp['message']);
            exit();
        }
        $msg = '';
        $records = $resp['data']['data'];
        if (count($records) == 0) {
            $this->_sendTgNotif('تراکنشی یافت نشد');
        }
        foreach ($records as $tr_record) {
            $msg .= "card number: ";
            $msg .= $tr_record['card_number'] . "\n";
            $msg .= "status: ";
            $msg .= $tr_record['status'] . "\n";
            $msg .= "price: ";
            $msg .= $tr_record['price'] . "\n";
            $msg .= "date: ";
            $msg .= $tr_record['payment_date'] . "\n";
            $msg .= "-------------- \n";
        }

        $this->_sendTgNotif($msg);
    }

    private function _sendTgNotif($txt)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => $txt,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }
}