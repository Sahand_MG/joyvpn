<?php

namespace App\Http\Controllers\Api\Components\Admin;

use App\Classes\Constants;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;
use App\Services\HasVnode;
use App\Services\HttpService;

class GetAdminServerRestartAction extends AbstractComponent
{
    use HasVnode;

    public function execute($arguments = null)
    {
        $data = app('extractor');
        $server_id = $data->command;
        $fqdn = $server_id.'.'.Constants::IRAN_BASE.'/api/restart?pass='.env('RESTART_PASS');
        $resp = HttpService::sendHttp('GET', $fqdn);
        if ($resp == 'ok') {
            $msg = [
                'chat_id' => $data->chat_id,
                'text' => 'سرور ری‌ استارت شد',
                'parse_mode' => 'HTML',
            ];
        } else{
            $msg = [
                'chat_id' => $data->chat_id,
                'text' => 'خطا در اجرای فرمان',
                'parse_mode' => 'HTML',
            ];
        }

        SendTelegramNotif::dispatch($msg);
    }
}