<?php

namespace App\Http\Controllers\Api\Components\Maintenance;

use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostMaintenanceAction extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $msg = [
            'chat_id' => $data->chat_id,
            'text' => 'ربات در حال بروزرسانی است. به زودی بر میگردیم ...'
        ];
        SendTelegramNotif::dispatch($msg);
    }
}