<?php

namespace App\Http\Controllers\Api\Components\Lottery;

use App\Classes\CacheDB;
use App\Classes\LotteryDB;
use App\Classes\UserDB;
use App\Classes\WalletDB;
use App\Http\Controllers\Api\Components\AbstractComponent;
use App\Jobs\SendTelegramNotif;

class PostLottery extends AbstractComponent
{
    public function execute($arguments = null)
    {
        $data = app('extractor');
        $chat_id = $data->chat_id;
        $lot = LotteryDB::getActiveLottery();
        if (is_null($lot)) {
            $msg = [
                'chat_id' => $chat_id,
                'text' => 'درحال حاضر قرعه‌کشی فعالی نداریم. برای اطلاع از تاریخ قرعه‌کشی‌ها کانالمون رو دنبال کنید @joyvpn'
            ];
            SendTelegramNotif::dispatch($msg);
            CacheDB::clearStack($chat_id);
            exit();
        }
        $participant = LotteryDB::getParticipantByChatIdAndLotId($chat_id, $lot->id);
        if (!is_null($participant)) {
            $msg = [
                'chat_id' => $chat_id,
                'text' => 'شما پیش از این در قرعه‌کشی شرکت کرده‌اید'
            ];
            SendTelegramNotif::dispatch($msg);
            CacheDB::clearStack($chat_id);
            exit();
        }
        if ($data->forward_from == '') {
            $msg = [
                'chat_id' => $chat_id,
            ];
            $resp = (app('bot')->sendDice($msg))['result'];
            $dice_val = $resp['dice']['value'];
            sleep(2);
            if ($dice_val == 1) {
                $msg = [
                    'chat_id' => $resp['chat']['id'],
                    'text' => "پوچ☹️ اشکال نداره، دفعه بعد برنده‌ای😌"
                ];
                SendTelegramNotif::dispatch($msg);
                LotteryDB::createParticipant($chat_id, 0, $lot->id);
                exit();
            }
            $prize = $dice_val * 5000;
            LotteryDB::createParticipant($chat_id, $prize, $lot->id);
            $user = UserDB::getUserByChatId($chat_id);
            $wallet = $user->wallet;
            if (is_null($wallet)) {
                $wallet = WalletDB::createWalletRecord($user);
            }
            $wallet->update(['amount' => $wallet->amount + $prize]);
            $msg = [
                'chat_id' => $resp['chat']['id'],
                'text' => "تبریییییک😎 کیف پول شما $prize تومان شارژ شد "
            ];
            SendTelegramNotif::dispatch($msg);
        }
    }
}