<?php

namespace App\Http\Controllers;

use App\Classes\Constants;
use App\Classes\Payment\PaymentDirector;
use App\Classes\Payment\Paystar;
use App\Classes\Payment\PaystarPayment;
use App\Classes\Payment\ZarinpalPayment;
use App\Classes\Payment\ZarinPaymentRenew;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    public function paymentCallback()
    {
        switch (config('bot.payment_service')){
        case Constants::ZARINPAL:
            return (new PaymentDirector(new ZarinpalPayment()))->callback();
                break;
        case Constants::PAYSTAR:
            return (new PaymentDirector(new PaystarPayment()))->callback();
                break;
        default:
            return (new PaymentDirector(new ZarinpalPayment()))->callback();
                break;
        }
    }

    public function paymentWalletCallback()
    {
        return (new PaymentDirector(new ZarinpalPayment()))->callback();
    }

    public function paymentRenewCallback()
    {
        switch (config('bot.payment_service')){
        case Constants::ZARINPAL:
            return (new PaymentDirector(new ZarinpalPayment()))->callback();
                break;
        case Constants::PAYSTAR:
            return (new PaymentDirector(new PaystarPayment()))->callback();
                break;
        default:
            return (new PaymentDirector(new ZarinpalPayment()))->callback();
                break;
        }
    }

    public function paymentRenewNapsternZarinCallback()
    {
        $transactionId = request('Authority');
        $trans = Transaction::where('authority', $transactionId)->first();
        if (is_null($trans)) {
            return 'کد تراکنش نادرست است';
        }
        if ($trans->status == 'paid' || $trans->status == 'canceled') {
            return 'کد تراکنش نادرست است';
        }
        $token = decrypt(request('token'));
        $data = array_values((explode('&', $token)));
        $fqdn = $data[0];
        $vol = $data[1];
        $has_map = $data[2];
        $result = (new ZarinPaymentRenew($trans->amount, $trans->remark, Constants::VOL, $fqdn, $vol, $has_map))->paymentCallback($trans);
        return $result;
    }

    public function paymentRenewNapsternPaystarCallback(Request $request)
    {
        $paymentData = $request->all();
        $transactionId = request('ref_num');
        $trans = Transaction::where('authority', $transactionId)->first();
        if (is_null($trans)) {
           return 'کدتراکنش نادرست است. درصورتی که قبلا این تراکنش را پرداخت نکرده‌اید، مجددا از ربات درخواست تمدید اکانت بدهید';
        }
        if ($trans->status == 'paid' || $trans->status == 'canceled') {
            return 'تراکنش پیش از این پردازش شده است';
        }
        $token = base64_decode(request('token'));
        $data = array_values((explode('&', $token)));
        $fqdn = $data[0];
        $vol = $data[1];
        return (new Paystar($trans->amount, $trans->remark, Constants::VOL, $fqdn, $vol))->paymentCallback($paymentData);
    }

    public function successPayment($transid)
    {
        $trans = Transaction::where('trans_id', $transid)->first();
        if(is_null($trans)) {
            return 'تراکنش نامعتبر';
        }
        $code = $trans->trans_id;
        return view('success', compact('code'));
    }

    public function failedPayment($transid)
    {
        $trans = Transaction::where('trans_id', $transid)->first();
        if(is_null($trans)) {
            return 'تراکنش نامعتبر';
        }
        $code = $trans->trans_id;
        return view('failed', compact('code'));
    }
}
