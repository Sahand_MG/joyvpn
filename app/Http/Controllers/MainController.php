<?php

namespace App\Http\Controllers;

use App\Classes\CacheDB;
use App\Classes\CacheKeys;
use App\Classes\Commands;
use App\Classes\Constants;
use App\Classes\RemarkDB;
use App\Classes\TelegramHelper;
use App\Http\Controllers\Api\Components\Admin\GetAdminServerRestart;
use App\Http\Controllers\Api\Components\Admin\GetAdminServerRestartAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminChangeAccountOwnerAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminChangeAccountOwnerGetDevices;
use App\Http\Controllers\Api\Components\Admin\PostAdminChangeDateAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminChangeDateServer;
use App\Http\Controllers\Api\Components\Admin\PostAdminCheckServerCapAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminCreateLotteryAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminDisconnectAppDeviceAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminDisconnectAppGetDevices;
use App\Http\Controllers\Api\Components\Admin\PostAdminGetSublinkAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminNewSublinkAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminNewSublinkGetNum;
use App\Http\Controllers\Api\Components\Admin\PostAdminReconnectAppDeviceAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminReconnectAppGetDevices;
use App\Http\Controllers\Api\Components\Admin\PostAdminRemarkTransAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminRenewSelectAccountAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminResetAppAccountAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminTransReport;
use App\Http\Controllers\Api\Components\Admin\PostAdminTransReportAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminVmessConverterAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminVmessConverterGetConfig;
use App\Http\Controllers\Api\Components\Admin\PostAdminVol;
use App\Http\Controllers\Api\Components\Admin\PostAdminVolAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminVolChangeAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminVolChangeServer;
use App\Http\Controllers\Api\Components\Admin\PostAdminVolServer;
use App\Http\Controllers\Api\Components\Admin\PostAdminWalletChargeAccount;
use App\Http\Controllers\Api\Components\Admin\PostAdminWalletChargeAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminWalletReportAction;
use App\Http\Controllers\Api\Components\Contact\PostContactDataAction;
use App\Http\Controllers\Api\Components\Disconnect\PostDisconnectAccountAction;
use App\Http\Controllers\Api\Components\Disconnect\PostDisconnectAction;
use App\Http\Controllers\Api\Components\Disconnect\PostDisconnectNapsAccountAction;
use App\Http\Controllers\Api\Components\Increase\PostIncreaseExpAction;
use App\Http\Controllers\Api\Components\Lottery\PostLotteryAction;
use App\Http\Controllers\Api\Components\Maintenance\PostMaintenanceAction;
use App\Http\Controllers\Api\Components\Not\PostNotAllowedAction;
use App\Http\Controllers\Api\Components\Not\PostNotFoundAction;
use App\Http\Controllers\Api\Components\Reconnect\PostReconnectAction;
use App\Http\Controllers\Api\Components\Reconnect\PostReconnectRequestAction;
use App\Http\Controllers\Api\Components\Register\PostRegisterAccountAction;
use App\Http\Controllers\Api\Components\Register\PostRegisterGetOsAction;
use App\Http\Controllers\Api\Components\Register\PostRegisterPhoneAction;
use App\Http\Controllers\Api\Components\Renew\PostRenewNapsCheckAccountAction;
use App\Http\Controllers\Api\Components\Renew\PostRenewSelectAccountAction;
use App\Http\Controllers\Api\Components\Report\PostReportAccountAction;
use App\Http\Controllers\Api\Components\Wallet\PostWalletFundAddAction;
use App\Jobs\SendTelegramNotif;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use function PHPUnit\Framework\stringContains;
use function Symfony\Component\String\length;

class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('channel');
    }

    //    telegram-bot-api --api-id='4539523' --api-hash='f3eebbf0b4d7d187872eae814ecb190e' --local
    public function AcceptTelegramMessageAction()
    {
        try {
            $command = app('extractor')->command;
            Log::alert($command);
            if (config('bot.maintenance')) {
                app()->make(PostMaintenanceAction::class)->execute();
                return;
            }
            if (is_null($command)) {
                app()->make(PostNotAllowedAction::class)->execute();
            } else {
                $class = Commands::getClass($command);
                if ($class !== false) {
                    $chat_id = app('extractor')->chat_id;
                    // set restriction on admin commands
                    if (in_array($command, Commands::getAllAdminCommands())) {
                        if (!in_array($chat_id, config('bot.admin_ids'))) {
                            app()->make(PostNotFoundAction::class)->execute();
                            exit();
                        }
                    }
                    CacheDB::initiateStack($chat_id);
                    CacheDB::addToStack($chat_id, $command);
                    app()->make($class)->execute();
                } else {
                    $chat_id = app('extractor')->chat_id;
                    $stack = CacheDB::getStack($chat_id);
                    if (count($stack) == 0) {
                        app()->make(PostNotFoundAction::class)->execute();
                        exit();
                    }
                    $main_command = $stack[0];
                    if ($main_command == Constants::DISCONNECT || $main_command == Constants::DISCONNECT_FA) {
                        if (str_contains($command, 'ir') || str_contains($command, 'free')) {
                            CacheDB::addToStack($chat_id, $command);
                            app()->make(PostDisconnectAccountAction::class)->execute([$command]);
                        }
                    } elseif ($main_command == Constants::ACCOUNT1M) {
                        if (preg_match("/^\d+$/", $command) && strlen("$command") == 11 && Cache::has(CacheKeys::getPaymentKey())) {
                            CacheDB::addToStack($chat_id, $command);
                            app()->make(PostContactDataAction::class)->execute(['phone', $command]);
                        }
                    } elseif ($main_command == Constants::ACCOUNT1MPC) {
                        if (preg_match("/^\d+$/", $command) && strlen("$command") == 11 && Cache::has(CacheKeys::getPaymentKey())) {
                            CacheDB::addToStack($chat_id, $command);
                            app()->make(PostContactDataAction::class)->execute(['phone', $command]);
                        }
                    } elseif ($main_command == Constants::REGISTER || $main_command == Constants::REGISTER_FA) {
                        if ($command == Constants::IOS || $command == Constants::ANDROID) {
                            app()->make(PostRegisterGetOsAction::class)->execute();
                            CacheDB::addToStack($chat_id, $command);
                            exit();
                        } elseif (preg_match("/^\d+$/", $command) && strlen("$command") == 11) {
                            app()->make(PostRegisterPhoneAction::class)->execute();
                            CacheDB::addToStack($chat_id, $command);
                            RemarkDB::createRemarkRecord($chat_id);
                            CacheDB::clearStack($chat_id);
                            exit();
                        } elseif (isset($stack[1]) && ($stack[1] == Constants::IOS || $stack[1] == Constants::ANDROID)) {
                            app()->make(PostRegisterAccountAction::class)->execute();
                            CacheDB::addToStack($chat_id, $command);
                        }
                        exit();
                    } elseif ($main_command == Constants::WALLET_ADD_FUND || $main_command == Constants::WALLET_ADD_FUND_FA) {
                        if (preg_match("/^\d+$/", $command)) {
                            CacheDB::addToStack($chat_id, $command);
                            app()->make(PostWalletFundAddAction::class)->execute($command);
                            CacheDB::clearStack($chat_id);
                        } else {
                            $txt = 'مبلغ را به صورت عدد و به انگلیسی وارد کنید';
                            $this->_sendTgNotif($txt);
                        }
                        exit();
                    } elseif ($main_command == Constants::ADMIN_WALLET_CHARGE || $main_command == Constants::ADMIN_WALLET_CHARGE_FA) {
                        if (preg_match("/^\d+$/", $command)) {
                            CacheDB::addToStack($chat_id, $command);
                            app()->make(PostAdminWalletChargeAccount::class)->execute();
                        } else {
                            CacheDB::addToStack($chat_id, $command);
                            app()->make(PostAdminWalletChargeAction::class)->execute();
                            CacheDB::clearStack($chat_id);
                            exit();
                        }
                    } elseif ($main_command == Constants::EXTRA_DAYS || $main_command == Constants::EXTRA_DAYS_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostIncreaseExpAction::class)->execute();
                        exit();
                    }
                    elseif ($main_command == Constants::RENEW_NAPS || $main_command == Constants::RENEW_NAPS_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostRenewNapsCheckAccountAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_RENEW_NAPS || $main_command == Constants::ADMIN_RENEW_NAPS_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostAdminRenewSelectAccountAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_WALLET_REPORT_FA || $main_command == Constants::ADMIN_WALLET_REPORT) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostAdminWalletReportAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_ASK_TRANS || $main_command == Constants::ADMIN_ASK_TRANS_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostAdminTransReportAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_LOTTERY_CREATE || $main_command == Constants::ADMIN_LOTTERY_CREATE_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostAdminCreateLotteryAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::REMARK_TRANS || $main_command == Constants::REMARK_TRANS_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostAdminRemarkTransAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_RESTART_SERVER || $main_command == Constants::ADMIN_RESTART_SERVER_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(GetAdminServerRestartAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_SUB_LINK_FA || $main_command == Constants::ADMIN_SUB_LINK) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostAdminGetSublinkAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_ADD_VOL || $main_command == Constants::ADMIN_ADD_VOL_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        if (preg_match("/^\d+$/", $command)) { // receiving vol amount
                            if ($command < 1 && $command > 60) {
                                $txt = 'حجم انتخابی باید بین ۱ تا ۶۰ باشد';
                                $this->_sendTgNotif($txt);
                            }
                            app()->make(PostAdminVolServer::class)->execute();
                            exit();
                        }
                        app()->make(PostAdminVolAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_CONVERT_VMESS || $main_command == Constants::ADMIN_CONVERT_VMESS_FA) {
                        $stack = CacheDB::getStack($chat_id);
                        if (!str_contains($command, 'ir') && count($stack) == 1) {
                            $txt = 'نام کاربری صحیح نیست';
                            $this->_sendTgNotif($txt);
                        }
                        if (!str_contains($command, 'vmess') && count($stack) == 1) {
                            CacheDB::addToStack($chat_id, $command); // saving username
                            app()->make(PostAdminVmessConverterGetConfig::class)->execute();
                            exit();
                        }
                        if (str_contains($command, 'vmess') && count($stack) == 2) {
                            CacheDB::addToStack($chat_id, $command);
                            app()->make(PostAdminVmessConverterAction::class)->execute();
                        } else {
                            $txt = 'فرمت کانفیگ صحیح نیست';
                            $this->_sendTgNotif($txt);
                            exit();
                        }
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_SET_VOL || $main_command == Constants::ADMIN_SET_VOL_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        if (preg_match("/^\d+$/", $command)) { // receiving vol amount
                            if ($command < 1 && $command > 60) {
                                $txt = 'حجم انتخابی باید بین ۱ تا ۶۰ باشد';
                                $this->_sendTgNotif($txt);
                            }
                            app()->make(PostAdminVolChangeServer::class)->execute();
                            exit();
                        }
                        app()->make(PostAdminVolChangeAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    }
                    elseif ($main_command == Constants::ADMIN_SET_DATE || $main_command == Constants::ADMIN_SET_DATE_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        if (preg_match("/^(0[1-9]|[12][0-9]|3[01])/", $command)) { // receiving date
                            app()->make(PostAdminChangeDateServer::class)->execute();
                            exit();
                        }
                        app()->make(PostAdminChangeDateAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    }
                    elseif ($main_command == Constants::RECONNECT || $main_command == Constants::RECONNECT_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostReconnectRequestAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::DISCONNECT_NAPS || $main_command == Constants::DISCONNECT_NAPS_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostDisconnectNapsAccountAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_DISCONNECT_APP_DEVICE_FA || $main_command == Constants::ADMIN_DISCONNECT_APP_DEVICE) {
                        CacheDB::addToStack($chat_id, $command);
                        if (str_starts_with(strtolower($command), 'ir')) {
                            app()->make(PostAdminDisconnectAppGetDevices::class)->execute();
                        } else {
                            app()->make(PostAdminDisconnectAppDeviceAction::class)->execute();
                            CacheDB::clearStack($chat_id);
                        }
                        exit();
                    } elseif ($main_command == Constants::ADMIN_GET_NEW_SUB_FA || $main_command == Constants::ADMIN_GET_NEW_SUB) {
                        $command = p2e($command);
                        CacheDB::addToStack($chat_id, p2e($command));
                        if (!preg_match("/^\d+$/", $command)) {
                            app()->make(PostAdminNewSublinkGetNum::class)->execute();
                        }
                        else {
                            app()->make(PostAdminNewSublinkAction::class)->execute();
                            CacheDB::clearStack($chat_id);
                        }
                        exit();
                    }
                    elseif ($main_command == Constants::ADMIN_CHANGE_ACCOUNT_OWNER_FA || $main_command == Constants::ADMIN_CHANGE_ACCOUNT_OWNER) {
                        CacheDB::addToStack($chat_id, $command);
                        if (str_starts_with(strtolower($command), 'ir')) {
                            app()->make(PostAdminChangeAccountOwnerGetDevices::class)->execute();
                        } else {
                            app()->make(PostAdminChangeAccountOwnerAction::class)->execute();
                            CacheDB::clearStack($chat_id);
                        }
                        exit();
                    } elseif ($main_command == Constants::ADMIN_RESET_APP_ACCOUNT_FA || $main_command == Constants::ADMIN_RESET_APP_ACCOUNT) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostAdminResetAppAccountAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::ADMIN_RECONNECT_APP_DEVICE_FA || $main_command == Constants::ADMIN_RECONNECT_APP_DEVICE) {
                        CacheDB::addToStack($chat_id, $command);
                        if (str_starts_with(strtolower($command), 'ir')) {
                            app()->make(PostAdminReconnectAppGetDevices::class)->execute();
                        } else {
                            app()->make(PostAdminReconnectAppDeviceAction::class)->execute();
                            CacheDB::clearStack($chat_id);
                        }
                        exit();
                    } elseif ($main_command == Constants::ADMIN_SERVER_SESSION_FA || $main_command == Constants::ADMIN_SERVER_SESSION) {
                        app()->make(PostAdminCheckServerCapAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::REPORT || $main_command == Constants::REPORT_FA) {
                        CacheDB::addToStack($chat_id, $command);
                        app()->make(PostReportAccountAction::class)->execute();
                        CacheDB::clearStack($chat_id);
                        exit();
                    } elseif ($main_command == Constants::STATE) {
                        CacheDB::clearStack($chat_id);
                        exit();
                    } else {
                        app()->make(PostNotFoundAction::class)->execute();
                    }
                }
            }
        } catch (\Exception $exception) {
            $msg = [
                'chat_id' => Constants::ADMIN_CHAT_ID,
                'text' => $exception->getMessage() . ' ' . $exception->getFile() . ' ' . $exception->getLine()
            ];
            SendTelegramNotif::dispatch($msg);
            return;
        }
    }

    private function _sendTgNotif($txt)
    {
        $msg = [
            'chat_id' => $chat_id = app('extractor')->chat_id,
            'text' => $txt
        ];
        SendTelegramNotif::dispatch($msg);
        exit();
    }
}
