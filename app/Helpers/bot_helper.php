<?php
use Illuminate\Support\Facades\Cache;

function concat(array $string_array, $delimiter = '')
{
    $tmp = '';
    for($i=0;$i<count($string_array);$i++){
        $tmp = $tmp.$string_array[$i].$delimiter;
    }
    return str_split($tmp,strlen($tmp) - 1)[0];
}

function ConvertJalaliToMiladi($date)
{

    return \Morilog\Jalali\CalendarUtils::createCarbonFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d H:i:s'); //2016-05-8
}

function p2e($input)
{
    $persian = ['۰', '۱', '۲', '۳', '۴', '٤', '۵', '٥', '٦', '۶', '۷', '۸', '۹'];
    $english = [ 0 ,  1 ,  2 ,  3 ,  4 ,  4 ,  5 ,  5 ,  6 ,  6 ,  7 ,  8 ,  9 ];
    return str_replace($persian, $english, $input);
}

function e2p($input)
{
    $persian = ['۰', '۱', '۲', '۳', '۴', '٤', '۵', '٥', '٦', '۶', '۷', '۸', '۹'];
    $english = [ 0 ,  1 ,  2 ,  3 ,  4 ,  4 ,  5 ,  5 ,  6 ,  6 ,  7 ,  8 ,  9 ];
    return str_replace($english,$persian , $input);
}

function addWage($price)
{
    return $price * 1.015 + 120;
}

function subWage($price)
{
    return round(($price - 120) / 1.015);
}

function rialsCheck($price, $transId)
{
    if (str_contains($transId, 'pay_')) {
        $price /= 10;
    }
    return $price;
}


if(!function_exists('user')){

    function user(){

        return \Illuminate\Support\Facades\Auth::user();
    }
}

function grabToken():string
{
    $token = explode(' ', request()->header('authorization'))[1];
    return $token;
}

function check_network_state($network)
{
    $state = Cache::has($network->nuid)?Cache::get($network->nuid):'';
    Cache::forget($network->nuid);
    if($state){
        $network->update(['state' => $state]);
    }
    return $state;
}

function changeTimeStamp(\Carbon\Carbon $ts)
{
    $dst_start_date = \Carbon\Carbon::create(\Carbon\Carbon::now()->year,3,14);
    $dst_end_date = \Carbon\Carbon::create(\Carbon\Carbon::now()->year,11,7);
    if($ts > $dst_start_date && $ts < $dst_end_date){
        return $ts->addHours(4)->addMinutes(30)->timestamp;
    }else{
        return $ts->addHours(3)->addMinutes(30)->timestamp;
    }

}

if(!function_exists('hashid')){

    function hashid($val){
        if(gettype($val) == 'integer'){
            return \Vinkla\Hashids\Facades\Hashids::encode($val);
        }else{
            return \Vinkla\Hashids\Facades\Hashids::decode($val)[0];
        }
    }
}

function d($var){
    Cache::forever('location',debug_backtrace()[0]);
    dd($var);
}