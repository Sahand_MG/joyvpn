<?php

namespace App\Providers;

use App\Classes\Constants;
use App\Classes\Extractor\DataExtractorDirector;
use App\Classes\Extractor\EditedDataExtractor;
use App\Classes\Extractor\InlineDataExtractor;
use App\Classes\Extractor\NormalDataExtractor;
use App\Classes\Extractor\PollAnswerDataExtractor;
use App\Classes\TelegramHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $log = fopen(storage_path('logs/bot.log'),'w');
        fwrite($log,Carbon::now()->format('Y-m-d H:i:s')."\n");
        fwrite($log,print_r(request()->all(),true));
        fclose($log);
        Log::info("========= Incoming Request ==========");
        Log::alert(request()->all());
        Log::info("========= EOR ==========");
        app()->singleton('bot',function(){
            return new TelegramHelper(env('BOT_TOKEN'));
        });
        app()->singleton('extractor',function(){
            if(request()->has(Constants::MESSAGE)){
                return (new DataExtractorDirector(new NormalDataExtractor()))->getExtractor();
            }elseif (request()->has(Constants::CALLBACK_QUERY)){
                return (new DataExtractorDirector(new InlineDataExtractor()))->getExtractor();
            }elseif (request()->has(Constants::EDITED_MESSAGE)){
                return (new DataExtractorDirector(new EditedDataExtractor()))->getExtractor();
            }elseif (request()->has(Constants::POLL_ANSWER)){
                return (new DataExtractorDirector(new PollAnswerDataExtractor()))->getExtractor();
            }
        });
        require_once app_path('Classes/Constants.php');
        require_once app_path('Helpers/bot_helper.php');
    }
}
