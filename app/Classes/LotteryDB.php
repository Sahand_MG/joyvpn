<?php


namespace App\Classes;


use App\Models\Lottery;
use App\Models\LotteryParticipants;
use Illuminate\Support\Str;

class LotteryDB
{
    public static function getParticipantByChatIdAndLotId($chat_id, $lot_id)
    {
        return LotteryParticipants::query()->where('chat_id', $chat_id)->where('lottery_id', $lot_id)->first();
    }

    public static function getActiveLottery()
    {
        return Lottery::query()->where('is_active', 1)->first();
    }

    public static function createParticipant($chat_id, $prize, $lot_id)
    {
        LotteryParticipants::query()->create([
            'chat_id' => $chat_id,
            'prize' => $prize,
            'lottery_id' => $lot_id,
            'is_done' => 1
        ]);
    }

    public static function createLottery($name)
    {
        return Lottery::query()->create([
            'name' => $name,
            'token' => Str::random(10),
            'is_active' => false
        ]);
    }

    public static function activeLastLottery()
    {
        $lot = Lottery::query()->orderBy('id', 'desc')->first();
        $lot->update(['is_active' => 1]);
        return $lot;
    }

    public static function deactiveLastLottery()
    {
        $lot = Lottery::query()->orderBy('id', 'desc')->first();
        $lot->update(['is_active' => 0]);
        return $lot;
    }
}