<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 11:55 AM
 */

namespace App\Classes;


use Spatie\Emoji\Emoji;

class Keyboards
{
    public static function mainKeyboard()
    {
        return [
//            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft().trans('dict.'.Constants::ACCOUNT1M_TEXT).Emoji::backhandIndexPointingRight(),'',Constants::ACCOUNT1M)],
//            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft().trans('dict.'.Constants::ACCOUNT1M_PC_TEXT).Emoji::backhandIndexPointingRight(),'',Constants::ACCOUNT1MPC)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . trans('dict.' . Constants::RENEW_NAPS_TEXT) . Emoji::backhandIndexPointingRight(), '', Constants::RENEW_NAPS)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::pencil() . ' ' . Constants::REGISTER_FA . ' ' . Emoji::pencil(), '', Constants::REGISTER)],
//            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft().' '.Constants::RECONNECT_FA.' '.Emoji::backhandIndexPointingRight(),'',Constants::RECONNECT)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . ' ' . Constants::REPORT_FA . ' ' . Emoji::backhandIndexPointingRight(), '', Constants::REPORT)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . ' ' . Constants::WALLET_CHECK_FA . ' ' . Emoji::backhandIndexPointingRight(), '', Constants::WALLET_CHECK)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . ' ' . Constants::WALLET_ADD_FUND_FA . ' ' . Emoji::backhandIndexPointingRight(), '', Constants::WALLET_ADD_FUND)],
//            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft().trans('dict.'.Constants::ACCOUNT3M2U_TEXT).Emoji::backhandIndexPointingRight(),'',Constants::ACCOUNT3M2U )],
//            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft().trans('dict.'.Constants::ACCOUNT3M10U_TEXT).Emoji::backhandIndexPointingRight(),'',Constants::ACCOUNT3M10U)],
//            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft().trans('dict.'.Constants::ACCOUNT3M5U_TEXT).Emoji::backhandIndexPointingRight(),'',Constants::ACCOUNT3M5U)],
//            [app('bot')->buildInlineKeyBoardButton(Emoji::smilingFaceWithSunglasses().trans('dict.'.Constants::FREE_TEXT).Emoji::smilingFaceWithSunglasses(),'',Constants::FREE)],
//            [app('bot')->buildInlineKeyBoardButton(Emoji::bomb().' '.Constants::DISCONNECT_FA.' '.Emoji::bomb(),'',Constants::DISCONNECT)],
//            [app('bot')->buildInlineKeyBoardButton(Emoji::mobilePhone().trans('dict.clients').Emoji::mobilePhone(), Constants::JOYVPN_CLIENTS)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::headphone() . ' ' . trans('dict.support') . ' ' . Emoji::headphone(), 'https://t.me/JoyVpn_Support')],
            [app('bot')->buildInlineKeyBoardButton(Emoji::speakerMediumVolume() . ' ' . trans('dict.channel') . ' ' . Emoji::speakerMediumVolume(), 'https://t.me/joyvpn')],
        ];
    }

    public static function adminMainKeyboard()
    {
        return [
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . trans('dict.' . Constants::RENEW_NAPS_TEXT) . Emoji::backhandIndexPointingRight(), '', Constants::ADMIN_RENEW_NAPS)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . Constants::ADMIN_ADD_VOL_FA . Emoji::backhandIndexPointingRight(), '', Constants::ADMIN_ADD_VOL)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . Constants::ADMIN_WALLET_CHARGE_FA . Emoji::backhandIndexPointingRight(), '', Constants::ADMIN_WALLET_CHARGE)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . Constants::ADMIN_ASK_TRANS_FA . Emoji::backhandIndexPointingRight(), '', Constants::ADMIN_ASK_TRANS)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . Constants::REMARK_TRANS_FA . Emoji::backhandIndexPointingRight(), '', Constants::REMARK_TRANS)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . ' ' . Constants::REPORT_FA . ' ' . Emoji::backhandIndexPointingRight(), '', Constants::REPORT)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . ' ' . Constants::DISCONNECT_FA . ' ' . Emoji::backhandIndexPointingRight(), '', Constants::DISCONNECT_NAPS)],
            [app('bot')->buildInlineKeyBoardButton(Emoji::backhandIndexPointingLeft() . ' ' . Constants::RECONNECT_FA . ' ' . Emoji::backhandIndexPointingRight(), '', Constants::RECONNECT)],

        ];
    }

    public static function registerKeyboard()
    {
        return [
            [app('bot')->buildInlineKeyBoardButton('Ios', '', Constants::IOS)],
            [app('bot')->buildInlineKeyBoardButton('Android', '', Constants::ANDROID)],
        ];
    }

    public static function paymentKeyboard($gatewayUrl)
    {
        return [
            [
                app('bot')->buildInlineKeyBoardButton(trans('dict.forward_to_gateway'), $gatewayUrl),
            ],
            [
                app('bot')->buildInlineKeyBoardButton(Constants::WALLET_SUBTRACT_FUND_FA, '', Constants::WALLET_SUBTRACT_FUND),
            ],
            [
                app('bot')->buildInlineKeyBoardButton(trans('dict.refused'), '', Constants::REFUSED)
            ],
        ];
    }

    public static function walletChargeKeyboard($gatewayUrl)
    {
        return [
            [
                app('bot')->buildInlineKeyBoardButton(trans('dict.forward_to_gateway'), $gatewayUrl),
            ],
            [
                app('bot')->buildInlineKeyBoardButton(trans('dict.refused'), '', Constants::REFUSED)
            ],
        ];
    }

    public static function renewNapsternKeyBoard()
    {
        return [
            [app('bot')->buildInlineKeyBoardButton(trans('dict.' . Constants::REGISTER_TEXT), '', Constants::REGISTER)],
        ];
    }

    public static function glassyKeyboard()
    {
        return [
            [
                app('bot')->buildKeyboardButton(Constants::XDL_BOT_FA),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::RENEW_NAPS_FA),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::LOTTERY_FA),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::REPORT_FA),
                app('bot')->buildKeyboardButton(Constants::REGISTER_FA),
            ],
            [

                app('bot')->buildKeyboardButton(Constants::WALLET_CHECK_FA),
                app('bot')->buildKeyboardButton(Constants::WALLET_ADD_FUND_FA),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::EXTRA_DAYS_FA),
            ],
            [
                app('bot')->buildKeyboardButton(trans('dict.support')),
                app('bot')->buildKeyboardButton(trans('dict.transactions')),
            ],
            [
                app('bot')->buildKeyboardButton(trans('dict.restart')),
                app('bot')->buildKeyboardButton(Constants::CHAT_ID_FA),
            ]
        ];
    }

    public static function adminGlassyKeyboard()
    {
        return [
            [
                app('bot')->buildKeyboardButton('کیبورد ادمین'),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::ADMIN_RENEW_NAPS_FA),
                app('bot')->buildKeyboardButton(Constants::REPORT_FA),
            ],
            [

                app('bot')->buildKeyboardButton(Constants::ADMIN_SET_VOL_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_SET_DATE_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_ADD_VOL_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_WALLET_CHARGE_FA),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::REMARK_TRANS_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_ASK_TRANS_FA),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::ADMIN_GET_NEW_SUB_FA),
                app('bot')->buildKeyboardButton(Constants::DISCONNECT_NAPS_FA),
                app('bot')->buildKeyboardButton(Constants::RECONNECT_FA),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::ADMIN_LOTTERY_DEACTIVE_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_LOTTERY_ACTIVE_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_LOTTERY_CREATE_FA),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::ADMIN_DISCONNECT_APP_DEVICE_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_RECONNECT_APP_DEVICE_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_CHANGE_ACCOUNT_OWNER_FA),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::ADMIN_SERVER_SESSION_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_WALLET_REPORT_FA),
            ],
            [
                app('bot')->buildKeyboardButton(Constants::ADMIN_CONVERT_VMESS_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_SUB_LINK_FA),
            ],
            [
                app('bot')->buildKeyboardButton(trans('dict.restart')),
                app('bot')->buildKeyboardButton(Constants::ADMIN_RESTART_SERVER_FA),
                app('bot')->buildKeyboardButton(Constants::ADMIN_RESET_APP_ACCOUNT_FA),
            ],
        ];
    }
}
