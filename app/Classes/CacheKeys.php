<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/11/21
 * Time: 12:46 PM
 */

namespace App\Classes;


class CacheKeys
{

    public static function getPaymentKey()
    {
        return app('extractor')->chat_id.'_payment';
    }

    public static function getRenewKey()
    {
        return app('extractor')->chat_id.'_renew';
    }

    public static function getRenewSafeKey()
    {
        return app('extractor')->chat_id.'_renew_safe';
    }
}