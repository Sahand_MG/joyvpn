<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 12:04 PM
 */

namespace App\Classes;


class Constants
{
    const BASE_URL = "http://pay.iclland.ir/";
    const JOYVPN_GUIDE = "http://joyv.fun/";
    const JOYVPN_CLIENTS = "http://joyv.fun/clients";
    const ACCOUNT1M = '/account1m';
    const ACCOUNT1MPC = '/account1mpc';
    const ACCOUNT1M_TEXT = 'account1m';
    const ACCOUNT1M_PC_TEXT = 'account1mpc';
    const ACCOUNT3M2U = '/account3m2u';
    const ACCOUNT3M2U_TEXT = 'account3m2u';
    const ACCOUNT3M10U = '/account3m10u';
    const ACCOUNT3M10U_TEXT = 'account3m10u';
    const ACCOUNT3M5U = '/account3m5u';
    const ACCOUNT3M5U_TEXT = 'account3m5u';
    const FREE = '/accountFree';
    const DISCONNECT = '/disconnect';
    const IOS = '/ios';
    const ANDROID = '/android';
    const REGISTER = '/register';
    const REGISTER_TEXT = 'register';
    const DISCONNECT_TEXT = 'disconnect';
    const FREE_TEXT = 'accountFree';
    const REFUSED = '/refused';
    const RESTART = '/restart';
    const START = '/start';
    const RECONNECT = '/rec';
    const STATE = '/state';
    const DISCONNECT_NAPS = '/disc';
    const DISCONNECT_NAPS_FA = 'قطع اکانت نپسترن';
    const REPORT = '/report';
    const RECONNECT_TEXT = 'reconnect';
    const RECONNECT_FA = 'فعالسازی اکانت نپسترن';
    const RESTART_FA = 'شروع مجدد';
    const TRANSACTION = '/transactions';
    const TRANSACTION_FA = 'لیست تراکنش‌ها';
    const SERVERS = '/servers';
    const SERVERS_FA = 'لیست سرورها';
    const SUPPORT = '/support';
    const SUPPORT_FA = 'تماس با پشتیبانی';
    const CLIENTS = '/clients';
    const CLIENTS_FA = 'نرم افزارها';
    const TUTORIALS = '/tutorials';
    const LOTTERY = '/lottery';
    const LOTTERY_FA = 'شرکت در قرعه‌کشی';
    const TUTORIALS_FA = 'آموزش اتصال و دانلود';
    const DISCONNECT_FA = 'قطع اتصال اکانت';
    const REGISTER_FA = 'ثبت اکانت';
    const REPORT_FA = 'استعلام اکانت';
    const RENEW = '/renew';
    const RENEW_FA = 'تمدید اکانت';
    const EXTRA_DAYS = 'دریافت اعتبار جبرانی';
    const EXTRA_DAYS_FA = 'دریافت اعتبار جبرانی';
    const RENEW_NAPS = '/renew_naps';
    const ADMIN_RENEW_NAPS = '/teddy';
    const ADMIN_RENEW_NAPS_FA = 'تمدید ادمینی';
    const CHAT_ID = '/get_chat_id';
    const CHAT_ID_FA = 'شناسه کاربری';
    const ADMIN_ADD_VOL = '/teddy_bear';
    const ADMIN_ADD_VOL_FA = 'شارژ حجم';
    const ADMIN_SET_VOL = '/admin_vol_set';
    const ADMIN_SET_VOL_FA = 'ست کردن حجم';
    const ADMIN_SET_DATE = '/admin_date_set';
    const ADMIN_SET_DATE_FA = 'ست کردن تاریخ';
    const ADMIN_SUB_LINK = '/admin_get_sub_link';
    const ADMIN_SUB_LINK_FA = 'دریافت لینک اشتراک';
    const ADMIN_CONVERT_VMESS = '/admin_conver_vmess';
    const ADMIN_CONVERT_VMESS_FA = 'تبدیل کانفیگ vmess به jv';
    const ADMIN_ASK_TRANS = '/teddy_trans';
    const ADMIN_ASK_TRANS_FA = 'استعلام شماره کارت';
    const ADMIN_GET_NEW_SUB_FA = 'دریافت اکانت ip';
    const ADMIN_GET_NEW_SUB = '/admin_new_ip';
    const ADMIN_RESTART_SERVER = '/teddy_server_restart';
    const ADMIN_RESTART_SERVER_FA = 'ری استارت سرور';
    const ADMIN_DISCONNECT_APP_DEVICE = '/disconnect_app_device';
    const ADMIN_DISCONNECT_APP_DEVICE_FA = 'غیر فعالسازی اکانت اپ';
    const ADMIN_RECONNECT_APP_DEVICE = '/reconnect_app_device';
    const ADMIN_RECONNECT_APP_DEVICE_FA = 'فعالسازی اکانت اپ';
    const ADMIN_CHANGE_ACCOUNT_OWNER_FA = 'تغییر مالکیت';
    const ADMIN_CHANGE_ACCOUNT_OWNER = '/admin_change_owner';
    const ADMIN_RESET_APP_ACCOUNT = '/admin_reset_account';
    const ADMIN_RESET_APP_ACCOUNT_FA = 'ریست اکانت اپ';
    const ADMIN_SERVER_SESSION = '/admin_server_sessions';
    const ADMIN_SERVER_SESSION_FA = 'استعلام کاربران سرورها';
    const ADMIN_WALLET_REPORT = '/admin_wallet_report';
    const ADMIN_WALLET_REPORT_FA = 'استعلام کیف پول';
    const ADMIN_LOTTERY_CREATE = '/admin_lottery_create';
    const ADMIN_LOTTERY_CREATE_FA = 'ساخت قرعه‌کشی';
    const ADMIN_LOTTERY_ACTIVE_FA = 'فعالسازی قرعه‌کشی';
    const ADMIN_LOTTERY_ACTIVE = '/admin_active_lottery';
    const ADMIN_LOTTERY_DEACTIVE_FA = 'غیر فعالسازی قرعه‌کشی';
    const ADMIN_LOTTERY_DEACTIVE = '/admin_deactive_lottery';
    const RENEW_NAPS_TEXT = 'renew_naps';
    const RENEW_NAPS_FA = 'تمدید اکانت';
    const REMARK_TRANS = '/remark_trans';
    const XDL_BOT = '/xdl_bot';
    const XDL_BOT_FA = 'تبدیل فایل تلگرامی به لینک مستقیم';
    const REMARK_TRANS_FA = 'تراکنش‌های کاربر';
    const TEST = '/test';
    const DICE = '/dice';
    const POLL = '/poll';
    const SUBMIT_POLL = '/submitpoll';
    const GROUP_POLL = '/grouppoll';
    const WALLET_CHECK = '/wallet_check';
    const WALLET_CHECK_FA = 'موجودی کیف پول';
    const WALLET_ADD_FUND = '/add_fund';
    const WALLET_SUBTRACT_FUND = '/sub_fund';
    const WALLET_SUBTRACT_FUND_FA = 'کسر از کیف پول';
    const ADMIN_WALLET_CHARGE = '/teddy_wallet_charge';
    const ADMIN_WALLET_CHARGE_FA = 'شارژ کیف پول کاربر';
    const WALLET_ADD_FUND_FA = 'شارژ کیف پول';
    const CISCO = 'cisco';
    const WALLET = 'wallet';
    const ZARINPAL = 'zarinpal';
    const PAYSTAR = 'paystar';
    const ADMIN_CHAT_ID = '83525910';
    const ADMIN_CHAT_ID2 = '974855939';
    const NORMAL_DAYS = 0;
    const COMBO_DAYS = 15;
    const SUPER_COMBO_DAYS = 30;
    const FREE_ACCOUNT_DAYS = 1;
    const INLINE_QUERY = 'inline_query';
    const VOL = 'vol';
    const EXP = 'exp';
    const MESSAGE_TYPES = [
        'message' => 'sendMessage',
        'dice'    => 'sendDice',
        'poll'    => 'sendPoll',
        'contact'    => 'sendContact',
        'invoice'    => 'sendInvoice',
        'chat_action'   =>  'sendChatAction',
        'photo'   =>  'sendPhoto',
        'delete_message' => 'deleteMessage',
        'animation'      => 'sendAnimation'
    ];
    const RANDOM_IMG = [
        self::BASE_URL.'img/joyvpn-02.png',
        self::BASE_URL.'img/joyvpn-02.png',
        self::BASE_URL.'img/joyvpn-02.png',
        self::BASE_URL.'img/joyvpn-02.png',
        self::BASE_URL.'img/joyvpn-02.png',
        self::BASE_URL.'img/joyvpn-03.png',
        self::BASE_URL.'img/joyvpn-03.png',
        self::BASE_URL.'img/joyvpn-04.png',
        self::BASE_URL.'img/joyvpn-04.png',
        self::BASE_URL.'img/joyvpn-04.png',
        self::BASE_URL.'img/joyvpn-04.png',
        self::BASE_URL.'img/joyvpn-05.png',
        self::BASE_URL.'img/joyvpn-06.png',
        self::BASE_URL.'img/joyvpn-06.png',
        self::BASE_URL.'img/joyvpn-06.png',
        self::BASE_URL.'img/joyvpn-06.png',
        self::BASE_URL.'img/joyvpn-06.png',
        self::BASE_URL.'img/joyvpn-07.png',
        self::BASE_URL.'img/joyvpn-09.png',
        self::BASE_URL.'img/joyvpn-09.png',
        self::BASE_URL.'img/joyvpn-09.png',
        self::BASE_URL.'img/joyvpn-09.png',
        self::BASE_URL.'img/joyvpn-09.png',
        self::BASE_URL.'img/joyvpn-09.png',
        self::BASE_URL.'img/joyvpn-10.png',
        self::BASE_URL.'img/joyvpn-11.png',
        self::BASE_URL.'img/joyvpn-11.png',
        self::BASE_URL.'img/joyvpn-11.png',
        self::BASE_URL.'img/joyvpn-11.png',
        self::BASE_URL.'img/joyvpn-11.png',
        self::BASE_URL.'img/joyvpn-12.png',
        self::BASE_URL.'img/joyvpn-12.png',
        self::BASE_URL.'img/joyvpn-12.png',
        self::BASE_URL.'img/joyvpn-12.png',
    ];
    const ADV_MESSAGE= 'میدونستی میتونی محصولات آرایشی مورد نیازت رو از جوی لوکس با یه قیمت مناسب تهیه کنی😍 تازه با کد **joyv10** روی کل سبد خریدت ۱۰ درصد تخفیفم میگیری!! به https://joylux.ir سر بزن😎';
    const IRAN_BASE = 'jvland.fun';
    const IRAN_BASE2 = 'jvland.fun';
    const IRAN_BASE3 = 'jvland.xyz';
    const IRAN_SERVERS = [
        'br4' => '95.215.173.29',
        'br7' => '193.111.235.90',
        'br10' => '95.215.173.52',
        'br11' => '95.215.173.31',
        'br12' => '185.133.125.162',
        'br14' => '95.215.173.33',
        'br15' => '89.235.119.158',
        'br16' => '95.215.173.62',
        'br18' => '95.215.173.40',
        'br21' => '95.215.173.57',
        'br44' => '95.215.173.57',
        'br38' => '95.215.173.57',
        'br22' => '95.215.173.67',
        'br23' => '5.198.178.243',
        'br24' => '95.215.173.62',
        'br30' => '95.215.173.40',
        'br51' => '95.215.173.57',
        'br8' => '95.215.173.67',
        'br37' => '5.198.178.243',
        'br41' => '5.198.178.243',
        'br25' => '95.215.173.34',
        'm2' => '95.215.173.34',
        'm11' => '95.215.173.34',
        'fe' => '95.215.173.34',
        'br9' => '95.215.173.34',
        'br27' => '5.198.178.179',
        'br28' => '95.215.173.67',
        'br17' => '95.215.173.67',
        'br20' => '95.215.173.67',
        'br31' => '95.215.173.34',
        'br32' => '95.215.173.34',
        'br33' => '195.158.230.93',
        'br39' => '89.235.117.187',
        'br46' => '89.235.117.187',
        'br48' => '89.235.117.187',
        'br56' => '195.158.230.93',
        'br57' => '195.158.230.93',
        'br58' => '37.152.179.177',
        'br59' => '37.152.179.177',
        'br62' => '95.215.173.57',
        'br63' => '185.97.116.227',
        'br5' => '185.97.116.227',
        'br6' => '185.97.116.227',
        'br64' => '185.239.2.28',
        'br65' => '185.239.2.36',
        'br66' => '5.198.179.117',
        'br67' => '5.198.179.120',
        'br68' => '193.111.235.78',
        'br69' => '62.60.204.119',
        'br70' => '5.198.179.117',
        'br71' => '89.235.117.153',
        'pc' => '37.152.177.80',
//         'fe' => 'fe.'.self::IRAN_BASE,
//         // 'cu' => 'cu.'.self::IRAN_BASE,
//         'br4' => 'bridge4.'.self::IRAN_BASE,
//         'br5' => 'bridge5.'.self::IRAN_BASE,
//         'br7' => 'bridge7.'.self::IRAN_BASE,
//         'br8' => 'bridge8.'.self::IRAN_BASE,
//         'br9' => 'bridge9.'.self::IRAN_BASE,
//         'br10' => 'bridge10.'.self::IRAN_BASE,
//         'br11' => 'bridge11.'.self::IRAN_BASE,
//         'br12' => 'bridge12.'.self::IRAN_BASE,
//         'br13' => 'bridge13.'.self::IRAN_BASE,
//         'br14' => 'bridge14.'.self::IRAN_BASE,
//         'br15' => 'bridge15.'.self::IRAN_BASE,
//         'br16' => 'bridge16.'.self::IRAN_BASE,
//         'br17' => 'bridge17.'.self::IRAN_BASE,
//         'br18' => 'bridge18.'.self::IRAN_BASE,
// //        'br19' => 'bridge19.'.self::IRAN_BASE,
//         'br20' => 'bridge20.'.self::IRAN_BASE,
//         'br21' => '185.239.2.46',
//         'br22' => 'bridge22.'.self::IRAN_BASE,
// //        'br23' => 'bridge23.'.self::IRAN_BASE,
//         'br23' => '5.198.178.243',
//         'br24' => 'bridge24.'.self::IRAN_BASE,
//         'br25' => 'bridge25.'.self::IRAN_BASE,
// //        'br26' => 'bridge26.'.self::IRAN_BASE,
//         'br27' => 'bridge27.'.self::IRAN_BASE,
//         'br28' => 'bridge28.'.self::IRAN_BASE,
//         'br30' => 'bridge30.'.self::IRAN_BASE,
//         'br31' => 'bridge31.'.self::IRAN_BASE,
//         'br32' => 'bridge32.'.self::IRAN_BASE,
//         'br33' => 'bridge33.'.self::IRAN_BASE,
// //        'br34' => 'bridge34.'.self::IRAN_BASE,
// //        'br35' => 'bridge35.'.self::IRAN_BASE,
//         // 'br36' => 'bridge36.'.self::IRAN_BASE,
//         'br37' => 'bridge37.'.self::IRAN_BASE,
//         // 'br38' => 'bridge38.'.self::IRAN_BASE,
//         'br39' => 'bridge39.'.self::IRAN_BASE,
// //        'br40' => 'bridge40.'.self::IRAN_BASE,
//         'br41' => 'bridge41.'.self::IRAN_BASE,
//         // 'br42' => 'bridge42.'.self::IRAN_BASE,
//         // 'br44' => 'bridge44.'.self::IRAN_BASE,
// //        'br45' => 'bridge45.'.self::IRAN_BASE,
//         'br46' => 'bridge46.'.self::IRAN_BASE,
//         // 'br47' => 'bridge47.'.self::IRAN_BASE,
//         // 'br49' => 'bridge49.'.self::IRAN_BASE,
//         // 'br50' => 'bridge50.'.self::IRAN_BASE,
//         'br51' => 'bridge51.'.self::IRAN_BASE,
//         // 'br52' => 'bridge52.'.self::IRAN_BASE,
//         // 'br53' => 'bridge53.'.self::IRAN_BASE,
//         // 'br54' => 'bridge54.'.self::IRAN_BASE,
//         'br56' => 'bridge56.'.self::IRAN_BASE,
//         'br57' => 'bridge57.'.self::IRAN_BASE,
//         'br58' => 'bridge58.'.self::IRAN_BASE,
//         'br59' => 'bridge59.'.self::IRAN_BASE,
//         'br64' => 'bridge64.'.self::IRAN_BASE,
//         'br65' => 'bridge65.'.self::IRAN_BASE,
//         'br66' => '5.198.179.117',
//         'br67' => '5.198.179.120',
//         'br68' => 'bridge68.'.self::IRAN_BASE,
//         'br69' => 'bridge69.'.self::IRAN_BASE,
//         'br70' => 'bridge70.'.self::IRAN_BASE,
//         'br71' => '89.235.117.153',
//         // 'br60' => 'bridge60.'.self::IRAN_BASE2,
//         // 'br61' => 'bridge61.'.self::IRAN_BASE2,
//         'br62' => 'bridge62.'.self::IRAN_BASE,
//         'br63' => 'bridge63.'.self::IRAN_BASE2,
//         'br91' => 'bridge91.'.self::IRAN_BASE2,
//         'br100' => 'bridge100.'.self::IRAN_BASE2,
//         'br150' => 'bridge150.'.self::IRAN_BASE2,
//         'br151' => 'bridge151.'.self::IRAN_BASE2,
//         'br152' => 'bridge152.'.self::IRAN_BASE2,
//         'br153' => 'bridge153.'.self::IRAN_BASE2,
//         'br154' => 'bridge154.'.self::IRAN_BASE2,
//         'br155' => 'bridge155.'.self::IRAN_BASE2,
//         // 'm1' => 'm1.'.self::IRAN_BASE,
//         // 'm2' => 'm2.'.self::IRAN_BASE,
//         // 'm3' => 'm3.'.self::IRAN_BASE,
//         // 'm5' => 'm5.'.self::IRAN_BASE,
//         // 'm6' => 'm6.'.self::IRAN_BASE,
//         // 'm8' => 'm8.'.self::IRAN_BASE,
//         // 'm9' => 'm9.'.self::IRAN_BASE,
//         // 'm10' => 'm10.'.self::IRAN_BASE,
//         // 'm11' => 'm11.'.self::IRAN_BASE,
//         // 's1'  => 's1.'.self::IRAN_BASE,
//         // 's2'  => 's2.'.self::IRAN_BASE,
//         // 'no1' => 'no1.'.self::IRAN_BASE,
//         'ip1' => 'ip1.'.self::IRAN_BASE,
//         // 'pc'  => 'pc.'.self::IRAN_BASE,
    ];
    const DIRECT_SERVERS = [
        'br60' => 'bridge60.'.self::IRAN_BASE2,
        'br61' => 'bridge61.'.self::IRAN_BASE2,
    ];
    const ADMIN_IRAN_SERVERS = [
        'br4' => '95.215.173.29',
        'br7' => '193.111.235.90',
        'br10' => '95.215.173.52',
        'br11' => '95.215.173.31',
        'br12' => '185.133.125.162',
        'br14' => '95.215.173.33',
        'br15' => '89.235.119.158',
        'br16' => '95.215.173.62',
        'br18' => '95.215.173.40',
        'br21' => '95.215.173.57',
        'br44' => '95.215.173.57',
        'br38' => '95.215.173.57',
        'br22' => '95.215.173.67',
        'br23' => '5.198.178.243',
        'br24' => '95.215.173.62',
        'br30' => '95.215.173.40',
        'br51' => '95.215.173.57',
        'br8' => '95.215.173.67',
        'br37' => '5.198.178.243',
        'br41' => '5.198.178.243',
        'br25' => '95.215.173.34',
        'm2' => '95.215.173.34',
        'm11' => '95.215.173.34',
        'fe' => '95.215.173.34',
        'br9' => '95.215.173.34',
        'br27' => '5.198.178.179',
        'br28' => '95.215.173.67',
        'br17' => '95.215.173.67',
        'br20' => '95.215.173.67',
        'br31' => '95.215.173.34',
        'br32' => '95.215.173.34',
        'br33' => '195.158.230.93',
        'br39' => '89.235.117.187',
        'br46' => '89.235.117.187',
        'br48' => '89.235.117.187',
        'br56' => '195.158.230.93',
        'br57' => '195.158.230.93',
        'br58' => '37.152.179.177',
        'br59' => '37.152.179.177',
        'br62' => '95.215.173.57',
        'br63' => '185.97.116.227',
        'br5' => '185.97.116.227',
        'br6' => '185.97.116.227',
        'br64' => '185.239.2.28',
        'br65' => '185.239.2.36',
        'br66' => '5.198.179.117',
        'br67' => '5.198.179.120',
        'br68' => '193.111.235.78',
        'br69' => '62.60.204.119',
        'br70' => '5.198.179.117',
        'br71' => '89.235.117.153',
        'pc' => '37.152.177.80',
//         'fe' => 'fe.'.self::IRAN_BASE,
//         // 'cu' => 'cu.'.self::IRAN_BASE,
//         'br4' => 'bridge4.'.self::IRAN_BASE,
//         'br5' => 'bridge5.'.self::IRAN_BASE,
//         'br7' => 'bridge7.'.self::IRAN_BASE,
//         'br8' => 'bridge8.'.self::IRAN_BASE,
//         'br9' => 'bridge9.'.self::IRAN_BASE,
//         'br10' => 'bridge10.'.self::IRAN_BASE,
//         'br11' => 'bridge11.'.self::IRAN_BASE,
//         'br12' => 'bridge12.'.self::IRAN_BASE,
//         'br13' => 'bridge13.'.self::IRAN_BASE,
//         'br14' => 'bridge14.'.self::IRAN_BASE,
//         'br15' => 'bridge15.'.self::IRAN_BASE,
//         'br16' => 'bridge16.'.self::IRAN_BASE,
//         'br17' => 'bridge17.'.self::IRAN_BASE,
//         'br18' => 'bridge18.'.self::IRAN_BASE,
// //        'br19' => 'bridge19.'.self::IRAN_BASE,
//         'br20' => 'bridge20.'.self::IRAN_BASE,
//         'br21' => '185.239.2.46',
//         'br22' => 'bridge22.'.self::IRAN_BASE,
//         'br23' => '5.198.178.243',
//         'br24' => 'bridge24.'.self::IRAN_BASE,
//         'br25' => 'bridge25.'.self::IRAN_BASE,
// //        'br26' => 'bridge26.'.self::IRAN_BASE,
//         'br27' => 'bridge27.'.self::IRAN_BASE,
//         'br28' => 'bridge28.'.self::IRAN_BASE,
//         'br30' => 'bridge30.'.self::IRAN_BASE,
//         'br31' => 'bridge31.'.self::IRAN_BASE,
//         'br32' => 'bridge32.'.self::IRAN_BASE,
//         'br33' => 'bridge33.'.self::IRAN_BASE,
// //        'br34' => 'bridge34.'.self::IRAN_BASE,
// //        'br35' => 'bridge35.'.self::IRAN_BASE,
//         // 'br36' => 'bridge36.'.self::IRAN_BASE,
//         'br37' => 'bridge37.'.self::IRAN_BASE,
//         // 'br38' => 'bridge38.'.self::IRAN_BASE,
//         'br39' => 'bridge39.'.self::IRAN_BASE,
// //        'br40' => 'bridge40.'.self::IRAN_BASE,
//         'br41' => 'bridge41.'.self::IRAN_BASE,
//         // 'br42' => 'bridge42.'.self::IRAN_BASE,
//         // 'br44' => 'bridge44.'.self::IRAN_BASE,
// //        'br45' => 'bridge45.'.self::IRAN_BASE,
//         'br46' => 'bridge46.'.self::IRAN_BASE,
//         // 'br47' => 'bridge47.'.self::IRAN_BASE,
//         // 'br49' => 'bridge49.'.self::IRAN_BASE,
//         // 'br50' => 'bridge50.'.self::IRAN_BASE,
//         'br51' => 'bridge51.'.self::IRAN_BASE,
//         'br52' => 'bridge52.'.self::IRAN_BASE,
//         // 'br53' => 'bridge53.'.self::IRAN_BASE,
//         // 'br54' => 'bridge54.'.self::IRAN_BASE,
//         'br56' => 'bridge56.'.self::IRAN_BASE,
//         'br57' => 'bridge57.'.self::IRAN_BASE,
//         'br58' => 'bridge58.'.self::IRAN_BASE,
//         'br59' => 'bridge59.'.self::IRAN_BASE,
//         'br64' => 'bridge64.'.self::IRAN_BASE,
//         'br65' => 'bridge65.'.self::IRAN_BASE,
//         'br66' => '5.198.179.117',
//         'br67' => '5.198.179.120',
//         'br68' => 'bridge68.'.self::IRAN_BASE,
//         'br69' => 'bridge69.'.self::IRAN_BASE,
//         'br70' => 'bridge70.'.self::IRAN_BASE,
//         'br71' => '89.235.117.153',
        // 'br60' => 'bridge60.'.self::IRAN_BASE2,
        // 'br61' => 'bridge61.'.self::IRAN_BASE2,
        // 'br62' => 'bridge62.'.self::IRAN_BASE,
        // 'br63' => 'bridge63.'.self::IRAN_BASE2,
        // // 'br91' => 'bridge91.'.self::IRAN_BASE2,
        // // 'br100' => 'bridge100.'.self::IRAN_BASE2,
        // 'br150' => 'bridge150.'.self::IRAN_BASE2,
        // 'br151' => 'bridge151.'.self::IRAN_BASE2,
        // // 'br152' => 'bridge152.'.self::IRAN_BASE2,
        // // 'br153' => 'bridge153.'.self::IRAN_BASE2,
        // // 'br154' => 'bridge154.'.self::IRAN_BASE2,
        // // 'br155' => 'bridge155.'.self::IRAN_BASE2,
        // // 'm1' => 'm1.'.self::IRAN_BASE,
        // 'm2' => 'm2.'.self::IRAN_BASE,
        // // 'm3' => 'm3.'.self::IRAN_BASE,
        // // 'm5' => 'm5.'.self::IRAN_BASE,
        // // 'm6' => 'm6.'.self::IRAN_BASE,
        // // 'm8' => 'm8.'.self::IRAN_BASE,
        // // 'm9' => 'm9.'.self::IRAN_BASE,
        // // 'm10' => 'm10.'.self::IRAN_BASE,
        // 'm11' => 'm11.'.self::IRAN_BASE,
        // // 's1'  => 's1.'.self::IRAN_BASE,
        // // 's2'  => 's2.'.self::IRAN_BASE,
        // // 'no1' => 'no1.'.self::IRAN_BASE,
        // 'ip1' => 'ip1.'.self::IRAN_BASE,
        // 'pc'  => 'pc.'.self::IRAN_BASE,
        'app1' => 'app1.'.self::IRAN_BASE2,
        'app2' => 'app2.'.self::IRAN_BASE2,
        'app3' => 'app3.'.self::IRAN_BASE2,
        'app4' => 'app4.'.self::IRAN_BASE2,
        'app5' => 'app5.'.self::IRAN_BASE2,
        'app7' => 'app7.'.self::IRAN_BASE2,
        'app8' => 'app8.'.self::IRAN_BASE2,
        'app9' => 'app9.'.self::IRAN_BASE2,
        'app11' => 'app11.'.self::IRAN_BASE2,
        'app15' => 'app15.'.self::IRAN_BASE2,
        'app16' => 'app16.'.self::IRAN_BASE2,
        'app17' => 'app17.'.self::IRAN_BASE2,
        'app18' => 'app18.'.self::IRAN_BASE2,
        'app19' => 'app19.'.self::IRAN_BASE2,
        'app20' => 'app20.'.self::IRAN_BASE2,
        'app21' => 'app21.'.self::IRAN_BASE2,
        'app22' => 'app22.'.self::IRAN_BASE2,
        'app23' => 'app23.'.self::IRAN_BASE2,
        'app24' => 'app24.'.self::IRAN_BASE2,
        'app25' => 'app25.'.self::IRAN_BASE2,
    ];
    const ANDROID_DIRECT_SERVERS = [];

    const JVPAY = self::BASE_URL.'img/jvpay.png';

    const JV_APP_BANNER = self::BASE_URL.'img/jvapp_banner.png';
    /**
     * Constant for type Callback Query.
     */
    const CALLBACK_QUERY = 'callback_query';
    /**
     * Constant for type Edited Message.
     */
    const EDITED_MESSAGE = 'edited_message';
    /**
     * Constant for type Reply.
     */
    const REPLY = 'reply';
    /**
     * Constant for type Message.
     */
    const MESSAGE = 'message';

    const POLL_ANSWER = 'poll_answer';
    /**
     * Constant for type Photo.
     */
    const PHOTO = 'photo';
    /**
     * Constant for type Video.
     */
    const VIDEO = 'video';
    /**
     * Constant for type Audio.
     */
    const AUDIO = 'audio';
    /**
     * Constant for type Voice.
     */
    const VOICE = 'voice';
    /**
     * Constant for type animation.
     */
    const ANIMATION = 'animation';
    /**
     * Constant for type Document.
     */
    const DOCUMENT = 'document';
    /**
     * Constant for type Location.
     */
    const LOCATION = 'location';
    /**
     * Constant for type Contact.
     */
    const CONTACT = 'contact';
    /**
     * Constant for type Channel Post.
     */
    const CHANNEL_POST = 'channel_post';

    // Payment Statuses
    const PAYMENT_CANCELED = 'canceled';
    const PAYMENT_UNPAID = 'unpaid';
    const PAYMENT_PAID = 'paid';

    // Emoji Constants
    const DICE_EMOJI = "\xf0\x9f\x8e\xb2";

    const INSPIRING = [
        'Act only according to that maxim whereby you can, at the same time, will that it should become a universal law. - Immanuel Kant',
        'An unexamined life is not worth living. - Socrates',
        'Be present above all else. - Naval Ravikant',
        'Happiness is not something readymade. It comes from your own actions. - Dalai Lama',
        'He who is contented is rich. - Laozi',
        'I begin to speak only when I am certain what I will say is not better left unsaid - Cato the Younger',
        'If you do not have a consistent goal in life, you can not live it in a consistent way. - Marcus Aurelius',
        'It is not the man who has too little, but the man who craves more, that is poor. - Seneca',
        'It is quality rather than quantity that matters. - Lucius Annaeus Seneca',
        'Knowing is not enough; we must apply. Being willing is not enough; we must do. - Leonardo da Vinci',
        'Let all your things have their places; let each part of your business have its time. - Benjamin Franklin',
        'No surplus words or unnecessary actions. - Marcus Aurelius',
        'Order your soul. Reduce your wants. - Augustine',
        'People find pleasure in different ways. I find it in keeping my mind clear. - Marcus Aurelius',
        'Simplicity is an acquired taste. - Katharine Gerould',
        'Simplicity is the consequence of refined emotions. - Jean D\'Alembert',
        'Simplicity is the essence of happiness. - Cedric Bledsoe',
        'Simplicity is the ultimate sophistication. - Leonardo da Vinci',
        'Smile, breathe, and go slowly. - Thich Nhat Hanh',
        'The only way to do great work is to love what you do. - Steve Jobs',
        'The whole future lies in uncertainty: live immediately. - Seneca',
        'Very little is needed to make a happy life. - Marcus Antoninus',
        'Waste no more time arguing what a good man should be, be one. - Marcus Aurelius',
        'Well begun is half done. - Aristotle',
        'When there is no desire, all things are at peace. - Laozi',
        'Walk as if you are kissing the Earth with your feet. - Thich Nhat Hanh',
        'Because you are alive, everything is possible. - Thich Nhat Hanh',
        'Breathing in, I calm body and mind. Breathing out, I smile. - Thich Nhat Hanh',
        'Life is available only in the present moment. - Thich Nhat Hanh',
        'The best way to take care of the future is to take care of the present moment. - Thich Nhat Hanh',
    ];
}
