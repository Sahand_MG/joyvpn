<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 2:20 PM
 */

namespace App\Classes;


use App\Classes\Extractor\AbstractExtractor;
use App\Jobs\SendTelegramNotif;
use Carbon\Carbon;
use Morilog\Jalali\Jalalian;

class AdminNotifier
{
    public static function newUser(AbstractExtractor $data)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID,
            'text' => 'New User : ' . $data->username,
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function newUserOrder(AbstractExtractor $data)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID,
            'text' => 'New Order : ' . $data->username
                . "\n" .
                "type : " . $data->command
            ,
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function newTransaction(AbstractExtractor $data)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID2,
            'text' => 'New Order : ' . $data->username
                . "\n" .
                "type : " . $data->command
            ,
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function orderCanceled($trans)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID2,
            'text' => 'Order Canceled : ' . $trans->user->username,
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function orderAccepted($trans)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID2,
            'text' => 'Order Accepted : ' . $trans->user->username,
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function orderAcceptedRenewByAdmin($date, $expiry_date, $total_vol, $remark, $os)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID2,
            'text' => "✅ تمدید توسط ادمین"."\n".
                "🦸نام کاربری : $remark"."\n".
                "📆 تاریخ : $date"."\n".
                "📆 تاریخ انقضا: $expiry_date"."\n".
                "🔋حجم کل: $total_vol گیگ "."\n".
                "📱سیستم عامل: $os" ."\n"
            ,
        ];
        SendTelegramNotif::dispatch($msg); // sending receipt to admin
        $msg['chat_id'] = app('extractor')->chat_id;
        SendTelegramNotif::dispatch($msg);
    }

    public static function orderAcceptedRenewIPByAdmin($date, $expiry_date, $total_vol, $remark, $os, $map)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID2,
            'text' => "✅ تمدید توسط ادمین"."\n".
            "🦸نام کاربری : $remark"."\n".
            "📆 تاریخ : $date"."\n".
            "📆 تاریخ انقضا: $expiry_date"."\n".
            "🔋حجم کل: $total_vol گیگ "."\n".
            "📱سیستم عامل: $os" ."\n".
            "🌉 پل: $map "
        ];
        SendTelegramNotif::dispatch($msg);
        $msg['chat_id'] = app('extractor')->chat_id;
        SendTelegramNotif::dispatch($msg);
    }

    public static function volChangeByAdmin($date, $total_vol, $remark)
    {
        $msg = [
            'text' => "✅ تغییر حجم"."\n".
                "🦸نام کاربری : $remark"."\n".
                "📆 تاریخ : $date"."\n".
                "🔋حجم کل: $total_vol گیگ "
        ];
        $msg['chat_id'] = app('extractor')->chat_id;
        SendTelegramNotif::dispatch($msg);
    }

    public static function dateChangeByAdmin($date, $remark)
    {
        $msg = [
            'text' => "✅ تغییر تاریخ"."\n".
                "🦸نام کاربری : $remark"."\n".
                "📆 تاریخ انقضا : $date"."\n"
        ];
        $msg['chat_id'] = app('extractor')->chat_id;
        SendTelegramNotif::dispatch($msg);
    }

    public static function orderAcceptedVolByAdmin($date, $expiry_date, $total_vol, $remark, $os)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID2,
            'text' => "✅ شارژ حجم توسط ادمین
            📆 تاریخ : $date
            📆 تاریخ انقضا: $expiry_date
            🔋حجم کل: $total_vol گیگ
            🦸نام کاربری : $remark
            📱سیستم عامل: $os "
            ,
        ];
        $msg['chat_id'] = app('extractor')->chat_id;
        SendTelegramNotif::dispatch($msg);
    }

    public static function orderAcceptedRenew($trans)
    {
        $trans = (object)$trans;
        $date = Jalalian::fromCarbon(Carbon::parse($trans->updated_at))->format('d-m-Y H:i:s');
        $account = RemarkDB::getAccountByRemark($trans->remark);
        $os = is_null($account) ? 'ثبت نشده' : $account->os;
        $cardNumber = @unserialize($trans->meta)['data']['card_number'];
        $amount = $trans->amount;
        $msg = (new ReceiptMaker($trans->trans_id, $amount, $date, $trans->remark, '', 0, $trans->status, $cardNumber, $os, 'تمدید'))
            ->makeForAdmin()
            ->addCardNumber()
            ->addStatus()
            ->addRemark()
            ->addOs()
            ->addType()
            ->getMsg();
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID2,
            'text' => $msg,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function orderAcceptedRenewWallet($trans)
    {
        $trans = (object)$trans;
        $date = Jalalian::fromCarbon(Carbon::parse($trans->updated_at))->format('d-m-Y H:i:s');
        $account = RemarkDB::getAccountByRemark($trans->remark);
        $os = is_null($account) ? 'ثبت نشده' : $account->os;
        $cardNumber = @unserialize($trans->meta)['data']['card_number'];
        $amount = $trans->amount / 10;
        $msg = (new ReceiptMaker($trans->trans_id, $amount, $date, $trans->remark, '', 0, $trans->status, $cardNumber, $os, 'تمدید'))
            ->makeForPayWithWallet()
            ->addCardNumber()
            ->addStatus()
            ->addRemark()
            ->addOs()
            ->addType()
            ->getMsg();
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID2,
            'text' => $msg,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function adminWalletCharge($amount, $remark = '')
    {
        $date = Jalalian::now()->format('d-m-Y H:i:s');
        $msg = (new ReceiptMaker('JvWallet', $amount, $date, $remark, '', 0, '', '', '', 'کیف پول'))
            ->makeForChargeWallet()
            ->addRemark()
            ->addType()
            ->getMsg();
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID2,
            'text' => $msg,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function paymentStatus($trans)
    {
        $trans = (object)$trans;
        $date = Jalalian::fromCarbon(Carbon::parse($trans->updated_at)->addHours(3)->addMinutes(30))->format('d-m-Y H:i:s');
        $amount = $trans->amount / 10;
        $cardNumber = @unserialize($trans->meta)['data']['card_number'];
        $account = RemarkDB::getAccountByRemark($trans->remark);
        $os = is_null($account) ? 'ثبت نشده' : $account->os;
        $msg = (new ReceiptMaker($trans->trans_id, $amount, $date, $trans->remark, '', 0, $trans->status, $cardNumber, $os, 'لینک پرداخت'))
            ->makeForAdmin()
            ->addCardNumber()
            ->addStatus()
            ->addRemark()
            ->addType()
            ->getMsg();
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID2,
            'text' => $msg,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function serverStatus($msg)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID,
            'text' => $msg,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function freeAccount()
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID,
            'text' => 'Free Account For : ' . app('extractor')->username,
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function reminderAlarm($account)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID,
            'text' => 'Account reminded : ' . $account->username,
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function expireAlarm($account)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID,
            'text' => 'Account expired : ' . $account->username,
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function pollAnswered($data, $answer)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID,
            'text' => 'Answered By : ' . $data->username . "\n"
                . 'Answer : ' . $answer
            ,
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function newRegister($account)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID,
            'text' => $account . ' registered',
        ];
        SendTelegramNotif::dispatch($msg);
    }

    public static function reportError($txt)
    {
        $msg = [
            'chat_id' => Constants::ADMIN_CHAT_ID,
            'text' => $txt,
        ];
        SendTelegramNotif::dispatch($msg);
    }
}
