<?php


namespace App\Classes;


use App\Services\HttpService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\Jalalian;

class AppDB
{
    public static function renewUserName($username)
    {
        $url = config('bot.renew_app');
        return HttpService::sendHttp('POST', $url, ['username' => $username]);
    }

    public static function checkAccountAvailability($remark)
    {
        return DB::connection('mysql2')
            ->table('users')
            ->where('username', $remark)
            ->first();
    }

    public static function activateUserName($username)
    {
        DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->join('devices', 'users.id', '=', 'devices.user_id')
            ->update(['devices.is_active' => 1, 'users.is_active' => 1]);
    }

    public static function deactivateUserName($username)
    {
        DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->join('devices', 'users.id', '=', 'devices.user_id')
            ->update(['devices.is_active' => 0]);
    }

    public static function checkIfDeviceIsActive($did, $username)
    {// check if device is active on other accounts
        return DB::connection('mysql2')
            ->table('users')
            ->join('devices', 'users.id', '=', 'devices.user_id')
            ->where('device_id', $did)
            ->where('username', '!=', $username)
            ->where('devices.is_active', 1)
            ->count() == 0 ? false : true;
    }

    public static function reportUserName($username)
    {
        return DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->join('usages', 'users.id', '=', 'usages.user_id')
            ->join('devices', 'usages.device_id', '=', 'devices.id')
            ->select(['users.username', 'users.base_exp', 'users.base_vol', 'devices.is_active as device_active', 'devices.is_owner', 'users.is_active as user_active', 'devices.app_version', 'users.code', 'usages.vol', 'usages.exp', 'devices.agent', 'devices.device_id'])
            ->get();
    }

    public static function getDevices($username)
    {
        return DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->join('devices', 'devices.user_id', '=', 'users.id')
            ->select(['devices.device_id', 'agent'])
            ->get();
    }

    public static function disconnectDevice($device_id, $username)
    {
        return DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->join('devices', 'devices.user_id', '=', 'users.id')
            ->where('device_id', $device_id)
            ->update(['devices.is_active' => 0]);
    }

    public static function reconnectDevice($device_id, $username)
    {
        return DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->join('devices', 'devices.user_id', '=', 'users.id')
            ->where('device_id', $device_id)
            ->update(['devices.is_active' => 1, 'users.is_active' => 1]);
    }

    public static function resetAccount($username)
    {
        $url = config('bot.reset_app');
        return HttpService::sendHttp('POST', $url, ['username' => $username]);
    }

    public static function changeOwnerToThisDevice($device_id, $username)
    {
        $a = DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->join('devices', 'devices.user_id', '=', 'users.id')
            ->update(['devices.is_owner' => 0]);

        $b = DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->join('devices', 'devices.user_id', '=', 'users.id')
            ->where('device_id', $device_id)
            ->update(['devices.is_owner' => 1]);

        return $a & $b;
    }

    public static function setVol($username, $vol)
    {
        return DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->update(['base_vol' => $vol]);
    }

    public static function setDate($username, $jdate)
    {
        $gDate = Jalalian::fromFormat('Y-m-d', $jdate)->toCarbon();
        return DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->update(['base_exp' => $gDate]);
    }

    public static function setDateExtra($username, $gDate)
    {
        return DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->update(['base_exp' => $gDate]);
    }

    public static function getUser($username)
    {
        return DB::connection('mysql2')
            ->table('users')
            ->where('username', $username)
            ->first();
    }

    public static function getAppServerData($address)
    {
        return DB::connection('mysql2')
            ->table('servers')
            ->where('address', $address)
            ->first();
    }
}