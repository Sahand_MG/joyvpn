<?php

namespace App\Classes\Builders;

class RenewAccount
{
    private $username;
    private $account;
    private $initiate;

    public function setUserName($username)
    {
        $this->username = $username;
    }

    public function getUserName()
    {
        return $this->username;
    }

    public function setAccount($account)
    {
        $this->account = $account;
    }

    public function getAccount()
    {
        return $this->account;
    }
}