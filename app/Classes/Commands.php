<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 2:13 AM
 */

namespace App\Classes;


use App\Http\Controllers\Api\Components\Admin\GetAdminServerRestart;
use App\Http\Controllers\Api\Components\Admin\PostAdminChangeAccountOwner;
use App\Http\Controllers\Api\Components\Admin\PostAdminChangeAccountOwnerAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminChangeDate;
use App\Http\Controllers\Api\Components\Admin\PostAdminCheckServerCapAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminCreateLottery;
use App\Http\Controllers\Api\Components\Admin\PostAdminDisconnectAppDevice;
use App\Http\Controllers\Api\Components\Admin\PostAdminGetSublink;
use App\Http\Controllers\Api\Components\Admin\PostAdminLotteryActiveAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminLotteryDeactiveAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminNewSublink;
use App\Http\Controllers\Api\Components\Admin\PostAdminRecconnectAppDevice;
use App\Http\Controllers\Api\Components\Admin\PostAdminRemarkTrans;
use App\Http\Controllers\Api\Components\Admin\PostAdminRenewAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminResetAppAccount;
use App\Http\Controllers\Api\Components\Admin\PostAdminResetAppAccountAction;
use App\Http\Controllers\Api\Components\Admin\PostAdminServerState;
use App\Http\Controllers\Api\Components\Admin\PostAdminTransReport;
use App\Http\Controllers\Api\Components\Admin\PostAdminVmessConverter;
use App\Http\Controllers\Api\Components\Admin\PostAdminVol;
use App\Http\Controllers\Api\Components\Admin\PostAdminVolChange;
use App\Http\Controllers\Api\Components\Admin\PostAdminVolServer;
use App\Http\Controllers\Api\Components\Admin\PostAdminWalletCharge;
use App\Http\Controllers\Api\Components\Admin\PostAdminWalletReport;
use App\Http\Controllers\Api\Components\Chat\PostChatIdAction;
use App\Http\Controllers\Api\Components\Clients\PostClientsAction;
use App\Http\Controllers\Api\Components\Dice\PostDiceAction;
use App\Http\Controllers\Api\Components\Disconnect\PostDisconnectAction;
use App\Http\Controllers\Api\Components\Disconnect\PostDisconnectNapsAction;
use App\Http\Controllers\Api\Components\Increase\PostIncreaseExp;
use App\Http\Controllers\Api\Components\Listing\PostListingServers;
use App\Http\Controllers\Api\Components\Listing\PostListingTransactions;
use App\Http\Controllers\Api\Components\Lottery\PostLottery;
use App\Http\Controllers\Api\Components\Payment\PostPaymentRefused;
use App\Http\Controllers\Api\Components\Plan\PostPlanACCOUNT1MAction;
use App\Http\Controllers\Api\Components\Plan\PostPlanACCOUNT1MPCAction;
use App\Http\Controllers\Api\Components\Plan\PostPlanACCOUNT3M10UAction;
use App\Http\Controllers\Api\Components\Plan\PostPlanACCOUNT3M2UAction;
use App\Http\Controllers\Api\Components\Plan\PostPlanACCOUNT3M5UAction;
use App\Http\Controllers\Api\Components\Plan\PostPlanACCOUNTFreeAction;
use App\Http\Controllers\Api\Components\Poll\PostPollAction;
use App\Http\Controllers\Api\Components\Poll\PostPollGroupAction;
use App\Http\Controllers\Api\Components\Poll\PostPollSubmitAction;
use App\Http\Controllers\Api\Components\Reconnect\PostReconnectAction;
use App\Http\Controllers\Api\Components\Register\PostRegisterAction;
use App\Http\Controllers\Api\Components\Renew\PostRenewAction;
use App\Http\Controllers\Api\Components\Renew\PostRenewNapsAction;
use App\Http\Controllers\Api\Components\Report\PostReportAction;
use App\Http\Controllers\Api\Components\Restart\PostRestartAction;
use App\Http\Controllers\Api\Components\Start\PostStartAction;
use App\Http\Controllers\Api\Components\Support\PostSupportAction;
use App\Http\Controllers\Api\Components\Test\PostTestAction;
use App\Http\Controllers\Api\Components\Tutorial\PostTutorialAction;
use App\Http\Controllers\Api\Components\Wallet\PostWalletCheckAction;
use App\Http\Controllers\Api\Components\Wallet\PostWalletFundAction;
use App\Http\Controllers\Api\Components\Wallet\PostWalletFundAddAction;
use App\Http\Controllers\Api\Components\Wallet\PostWalletSubtractAction;
use App\Http\Controllers\Api\Components\Xdl\PostXdlAction;

class Commands
{
//    List of allowed commands
    private static $mapping = [
        Constants::START            => PostStartAction::class,
        Constants::RESTART          => PostRestartAction::class,
//        Constants::SERVERS          => PostListingServers::class,
        Constants::TRANSACTION      => PostListingTransactions::class,
        Constants::SUPPORT          => PostSupportAction::class,
        Constants::XDL_BOT          => PostXdlAction::class,
//        Constants::CLIENTS          => PostClientsAction::class,
//        Constants::TUTORIALS        => PostTutorialAction::class,
        Constants::RENEW            => PostRenewAction::class,
//        Constants::DISCONNECT       => PostDisconnectAction::class,
        Constants::REGISTER         => PostRegisterAction::class,
        Constants::RENEW_NAPS       => PostRenewNapsAction::class,
        Constants::REPORT           => PostReportAction::class,
        Constants::WALLET_CHECK     => PostWalletCheckAction::class,
        Constants::WALLET_ADD_FUND  => PostWalletFundAction::class,
        Constants::LOTTERY          => PostLottery::class,
        Constants::EXTRA_DAYS       => PostIncreaseExp::class,
        Constants::CHAT_ID          => PostChatIdAction::class,
    ];

    private static $private_mapping = [
//        Constants::ACCOUNT1M        => PostPlanACCOUNT1MAction::class,
//        Constants::ACCOUNT1MPC      => PostPlanACCOUNT1MPCAction::class,
//        Constants::ACCOUNT3M5U    => PostPlanACCOUNT3M5UAction::class,
//        Constants::ACCOUNT3M2U    => PostPlanACCOUNT3M2UAction::class,
//        Constants::ACCOUNT3M10U   => PostPlanACCOUNT3M10UAction::class,
//        Constants::FREE             => PostPlanACCOUNTFreeAction::class,
        Constants::DISCONNECT => PostDisconnectAction::class,
        Constants::DISCONNECT_FA => PostDisconnectAction::class,
        Constants::REGISTER => PostRegisterAction::class,
        Constants::REGISTER_FA => PostRegisterAction::class,
        Constants::REPORT_FA => PostReportAction::class,
        Constants::REFUSED => PostPaymentRefused::class,
        Constants::SUPPORT_FA => PostSupportAction::class,
        Constants::XDL_BOT_FA => PostXdlAction::class,
        Constants::CLIENTS_FA => PostClientsAction::class,
        Constants::TRANSACTION_FA => PostListingTransactions::class,
        Constants::SERVERS_FA => PostListingServers::class,
        Constants::RESTART_FA => PostRestartAction::class,
        Constants::TUTORIALS_FA => PostTutorialAction::class,
        Constants::RENEW_FA => PostRenewAction::class,
        Constants::RENEW_NAPS_FA => PostRenewNapsAction::class,
        Constants::TEST => PostTestAction::class,
        Constants::DICE => PostDiceAction::class,
        Constants::LOTTERY_FA => PostLottery::class,
        Constants::CHAT_ID_FA => PostChatIdAction::class,
        Constants::EXTRA_DAYS => PostIncreaseExp::class,
        Constants::EXTRA_DAYS_FA => PostIncreaseExp::class,
        Constants::POLL => PostPollAction::class,
        Constants::SUBMIT_POLL => PostPollSubmitAction::class,
        Constants::WALLET_SUBTRACT_FUND  => PostWalletSubtractAction::class,
        Constants::GROUP_POLL => PostPollGroupAction::class,
        Constants::ADMIN_RENEW_NAPS => PostAdminRenewAction::class,
        Constants::ADMIN_RENEW_NAPS_FA => PostAdminRenewAction::class,
        Constants::ADMIN_ADD_VOL => PostAdminVol::class,
        Constants::ADMIN_ADD_VOL_FA => PostAdminVol::class,
        Constants::ADMIN_ASK_TRANS => PostAdminTransReport::class,
        Constants::REMARK_TRANS => PostAdminRemarkTrans::class,
        Constants::REMARK_TRANS_FA => PostAdminRemarkTrans::class,
        Constants::ADMIN_ASK_TRANS_FA => PostAdminRemarkTrans::class,
        Constants::RECONNECT => PostReconnectAction::class,
        Constants::RECONNECT_FA => PostReconnectAction::class,
        Constants::DISCONNECT_NAPS => PostDisconnectNapsAction::class,
        Constants::DISCONNECT_NAPS_FA => PostDisconnectNapsAction::class,
        Constants::STATE => PostAdminServerState::class,
        Constants::ADMIN_WALLET_CHARGE  => PostAdminWalletCharge::class,
        Constants::ADMIN_WALLET_CHARGE_FA  => PostAdminWalletCharge::class,
        Constants::WALLET_CHECK_FA     => PostWalletCheckAction::class,
        Constants::WALLET_ADD_FUND_FA     => PostWalletFundAction::class,
        Constants::ADMIN_RESTART_SERVER_FA  => GetAdminServerRestart::class,
        Constants::ADMIN_DISCONNECT_APP_DEVICE_FA  => PostAdminDisconnectAppDevice::class,
        Constants::ADMIN_RECONNECT_APP_DEVICE_FA  => PostAdminRecconnectAppDevice::class,
        Constants::ADMIN_RESET_APP_ACCOUNT_FA  => PostAdminResetAppAccount::class,
        Constants::ADMIN_SERVER_SESSION_FA  => PostAdminCheckServerCapAction::class,
        Constants::ADMIN_WALLET_REPORT_FA  => PostAdminWalletReport::class,
        Constants::ADMIN_LOTTERY_CREATE_FA  => PostAdminCreateLottery::class,
        Constants::ADMIN_LOTTERY_ACTIVE_FA  => PostAdminLotteryActiveAction::class,
        Constants::ADMIN_LOTTERY_DEACTIVE_FA  => PostAdminLotteryDeactiveAction::class,
        Constants::ADMIN_CHANGE_ACCOUNT_OWNER_FA  => PostAdminChangeAccountOwner::class,
        Constants::ADMIN_SET_VOL_FA  => PostAdminVolChange::class,
        Constants::ADMIN_SET_DATE_FA  => PostAdminChangeDate::class,
        Constants::ADMIN_CONVERT_VMESS_FA  => PostAdminVmessConverter::class,
        Constants::ADMIN_SUB_LINK_FA  => PostAdminGetSublink::class,
        Constants::ADMIN_GET_NEW_SUB_FA  => PostAdminNewSublink::class,
    ];

    private static $admin_commands = [
        Constants::ADMIN_RENEW_NAPS => PostAdminRenewAction::class,
        Constants::ADMIN_RENEW_NAPS_FA => PostAdminRenewAction::class,
        Constants::ADMIN_ASK_TRANS => PostAdminTransReport::class,
        Constants::ADMIN_ASK_TRANS_FA => PostAdminTransReport::class,
        Constants::REMARK_TRANS => PostAdminRemarkTrans::class,
        Constants::REMARK_TRANS_FA => PostAdminRemarkTrans::class,
        Constants::ADMIN_ADD_VOL => PostAdminVol::class,
        Constants::ADMIN_ADD_VOL_FA => PostAdminVol::class,
        Constants::ADMIN_WALLET_CHARGE  => PostAdminWalletCharge::class,
        Constants::ADMIN_WALLET_CHARGE_FA  => PostAdminWalletCharge::class,
        Constants::ADMIN_RESTART_SERVER  => GetAdminServerRestart::class,
        Constants::ADMIN_RESTART_SERVER_FA  => GetAdminServerRestart::class,
        Constants::ADMIN_DISCONNECT_APP_DEVICE_FA  => PostAdminDisconnectAppDevice::class,
        Constants::ADMIN_RESET_APP_ACCOUNT_FA  => PostAdminResetAppAccount::class,
        Constants::ADMIN_SERVER_SESSION_FA  => PostAdminCheckServerCapAction::class,
        Constants::ADMIN_WALLET_REPORT_FA  => PostAdminWalletReport::class,
        Constants::ADMIN_LOTTERY_CREATE_FA  => PostAdminCreateLottery::class,
        Constants::ADMIN_LOTTERY_ACTIVE_FA  => PostAdminLotteryActiveAction::class,
        Constants::ADMIN_LOTTERY_DEACTIVE_FA  => PostAdminLotteryDeactiveAction::class,
        Constants::ADMIN_CHANGE_ACCOUNT_OWNER_FA  => PostAdminChangeAccountOwner::class,
        Constants::ADMIN_SET_VOL_FA  => PostAdminVolChange::class,
        Constants::ADMIN_SET_DATE_FA  => PostAdminChangeDate::class,
        Constants::ADMIN_CONVERT_VMESS_FA  => PostAdminVmessConverter::class,
        Constants::ADMIN_SUB_LINK_FA  => PostAdminGetSublink::class,
        Constants::ADMIN_GET_NEW_SUB_FA  => PostAdminNewSublink::class,
    ];

    public static function getClass($command)
    {
        return in_array($command, self::getAllCommands()) ? self::$mapping[$command] :
            (in_array($command, self::getAllPrivateCommands()) ? self::$private_mapping[$command] : false);
    }

    public static function getAllCommands()
    {
        return array_keys(self::$mapping);
    }

    public static function getAllPrivateCommands()
    {
        return array_keys(self::$private_mapping);
    }

    public static function getAllPrivates()
    {
        return self::$private_mapping;
    }

    public static function getAllAdminCommands()
    {
        return array_keys(self::$admin_commands);
    }
}
