<?php

namespace App\Classes;


use App\Services\HasVnode;
use App\Services\HttpService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\Jalalian;

class PlusDB
{
    use HasVnode;

    public static function getAccountReport($remark, $agent = 'user')
    {

        $url = config('bot.sub_report_url') . '?remark=' . $remark . '&agent=' . $agent;
        return HttpService::sendHttp('GET', $url);
    }

    public static function deactivateUserName($remark)
    {
        $url = config('bot.sub_get_url') . '?remark=' . $remark;
        $resp = HttpService::sendHttp('GET', $url);
        if ($resp['status'] != 200) {
            return 1;
        }
        $maps = $resp['msg']['maps'];
        $error = 0;
        foreach ($maps as $map) {
            $r = $map['remote_remark'];
            if (str_contains($r, '-')) {
                $r = explode('-', $r)[1];
            }
            $a = new self();
            $resp = $a->sendDisconnectRequest($map['remote_address'], $r);
            if (count($resp) == 0 || $resp['status'] != 200) {
                $error = 1;
            }
        }
        DB::connection('mysql_plus')
            ->table('subscriptions')
            ->where('remark', $remark)
            ->update(['enable' => 0]);
        return $error;
    }

    public static function activateUserName($remark)
    {
        $url = config('bot.sub_get_url') . '?remark=' . $remark;
        $resp = HttpService::sendHttp('GET', $url);
        $error = 0;
        if ($resp['status'] != 200) {
            return 1;
        }
        $maps = $resp['msg']['maps'];
        foreach ($maps as $map) {
            $r = $map['remote_remark'];
            if ($map['remote_isp'] != 'common' || $map['remote_isp'] != 'server4') {
                continue;
            }
            if (str_contains($r, '-')) {
                $r = explode('-', $r)[1];
            }
            $a = new self();
            try {
                $resp = $a->sendReconnectRequest($map['remote_address'], $r);
                if (count($resp) == 0 || $resp['status'] != 200) {
                    $error = 1;
                }
            }catch (\Exception $exception) {
              info($exception->getMessage().' while reconnecting ip user');
            }
        }
        DB::connection('mysql_plus')
            ->table('subscriptions')
            ->where('remark', $remark)
            ->update(['enable' => 1]);
        return $error;
    }

    public static function getSubAccount($remark)
    {
        $url = config('bot.sub_get_url') . '?remark=' . $remark;
        return HttpService::sendHttp('GET', $url);
    }

    public static function getSubAccount2($remark)
    {
        return DB::connection('mysql_plus')
            ->table('subscriptions')
            ->where('remark', $remark)
            ->first();
    }

    public static function sendUpdateRequest($remark)
    {
        // DONT update from bot. some consideration needed in plus while renewing
//        $sub = self::getSubAccount($remark);
//        $maps = $sub['msg']['maps'];
//        $remark_map = [];
//        foreach ($maps as $map) { // updating associated ip accounts
//            $base_url = $map['remote_address'];
//            $remark = $map['remote_remark'];
//            $remark_map[] = $remark;
//            if (str_contains($base_url, 'tikmag.ir')) {
//                $base_url = str_replace('tikmag.ir', 'joyvpn.fun', $base_url);
//            }
//            $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/expiry' : $base_url . '/api/expiry';
//            if (str_contains($remark, '-')) {
//                $remark = explode('-', $remark)[1];
//            }
//            HttpService::sendHttp('POST', $address, ['remark' => $remark, 'agent' => 'admin']);
//
//        }
        $url = config('bot.sub_renew_url');
        return HttpService::sendHttp('POST', $url, ['remark' => $remark]);
    }

    public static function renewIpDateUsageAndExp($remark)
    {
        $exp = Carbon::parse(Jalalian::now()->addMonths(1)->toCarbon());
        DB::connection('mysql_plus')
            ->table('subscriptions')
            ->where('remark', $remark)
            ->update(['usage' => 0, 'exp' => $exp, 'enable' => 1]);
    }

    public static function setVol($remark, $vol)
    {
        $sub = self::getSubAccount($remark);
        $maps = $sub['msg']['maps'];
        $remark_map = [];
        DB::connection('mysql_plus')
            ->table('subscriptions')
            ->where('remark', $remark)
            ->update(['total' => $vol]);
        foreach ($maps as $map) { // updating associated ip accounts
            $base_url = $map['remote_address'];
            $remark = $map['remote_remark'];
            if ($map['remote_isp'] == 'server4' || $map['remote_isp'] == 'common') {
                $remark_map[] = $remark;
                if (str_contains($base_url, 'tikmag.ir')) {
                    $base_url = str_replace('tikmag.ir', 'jvland.fun', $base_url);
                }
                $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/change-vol' : $base_url . '/api/change-vol';
                if (str_contains($remark, '-')) {
                    $remark = explode('-', $remark)[1];
                }
                HttpService::sendHttp('POST', $address, ['remark' => $remark, 'vol' => $vol * 1.066]);
            }
        }
    }

    public static function setDate($remark, $jdate)
    {
        $gDate = Jalalian::fromFormat('Y-m-d', $jdate)->toCarbon();
        DB::connection('mysql_plus')
            ->table('subscriptions')
            ->where('remark', $remark)
            ->update(['exp' => $gDate]);
        $add = config('bot.change_date_plus_path');
        HttpService::sendHttp('POST', $add, ['remark' => $remark, 'date' => $jdate]);

    }

    public static function setDateExtra($remark, $gDate)
    {
        DB::connection('mysql_plus')
            ->table('subscriptions')
            ->where('remark', $remark)
            ->update(['exp' => $gDate]);
        $extra_days = config('bot.extra_days');
        $add = config('bot.extra_days_plus_path');
        HttpService::sendHttp('POST', $add, ['remark' => $remark, 'days' => $extra_days]);
    }

    public static function getAffiliate($remark, $chat_id)
    {
        $aff = DB::connection('mysql_plus')
            ->table('affiliates')
            ->where('referral_username', $remark)
            ->first();
        if ($aff->master_username == $chat_id) {
            return null;
        }
        return $aff;
    }
}
