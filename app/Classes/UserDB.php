<?php


namespace App\Classes;


use App\Models\User;

class UserDB
{
    public static function getUserByChatId($chatId)
    {
        return User::query()->where('chat_id', $chatId)->first();
    }
}