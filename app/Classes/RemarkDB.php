<?php


namespace App\Classes;


use App\Models\Remark;
use App\Models\Transaction;

class RemarkDB
{
    public static function createRemarkRecord($chatId)
    {
        $stack = CacheDB::getStack($chatId);
        Remark::query()->create([
            'os' => $stack[1],
            'remark' => $stack[2],
            'phone' => $stack[3],
            'chat_id' => $chatId
        ]);
    }

    public static function getAccountByRemark($remark)
    {
        return Remark::query()->where('remark', $remark)->first();
    }

    public static function getRemarkTransactions($remark)
    {
        return Transaction::query()
            ->where('remark', $remark)
            ->orderBy('id', 'desc')
            ->take(5)
            ->get();
    }
}