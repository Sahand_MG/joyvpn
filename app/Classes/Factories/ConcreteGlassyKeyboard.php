<?php

namespace App\Classes\Factories;

class ConcreteGlassyKeyboard extends AbstractKeyboard
{
    public function createKeyboard()
    {
        return new GlassyKeyboard();
    }
}