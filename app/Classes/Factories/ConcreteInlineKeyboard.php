<?php

namespace App\Classes\Factories;

class ConcreteInlineKeyboard extends AbstractKeyboard
{
    public function createKeyboard()
    {
        return new InlineKeyboard();
    }
}