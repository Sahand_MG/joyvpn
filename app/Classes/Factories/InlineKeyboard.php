<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 11:33 AM
 */

namespace App\Classes\Factories;


class InlineKeyboard implements KeyboardContract
{
    public function make(int $rows,array $buttons)
    {
        app('bot')->buildInlineKeyBoard([
            [app('bot')->buildInlineKeyBoardButton(''),app('bot')->buildInlineKeyBoardButton('شروع مجدد')]
        ]);
    }
}