<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 11:41 AM
 */

namespace App\Classes\Factories;


interface KeyboardContract
{
    public function make(int $rows, array $buttons);
}