<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 11:31 AM
 */

namespace App\Classes\Factories;


abstract class AbstractKeyboard
{
    public abstract function createKeyboard();
}