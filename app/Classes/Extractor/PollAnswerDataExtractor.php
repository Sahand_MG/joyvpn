<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 12:31 PM
 */

namespace App\Classes\Extractor;


use App\Classes\Constants;

class PollAnswerDataExtractor extends AbstractExtractor
{
    public function extractCommandText()
    {
        $this->command = Constants::SUBMIT_POLL;
    }

    public function extractSenderData()
    {
        $this->fname = request()->all()['poll_answer']['user']['first_name'];
    }

    public function extractSenderChatId()
    {
        $this->chat_id = request()->all()['poll_answer']['user']['id'];
    }

    public function extractUsername()
    {
        $this->username = request()->all()['poll_answer']['user']['username'];
    }

    public function extractDate()
    {
        $this->date = request()->all()['poll_answer']['user']['date'];
    }

    public function extractPollAnswer()
    {
        $this->answer = request()->all()['poll_answer']['option_ids'][0];
    }

    public function extractMessageId()
    {
        $this->message_id = request()->all()['poll_answer']['message']['message_id'];
    }

    public function extractDiceValue(){}
}