<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 12:31 PM
 */

namespace App\Classes\Extractor;


use App\Classes\Constants;

class NormalDataExtractor extends AbstractExtractor
{

    public function extractCommandText()
    {
        if(isset(request()->all()['message']['text'])){
            $this->command = request()->all()['message']['text'];
        }elseif (isset(request()->all()['message']['dice'])){
            $this->command = Constants::DICE;
        }
    }

    public function extractSenderData()
    {
        $this->fname = request()->all()['message']['from']['first_name'];
    }

    public function extractSenderChatId()
    {
        $this->chat_id = request()->all()['message']['from']['id'];
    }

    public function extractUsername()
    {
        $this->username = request()->all()['message']['from']['username'];
    }

    public function extractDate()
    {
        $this->date = request()->all()['message']['date'];
    }

    public function extractDiceValue()
    {
        $this->dice_value = request()->all()['message']['dice']['value'];
    }

    public function extractMessageId()
    {
        $this->message_id = request()->all()['message']['message_id'];
    }

    public function extractForwardFrom()
    {
        $this->forward_from = request()->all()['message']['forward_from'];
    }

    public function extractPollAnswer(){}
}