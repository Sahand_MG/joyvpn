<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 12:30 PM
 */

namespace App\Classes\Extractor;


class DataExtractorDirector
{
    protected $builder;

    public function __construct(AbstractExtractor $builder)
    {
        $this->builder = $builder;
        $this->extractData();
    }

    private function extractData()
    {
        @$this->builder->extractCommandText();
        @$this->builder->extractDate();
        @$this->builder->extractUsername();
        @$this->builder->extractSenderChatId();
        @$this->builder->extractSenderData();
        @$this->builder->extractDiceValue();
        @$this->builder->extractPollAnswer();
        @$this->builder->extractMessageId();
    }

    public function getExtractor() : AbstractExtractor
    {
        return $this->builder;
    }

}