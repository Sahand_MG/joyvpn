<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 12:31 PM
 */

namespace App\Classes\Extractor;


class InlineDataExtractor extends AbstractExtractor
{

    public function extractCommandText()
    {
        $this->command = request()->all()['callback_query']['data'];
    }

    public function extractSenderData()
    {
        $this->fname = request()->all()['callback_query']['from']['first_name'];
    }

    public function extractSenderChatId()
    {
        $this->chat_id = request()->all()['callback_query']['from']['id'];
    }

    public function extractUsername()
    {
        $this->username = request()->all()['callback_query']['from']['username'];
    }

    public function extractDate()
    {
        $this->date = request()->all()['callback_query']['from']['date'];
    }

    public function extractMessageId()
    {
        $this->message_id = request()->all()['callback_query']['message']['message_id'];
    }

    public function extractDiceValue(){}

    public function extractPollAnswer(){}
}