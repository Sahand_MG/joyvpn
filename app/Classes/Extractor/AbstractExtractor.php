<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 2:30 AM
 */

namespace App\Classes\Extractor;


abstract class AbstractExtractor
{
    public $command;
    public $fname;
    public $chat_id;
    public $username;
    public $date;
    public $dice_value;
    public $answer;
    public $message_id;
    public $forward_from;

    abstract public function extractCommandText();

    abstract public function extractSenderData();

    abstract public function extractSenderChatId();

    abstract public function extractUsername();

    abstract public function extractDate();

    abstract public function extractDiceValue();

    abstract public function extractPollAnswer();

    abstract public function extractMessageId();
}