<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 12:31 PM
 */

namespace App\Classes\Extractor;


class EditedDataExtractor extends AbstractExtractor
{
    public function extractCommandText()
    {
        $this->command = request()->all()['edited_message']['text'];
    }

    public function extractSenderData()
    {
        $this->fname = request()->all()['edited_message']['from']['first_name'];
    }

    public function extractSenderChatId()
    {
        $this->chat_id = request()->all()['edited_message']['from']['id'];
    }

    public function extractUsername()
    {
        $this->username = request()->all()['edited_message']['from']['username'];
    }

    public function extractDate()
    {
        $this->date = request()->all()['edited_message']['from']['date'];
    }

    public function extractMessageId()
    {
        $this->message_id = request()->all()['edited_message']['message_id'];
    }

    public function extractDiceValue(){}

    public function extractPollAnswer(){}
}