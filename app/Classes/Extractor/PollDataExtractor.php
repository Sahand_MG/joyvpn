<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 12:31 PM
 */

namespace App\Classes\Extractor;


class PollDataExtractor extends AbstractExtractor
{
    public function extractCommandText()
    {
        $this->command = request()->all()['poll']['/pollanswer'];
    }

    public function extractSenderData()
    {
        $this->fname = request()->all()['poll']['user']['first_name'];
    }

    public function extractSenderChatId()
    {
        $this->chat_id = request()->all()['poll']['user']['id'];
    }

    public function extractUsername()
    {
        $this->username = request()->all()['poll']['user']['username'];
    }

    public function extractDate()
    {
        $this->date = request()->all()['poll']['from']['date'];
    }

    public function extractMessageId()
    {
        $this->message_id = request()->all()['poll']['message']['message_id'];
    }

    public function extractDiceValue(){}

    public function extractPollAnswer(){}
}