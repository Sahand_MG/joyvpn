<?php

namespace App\Classes\Payment;

class PaymentDirector
{
    /**
     * @var $builder AbstractPayment
     */
    protected $builder;

    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    public function setPrice($price)
    {
        $this->builder->price = $price;
    }

    public function setData($data)
    {
        $this->builder->data = $data;
    }

    public function setPlan($plan)
    {
        $this->builder->plan = $plan;
    }

    public function setRenew($renew)
    {
        $this->builder->renew = $renew;
    }

    public function getRenew($renew)
    {
        $this->builder->renew = $renew;
    }

    public function setTransaction($trans)
    {
        $this->builder->trans = $trans;
    }

    public function getTransaction()
    {
        return $this->builder->trans;
    }

    public function getPlan()
    {
        return $this->builder->plan;
    }

    public function pay()
    {
        return $this->builder->initiatePayment();
    }

    public function callback()
    {
        return $this->builder->paymentCallback();
    }
}