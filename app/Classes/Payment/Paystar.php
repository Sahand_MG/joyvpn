<?php
namespace App\Classes\Payment;
use App\Classes\AdminNotifier;
use App\Classes\AppDB;
use App\Classes\Constants;
use App\Classes\Keyboards;
use App\Classes\PlusDB;
use App\Classes\ReceiptMaker;
use App\Jobs\SendTelegramNotif;
use App\Models\User;
use App\Services\AffiliateService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Morilog\Jalali\Jalalian;
use Spatie\Emoji\Emoji;


class Paystar {

    public $amount;
    public $remark;
    public $type;
    public $fqdn;
    public $vol;
    public $chatId;
    public $msg;
    public $has_map;

    public function __construct($amount, $remark, $type, $fqdn, $vol = 0, $has_map = 0)
    {
        $this->amount = $amount * 10; // in Rials
        $this->remark = $remark;
        $this->type = $type;
        $this->fqdn = $fqdn;
        $this->vol = $vol;
        $this->has_map = $has_map;
    }

    public function initiatePayment()
    {
        list($result, $err) = $this->_sendPaymentData();
        Log::info('init payment log');
        Log::info(json_encode($result));
        if ($err) {
            return 404;
        } elseif(!is_null($result)) {
            if (isset($result["status"]) && $result["status"] == 1) {
                $trans = $this->_createTransactionRecord($this->amount, $result['data']);
                return [$result['data'], $trans];
            } else {
                $msg = [
                    'chat_id' => app('extractor')->chat_id,
                    'text' => 'اشکالی در پرداخت پیش آمده. مجدد تلاش کنید',
                ];
                SendTelegramNotif::dispatch($msg);
                exit();
            }
        } else {
            $msg = [
                'chat_id' => app('extractor')->chat_id,
                'text' => 'اشکالی در پرداخت پیش آمده',
            ];
            SendTelegramNotif::dispatch($msg);
            exit();
        }

    }

    private function _sendPaymentData(): array
    {
        $callback = config('bot.payment_callback_renew_naps_paystar').'?token=' . base64_encode($this->fqdn . '&' . $this->vol. '&'. $this->has_map). '&type=renew';
        $orderId = 'pay_'.random_int(1000,300000).Str::random(5);
        $toSign = $this->amount.'#'.$orderId.'#'.$callback;
        $data = array(
            // 'type' => $this->type,
            'amount' => $this->amount,
            'order_id' => $orderId,
            'callback' => $callback,
            'callback_method' => 1,
            'sign' => hash_hmac('sha512', $toSign, env('GATEWAY_KEY')),
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://core.paystar.ir/api/pardakht/create');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer '.env('PAYSTAR_TOKEN'),
            'Content-Length: ' . strlen($jsonData)
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return array($result, $err);
    }

    private function _createTransactionRecord($amount, $result)
    {
        try {
            $data = app('extractor');
            $trans = new Transaction();
            $trans->trans_id = $result['order_id'];
            $trans->status = 'unpaid';
            $trans->amount = $amount;
            $trans->authority = $result['ref_num'];
            $trans->remark = is_null($this->remark) ? 'new' : $this->remark;
            $trans->type = $this->type;
            $trans->user_id = User::where('chat_id', $data->chat_id)->first()->id ?? 1;
            $trans->chat_id = $data->chat_id;
            $trans->plan_id = 1;
            $trans->username = $data->username;
            $trans->save();
            $this->trans = $trans;
            return $trans;
        } catch (\Exception $e) {
            info($e->getMessage(). ' at paystar create trans');
            exit();
        }

    }

    public function paymentCallback($data)
    {
        $trans = Transaction::query()->where('trans_id', $data['order_id'])->first();
        if (is_null($trans)) {
            'کدتراکنش نادرست است. درصورتی که قبلا این تراکنش را پرداخت نکرده‌اید، مجددا از ربات درخواست تمدید اکانت بدهید';
        }
        if ($trans->status == 'paid' || $trans->status == 'canceled') {
            return 'تراکنش پیش از این پردازش شده است';
        }
        if ($data['status'] == 1) {
            info('verifying the transaction');
            list($result, $err) = $this->_sendVerificationRequest($trans, $data);
            Log::info($result);
            if ($err) {
                return "cURL Error #:" . $err;
            } else {
                if ($result['status'] == '1') {
                    $trans->update([
                        'meta' => serialize($result)
                    ]);
                    $this->_confirmPayment($trans);
                    return redirect()->route('RemotePaymentSuccess', ['transid' => $trans->trans_id]);

                } else {
                    $trans->update(['status' => 'canceled']);
                    $char = Emoji::redCircle();
                    $msg = [
                        'chat_id' => $trans->chat_id,
                        'text' => " $char $char پرداخت شما ناموفق بود "
                            . "\n" .
                            ' شماره تراکنش : ' . $trans->trans_id
                        ,
                        'reply_markup' => app('bot')->buildKeyBoard(Keyboards::glassyKeyboard()),
                    ];
                    SendTelegramNotif::dispatch($msg);
                    AdminNotifier::orderCanceled($trans);
                    return redirect()->route('RemotePaymentCanceled', ['transid' => $trans->trans_id]);
                }
            }

        } else {
            $trans->update(['status' => 'canceled']);
            $char = Emoji::redCircle();
            $msg = [
                'chat_id' => $trans->chat_id,
                'text' => " $char $char پرداخت شما ناموفق بود "
                    . "\n" .
                    ' شماره تراکنش : ' . $trans->trans_id
                ,
                'reply_markup' => app('bot')->buildKeyBoard(Keyboards::glassyKeyboard()),
            ];
            SendTelegramNotif::dispatch($msg);
            AdminNotifier::orderCanceled($trans);
            return redirect()->route('RemotePaymentCanceled', ['transid' => $trans->trans_id]);
        }
    }

    private function _sendVerificationRequest($trans, $data): array
    {
        $toSign = $trans->amount.'#'.$trans->authority.'#'.$data['card_number'].'#'.$data['tracking_code'];
        $data = array('MerchantID' => env('PAYSTAR_TOKEN'),
            'amount' => $trans->amount,
            'ref_num' => $trans->authority,
            'sign' => hash_hmac('sha512', $toSign, env('GATEWAY_KEY')),
        );
        info('data to verify');
        Log::info($data);
        $jsonData = json_encode($data);
        $ch = curl_init('https://core.paystar.ir/api/pardakht/verify');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer '.env('PAYSTAR_TOKEN'),
            'Content-Length: ' . strlen($jsonData)
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return array($result, $err);
    }

    private function _confirmPayment($trans)
    {
        $trans->update([
            'status' => 'paid'
        ]);
        $type = $trans->type;
        $date = Jalalian::fromCarbon(Carbon::parse($trans->created_at))->format('%B %d، %Y');
        if ($type == Constants::EXP) {
            $resp = $this->_sendExpiryUpdateRequest($trans);
            $expiry_date = Jalalian::fromCarbon(Carbon::createFromTimestampMs($resp->data->expiry_time))->format('%B %d، %Y');
            $total_vol = $resp->data->total/pow(10, 9);
            $receipt = (new ReceiptMaker($trans->trans_id, $trans->amount, $date, $trans->remark, $expiry_date, $total_vol, $trans->status))
                ->makeForUser()
                ->addExpDate()
                ->addStatus()
                ->addRemark()
                ->addTotalVol()
                ->getMsg();
            $msg = [
                'chat_id' => $trans->chat_id,
                'text' => $receipt,
                'parse_mode' => 'HTML',
            ];
            SendTelegramNotif::dispatch($msg);
            // add affiliate credit
            AffiliateService::addAffiliateCredit($trans);
            $msg['text'] = Constants::ADV_MESSAGE;
            $msg['parse_mode'] = 'Markdown';
            SendTelegramNotif::dispatch($msg);
        } elseif ($type == Constants::VOL) {
            $resp = $this->_sendVolRequest($trans->remark);
            $expiry_date = Jalalian::fromCarbon(Carbon::createFromTimestampMs($resp->data->expiry_time))->format('%B %d، %Y');
            $total_vol = $resp->data->total/pow(10, 9);
            $receipt = (new ReceiptMaker($trans->trans_id, $trans->amount, $date, $trans->remark, $expiry_date, $total_vol, $trans->status))
                ->makeForUser()
                ->addExpDate()
                ->addStatus()
                ->addRemark()
                ->addTotalVol()
                ->getMsg();
            $msg = [
                'chat_id' => $trans->chat_id,
                'text' => $receipt,
                'parse_mode' => 'HTML',
            ];
            SendTelegramNotif::dispatch($msg);
        }
        AdminNotifier::orderAcceptedRenew($trans);
    }

    private function _sendExpiryUpdateRequest($trans)
    {
        if ($this->fqdn == '0.0.0.0') {
            $resp = AppDB::renewUserName($trans->remark);
            if ($resp['status'] != 200) {
                $msg = [
                    'chat_id' => $trans->chat_id,
                    'text' => 'عملیات با خطا مواجه شد',
                    'parse_mode' => 'HTML',
                ];
                SendTelegramNotif::dispatch($msg);
                exit();
            }
            $expiry_date = Jalalian::now()->addMonths(1)->format('%B %d، %Y');
            $acc = AppDB::getUser($trans->remark);
            $total_vol = $acc->base_vol;
            $date = Jalalian::fromCarbon(Carbon::parse($trans->created_at))->format('%B %d، %Y');
            // add affiliate credit
            AffiliateService::addAffiliateCredit($trans);
            $receipt = (new ReceiptMaker($trans->trans_id, $trans->amount, $date, $trans->remark, $expiry_date, $total_vol, $trans->status, '', 'android'))
                ->makeForUser()
                ->addExpDate()
                ->addStatus()
                ->addRemark()
                ->addOs()
                ->addTotalVol()
                ->getMsg();
            $msg = [
                'chat_id' => $trans->chat_id,
                'text' => $receipt,
                'parse_mode' => 'HTML',
            ];
            SendTelegramNotif::dispatch($msg);
            $msg['text'] = Constants::ADV_MESSAGE;
            $msg['parse_mode'] = 'Markdown';
            SendTelegramNotif::dispatch($msg);
            AdminNotifier::orderAcceptedRenew($trans);
            redirect()->route('RemotePaymentSuccess', ['transid' => $trans->trans_id])->throwResponse();
            exit();
        }
        if ($this->has_map == 1) {
            PlusDB::sendUpdateRequest($trans->remark);
            PlusDB::renewIpDateUsageAndExp($trans->remark);
            $acc = PlusDB::getSubAccount2($trans->remark);
            $expiry_date = Jalalian::now()->addMonths(1)->format('%B %d، %Y');
            $total_vol = $acc->total;
            $date = Jalalian::fromCarbon(Carbon::parse($trans->created_at))->format('%B %d، %Y');
            // add affiliate credit
            AffiliateService::addAffiliateCredit($trans);
            $receipt = (new ReceiptMaker($trans->trans_id, $trans->amount, $date, $trans->remark, $expiry_date, $total_vol, $trans->status, '', 'ios'))
                ->makeForUser()
                ->addExpDate()
                ->addStatus()
                ->addOs()
                ->addRemark()
                ->addTotalVol()
                ->getMsg();
            $msg = [
                'chat_id' => $trans->chat_id,
                'text' => $receipt,
                'parse_mode' => 'HTML',
            ];
            SendTelegramNotif::dispatch($msg);
            $msg['text'] = Constants::ADV_MESSAGE;
            $msg['parse_mode'] = 'Markdown';
            SendTelegramNotif::dispatch($msg);
            AdminNotifier::orderAcceptedRenew($trans);
            redirect()->route('RemotePaymentSuccess', ['transid' => $trans->trans_id])->throwResponse();
            exit();
        }
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/expiry' : $this->fqdn . '/api/expiry';
        $resp = $this->sendHttp($address, ['remark' => $trans->remark, 'agent' => 'admin']);
        if ($resp->status != 200) {
            $this->msg = 'حساب یافت نشد';
        } else {
            $this->msg = 'Done';
        }
        return $resp;
    }

    private function _sendVolRequest($remark)
    {
        $address = env('APP_ENV') == 'local' ? 'http://127.0.0.1:8000/api/vol' : $this->fqdn . '/api/vol';
        $resp = $this->sendHttp($address, ['remark' => $remark, 'vol' => $this->vol]);
        if ($resp->status != 200) {
            $this->msg = 'حساب یافت نشد';

        } else {
            $this->msg = 'Done';
        }
        return $resp;
    }

    private function sendHttp($server_address, array $data)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $server_address,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
        ));
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            info(json_encode($response));
            $msg = [
                'chat_id' => $this->chatId,
                'text' => 'خطای node'
            ];
            SendTelegramNotif::dispatch($msg);
            AdminNotifier::reportError($server_address);
            exit();
        }
        curl_close($curl);
        info(json_encode($response));
        return json_decode($response);
    }
}
