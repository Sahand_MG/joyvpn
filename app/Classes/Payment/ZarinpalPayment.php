<?php

namespace App\Classes\Payment;

use App\Classes\AdminNotifier;
use App\Classes\Builders\RenewAccount;
use App\Classes\CacheKeys;
use App\Classes\Constants;
use App\Classes\Extractor\AbstractExtractor;
use App\Classes\Keyboards;
use App\Classes\ReceiptMaker;
use App\Jobs\SendTelegramNotif;
use App\Models\Accounts;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Morilog\Jalali\Jalalian;
use Spatie\Emoji\Emoji;

class ZarinpalPayment extends AbstractPayment
{
    public $amount;

    public function __construct($amount = 0)
    {
        $this->amount = $amount;
    }

    public function initiatePayment()
    {
        list($amount, $user, $result, $err) = $this->_sendPaymentData();
        if ($err) {
            return 404;
        } else {
            if (isset($result["Status"]) && $result["Status"] == '100' ) {
                return $this->_createTransactionRecord($amount,$result,$user);
            } else {
                return 'اشکالی در پرداخت پیش آمده';
            }
        }

    }

    public function paymentCallback()
    {
        $transactionId = request('Authority');
        $trans = Transaction::where('authority', $transactionId)->first();
        if (is_null($trans)) {
            return 'کد تراکنش نادرست است';
        }
        if ($trans->status == 'paid' || $trans->status == 'canceled') {
            return 'تراکنش تکراری است';
        }
        $data = array('MerchantID' => env('ZARRIN_TOKEN'), 'Authority' => $transactionId, 'Amount' => $trans->amount);
        list($result, $err) = $this->_sendVerificationRequest($data);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {

            if ($result['Status'] == '100') {
                $this->_confirmPayment($trans);
                return redirect()->route('RemotePaymentSuccess', ['transid' => $trans->trans_id]);

            } else {
                $trans->update(['status' => 'canceled']);
                $char = Emoji::redCircle();
                $msg = [
                    'chat_id' => $trans->chat_id,
                    'text' => " $char $char پرداخت شما ناموفق بود "
                        ."\n".
                        ' شماره تراکنش : '.$trans->trans_id
                    ,
                    'reply_markup' => app('bot')->buildKeyBoard(Keyboards::glassyKeyboard()),
                ];
                SendTelegramNotif::dispatch($msg);
                AdminNotifier::orderCanceled($trans);
                return redirect()->route('RemotePaymentCanceled', ['transid' => $trans->trans_id]);
            }
        }
    }

    private function _sendPaymentData():array
    {
        $amount = $this->amount;
        $chat_id = app('extractor')->chat_id;
        $callback = config('bot.wallet_payment_callback');
        $user = User::query()->where('chat_id', $chat_id)->first();
        $data = array('MerchantID' => env('ZARRIN_TOKEN'),
            'Amount' => $amount,
            'CallbackURL' => $callback,
            'Description' => 'خرید سرویس شبکه شخصی مجازی');
        $jsonData = json_encode($data);
        $ch = curl_init('https://www.zarinpal.com/pg/rest/WebGate/PaymentRequest.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return array($amount, $user, $result, $err);
    }

    private function _createTransactionRecord($amount, $result, $user)
    {
        $trans = new Transaction();
        $trans->trans_id = 'Zarrin_' . strtoupper(uniqid());
        $trans->status = 'unpaid';
        $trans->amount = $amount;
        $trans->authority = $result['Authority'];
        $trans->remark = 'wallet';
        $trans->type = Constants::WALLET;
        $trans->user_id = User::where('chat_id', $user->chat_id)->first()->id;
        $trans->chat_id = $user->chat_id;
        $trans->plan_id = 1;
        $trans->username = @$user->username;
        $trans->save();
        $this->trans = $trans;
        return $trans;
    }

    private function _sendVerificationRequest($data):array
    {
        $jsonData = json_encode($data);
        $ch = curl_init('https://www.zarinpal.com/pg/rest/WebGate/PaymentVerification.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return array($result, $err);
    }

    private function _confirmPayment($trans)
    {
        $wallet = $trans->user->wallet;
        $to_charge = 0;
        if (config('bot.combo_wallet')) {
            $ratio = config('bot.combo_wallet_ratio');
            $to_charge = $trans->amount * $ratio;
            $wallet->update(['amount' => $wallet->amount + $to_charge ]);
        } else {
            $to_charge = $trans->amount;
            $wallet->update(['amount' => $wallet->amount + $to_charge]);
        }
        $trans->update([
            'status' => 'paid'
        ]);
        $date = $date = Jalalian::fromCarbon(Carbon::parse($trans->created_at))->format('%B %d، %Y');
        $receipt = (new ReceiptMaker($trans->trans_id, $to_charge, $date, '', '', '', $trans->status, '', '', Constants::WALLET))
            ->makeForUser()
            ->addWalletFund($wallet->amount)
            ->getMsg();
        $msg = [
            'chat_id' => $trans->chat_id,
            'text' => $receipt,
            'parse_mode' => 'HTML',
        ];
        SendTelegramNotif::dispatch($msg);
        AdminNotifier::adminWalletCharge($trans->amount);
    }
}
