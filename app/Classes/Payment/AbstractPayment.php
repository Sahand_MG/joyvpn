<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 2:44 PM
 */

namespace App\Classes\Payment;


use App\Classes\Extractor\AbstractExtractor;

abstract class AbstractPayment
{
    public $price;
    public $data;
    public $plan;
    public $trans;
    public $renew = 0;

    abstract public function initiatePayment();

    abstract public function paymentCallback();
}