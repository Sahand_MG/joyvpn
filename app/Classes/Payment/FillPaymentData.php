<?php
/**
 * Created by PhpStorm.
 * User: Sahand
 * Date: 6/9/21
 * Time: 4:19 PM
 */

namespace App\Classes\Payment;


use App\Classes\CacheKeys;
use App\Classes\Constants;
use Illuminate\Support\Facades\Cache;

trait FillPaymentData
{
//    Used in Plan Classes
    public function fillPaymentData($plan,$data,$price,$renew)
    {
        switch (config('bot.payment_service')){
            case Constants::ZARINPAL:
                $director = (new PaymentDirector(new ZarinpalPayment()));
                break;
            case Constants::PAYSTAR:
                $director = (new PaymentDirector(new PaystarPayment()));
                break;
            default:
                $director = (new PaymentDirector(new ZarinpalPayment()));
                break;
        }
        $director->setPlan($plan);
        $director->setData($data);
        $director->setPrice($price);
        $director->setRenew($renew);
        Cache::forever(CacheKeys::getPaymentKey(),$director);
    }
}