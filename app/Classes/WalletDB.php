<?php

namespace App\Classes;


use App\Models\User;
use App\Models\Wallet;

class WalletDB
{
    public static function createWalletRecord($user)
    {
        return Wallet::query()->create([
            'amount' => 0,
            'user_id' => $user->id,
            'is_active' => true,
        ]);
    }

    public static function getUserWallet($chat_id)
    {
        $user = User::query()->where('chat_id', $chat_id)->first();
        return is_null($user) ? null : $user->wallet;
    }
}
