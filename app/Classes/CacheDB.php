<?php


namespace App\Classes;


use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class CacheDB
{
    public static function initiateStack($chatId)
    {
        Cache::put($chatId, [], 3600);
    }

    public static function addToStack($chatId, string $data)
    {
        $stack = Cache::get($chatId);
        $stack[] = $data;
        Cache::put($chatId, $stack, 3600);
    }

    public static function clearStack($chatId)
    {
        self::initiateStack($chatId);
    }

    public static function getStack($chatId): array
    {
        return Cache::get($chatId) ?? [];
    }

    public static function setMalformedServer($server_id)
    {
        $date = Carbon::now()->format('Y-m-d H:i:s');
        Cache::forever('ping.'.$server_id, $date);
    }

    public static function getPings($server_id)
    {
        return Cache::get('ping.'.$server_id);
    }

    public static function removeFromPing($url)
    {
        $urls = Cache::get('ping');
        unset($urls[$url]);
        Cache::forever('ping', $urls);
    }

    public static function setCompensatedAccounts($remark)
    {
        $cp = Cache::get('compensated.1');
        if (is_null($cp)) {
            Cache::forever('compensated.1', [$remark]);
        } else {
            $tmp = Cache::get('compensated.1');
            $tmp[] = $remark;
            Cache::forever('compensated.1', $tmp);
        }
    }

    public static function getCompensated()
    {
        return Cache::get('compensated.1');
    }
}