<?php


namespace App\Classes;


class ReceiptMaker
{
    public $transId;
    public $amount;
    public $date;
    public $expDate;
    public $vol;
    public $remark;
    public $status;
    public $cardNumber;
    public $os;
    public $msg = '';
    public $type = '';

    public function __construct($transId, $amount, $date, $remark = '' ,$expDate = '', $vol = 0, $status, $cardNumber = '', $os = '' , $type = 'تمدید')
    {
        $this->transId = $transId;
        $this->amount = rialsCheck($amount, $transId);
        $this->date = $date;
        $this->expDate = $expDate;
        $this->vol = $vol;
        $this->remark = $remark;
        $this->status = $status;
        $this->cardNumber = $cardNumber;
        $this->os = $os;
        $this->type = $type;
    }

    public function makeForUser()
    {
        $this->msg =  "✅ کد تراکنش : $this->transId \n".
            "💶 مبلغ پرداخت شده :  تومان $this->amount \n".
            "📆 تاریخ : $this->date \n";
        return $this;
    }

    public function makeForChargeWallet()
    {
        $this->msg = " شارژ کیف پول"."\n".
            "✅ کد تراکنش : $this->transId \n".
            "💶 مبلغ پرداخت شده :  تومان $this->amount \n".
            "📆 تاریخ : $this->date \n";
        return $this;
    }

    public function makeForExpExtra()
    {
        $this->msg = "شارژ اعتبار اکانت"."\n".
            "💶 تعداد روز اضافه شده: $this->amount \n".
            "📆 تاریخ انقضای جدید: $this->expDate \n";
        return $this;

    }

    public function makeForPayWithWallet()
    {
        $this->msg = " پرداخت از کیف پول"."\n".
            "✅ کد تراکنش : $this->transId \n".
            "💶 مبلغ پرداخت شده :  تومان $this->amount \n".
            "📆 تاریخ : $this->date \n";
        return $this;
    }

    public function makeForAdmin()
    {
        $this->msg =  "✅ کد تراکنش : $this->transId \n".
                "💶 مبلغ پرداخت شده :  تومان $this->amount \n".
                "📆 تاریخ : $this->date \n";
        return $this;
    }

    public function addWalletFund($fund)
    {
        $this->msg .= "💵 موجودی کیف پول :  تومان $fund \n";
        return $this;
    }

    public function addExpDate()
    {
        $this->msg .= "📆 تاریخ انقضا: $this->expDate \n";
        return $this;
    }

    public function addTotalVol()
    {
        $total_vol = $this->vol;
        $this->msg .= "🔋حجم کل: $total_vol گیگ "."\n";
        return $this;
    }

    public function addRemark()
    {
        $this->msg .= "🦸نام کاربری : $this->remark \n";
        return $this;
    }

    public function addType()
    {
        $this->msg .= "⚙️نوع : $this->type \n";
        return $this;
    }

    public function addCardNumber()
    {
        $this->msg .= "💰شماره کارت :$this->cardNumber \n";
        return $this;
    }

    public function addStatus()
    {
        $this->msg .= "🦠وضعیت: $this->status \n";
        return $this;
    }

    public function addOs()
    {
        $this->msg .= "سیستم عامل: 📱$this->os \n";
        return $this;
    }

    public function getMsg()
    {
        return $this->msg;
    }
}
