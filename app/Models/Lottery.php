<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lottery extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function participants()
    {
        return $this->hasMany(LotteryParticipants::class);
    }
}
