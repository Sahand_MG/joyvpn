<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $fillable = ['status','account_id', 'meta'];
    public function plan(){

        return $this->belongsTo(Plan::class);
    }

    public function account(){

        return $this->belongsTo(Accounts::class);
    }

    public function user(){

        return $this->belongsTo(User::class);
    }
}
