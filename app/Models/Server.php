<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $guarded = [];
    public function scopeUp($q)
    {
        return $q->where('status','up');
    }
}
