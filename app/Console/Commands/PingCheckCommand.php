<?php

namespace App\Console\Commands;

use App\Classes\CacheDB;
use App\Classes\Constants;
use App\Jobs\SendTelegramNotif;
use App\Services\HttpService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class PingCheckCommand extends Command
{

    protected $signature = 'check:ping';
    protected $description = 'Check each server status';
    public $urls;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
//        $pings = CacheDB::getPings(); // [id => timestamp]
        $all_servers = Constants::IRAN_SERVERS;
        foreach ($all_servers as $server_id => $url) {
            $date = CacheDB::getPings($server_id);
            if (!is_null($date) && Carbon::now()->diffInMinutes(Carbon::parse($date)) >= 10) { // means that the server has not introduced it self after 8 min
                $txt = '⚠️  عدم احراز '. $server_id;
                $this->sendTgNotif($txt, Constants::ADMIN_CHAT_ID2);
//                CacheDB::removeFromPing($url);
//                $txt = " ✅ احراز سرور $url رفع شد. ";
//                $this->sendTgNotif($txt);
            }
        }
    }

    public function sendHttp($method = 'GET', $url,  $id, array $headers = [])
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 7,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS => [],
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
        ));
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            list($this->urls, $should_notify) = CacheDB::setMalformedServer($id);
            if ($should_notify) {
                $txt = '⚠️ اختلال روی سرور '. $id;
                $this->sendTgNotif($txt, Constants::ADMIN_CHAT_ID);
            }
        }
        curl_close($curl);
        return json_decode($response, true);
    }

    public function sendTgNotif($txt, $chat_id)
    {
        $msg = [
            'chat_id' => $chat_id,
            'text' => $txt
        ];
        SendTelegramNotif::dispatch($msg);
    }
}
