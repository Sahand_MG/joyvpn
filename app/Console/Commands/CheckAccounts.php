<?php

namespace App\Console\Commands;


use App\Classes\AdminNotifier;
use App\Classes\TelegramHelper;
use App\Models\Accounts;
use App\Models\Plan;
use App\Models\Server;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Spatie\Emoji\Emoji;


class CheckAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:account';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->expire();
        $this->reminder_week();
        $this->reminder_day();

    }
    private function expire()
    {
        $accounts = Accounts::where('used', 1)->get();
        $servers = Server::where('status', 'up')->get();
        foreach ($accounts as $account) {
            $target = Carbon::parse($account->expires_at);
            if (Carbon::now()->diffInHours($target) == 0 && Carbon::now() > $target) {
                foreach ($servers as $server) {
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $server->region.':9090?id='.$account->username,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                    ));
                    $response = curl_exec($curl);
                    curl_close($curl);
                }
                AdminNotifier::expireAlarm($account);
                $this->sendExpirationNotif($account);
            }
        }
    }
// send a week before expiration
        private function reminder_week(){

            $accounts = Accounts::where('used',1)->where('expires_at','>',Carbon::now())->with(['plan','transactions'])->get();
            foreach ($accounts as $account){
                $trans = $account->transactions->first();
                if(!is_null($trans)){
                    $plan = $trans->plan;
                    $target_date = Carbon::parse($account->expires_at);
                    $diff = Carbon::now()->diffInHours($target_date);
                    if(Carbon::now() < $target_date && $diff == 168 ){

//                    ============== Sending Notifications ===============
                        AdminNotifier::reminderAlarm($account);
                        $textMsg =  'یادآوری تمدید حساب JV.'."\n"
                            . ' کاربر گرامی، تنها ۷ روز از اعتبار حساب شما باقی مانده است.'."\n"
                            .'جهت تمدید حساب با نام کاربری '.$account->username."\n"
                            . ' با قیمت ' . $trans->plan->price . ' تومان '."\n"
                            .' به ربات تلگرام مراجعه کنید ';
                    $this->sendTelegramReminderNotif($account,$textMsg);

//                    ============== END ===============
                    if(!is_null($trans->user->email)){
                        $this->_sendMailNotif($target_date, $account, $trans, $plan);
                    }else{
//                        $this->_sendSmsNotif($trans, $textMsg);
                    }
                 }
                }
            }

        }

        private function reminder_day(){

            $accounts = Accounts::where('used',1)->where('expires_at','>',Carbon::now())->with(['plan','transactions'])->get();
            foreach ($accounts as $account){
                $trans = $account->transactions->where('status','paid')->sortByDesc('created_at')->first();
                if(!is_null($trans)) {
                    $plan = $trans->plan;
                    $target_date = Carbon::parse($account->expires_at);
                    $diff = Carbon::now()->diffInHours($target_date);
                    if (Carbon::now() < $target_date && $diff == 24) {
//
//                    ============== Sending Notifications ===============
                        AdminNotifier::reminderAlarm($account);
                        $textMsg =  'یادآوری تمدید حساب JV.'."\n"
                            . ' کاربر گرامی، تنها ۱ روز از اعتبار حساب شما باقی مانده است.'."\n"
                            .'جهت تمدید حساب با نام کاربری '.$account->username."\n"
                            . ' با قیمت ' . $trans->plan->price . ' تومان '."\n"
                            .' به ربات تلگرام مراجعه کنید ';
//                        . "http://pay.joyvpn.xyz$link->abbr";

                    $this->sendTelegramReminderNotif($account,$textMsg);

//                    ============== END ===============
                    if (!is_null($trans->user->email)) {
                        $this->_sendMailNotif($target_date, $account, $trans, $plan);
                    } else {
//                        $this->_sendSmsNotif($trans, $textMsg);
                    }
                }
                }
            }

        }

//    Users Notification

        private function sendTelegramReminderNotif($account,$textMsg){

            $telegram = new TelegramHelper(env('BOT_TOKEN'));
            $msg = [
                'chat_id' => $account->user_id,
                'text' => $textMsg,
                'parse_mode' => 'HTML',
            ];
            $telegram->sendMessage($msg);
        }
        private function sendExpirationNotif($account){

            $telegram = new TelegramHelper(env('BOT_TOKEN'));
            $msg = [
                'chat_id' => $account->user_id,
                'text' => Emoji::redCircle()." حساب شما با نام کاربری $account->username منقضی شده است. ".Emoji::redCircle(),
                'parse_mode' => 'HTML',
            ];
            $telegram->sendMessage($msg);

        }

    private function _sendMailNotif($target_date, $account, $trans, $plan):void
    {
        $account->expires_at = \Morilog\Jalali\Jalalian::fromCarbon($target_date)->format('%d %B %Y');
        Mail::send('reminder', ['account' => $account, 'trans' => $trans, 'plan' => $plan], function ($message) use ($trans) {
            $message->to($trans->user->email);
            $message->subject('یادآوری تمدید حساب');
        });
    }

    private function _sendSmsNotif($trans, $textMsg):void
    {
        $api = new \Kavenegar\KavenegarApi(env('SMS'));
        $sender = "10008000800600";
        $receptor = array($trans->phone);
        $message = $textMsg;
        $api->Send($sender, $receptor, $message);
    }
}
