<?php

namespace App\Jobs;

use App\Classes\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendTelegramNotif implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $msg;
    public $type;

    public function __construct($message,$type = 'message')
    {
        $this->msg = $message;
        $this->type = Constants::MESSAGE_TYPES[$type];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        TODO Log is here
        Log::alert(app('bot')->{$this->type}($this->msg));
    }
}
