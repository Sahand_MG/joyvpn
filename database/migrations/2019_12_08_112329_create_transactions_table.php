<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amount');
            $table->string('authority');
            $table->string('username')->nullable();
            $table->string('remark')->nullable();
            $table->string('type')->nullable();
            $table->string('service');
            $table->string('status')->default('unpaid');
            $table->string('trans_id');
            $table->string('chat_id');
            $table->tinyInteger('plan_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('account_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
