<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotteryParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lottery_participants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chat_id');
            $table->string('prize');
            $table->unsignedInteger('lottery_id');
            $table->boolean('is_done')->default(false);
            $table->foreign('lottery_id')->references('id')->on('lotteries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lottery_participants');
    }
}
