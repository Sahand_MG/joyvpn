<?php

namespace Database\Factories;

use App\Models\Accounts;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccountsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Accounts::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return   [
            'username' => $this->faker->name,
            'password' => rand(1000, 20000),
            'plan_id' => rand(1, 2),
            'used' => 1,
            'user_id' => User::query()->inRandomOrder()->first()->id,
            'expires_at' => Carbon::now()->addMinutes(10)
        ];
    }
}
