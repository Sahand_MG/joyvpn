<?php

use App\Classes\Constants;
use Spatie\Emoji\Emoji;

return [
    'welcome' => 'به JoyVpn خوش آمدید لطفا طرح مورد نظرتون رو انتخاب کنید',
    'choose' => 'لطفا طرح مورد نظرتون رو انتخاب کنید',
    \App\Classes\Constants::ACCOUNT1M_TEXT    => ' حساب ۱ ماهه ios مخصوص سیسکو',
    \App\Classes\Constants::ACCOUNT1M_PC_TEXT    => ' حساب ۱ ماهه pc مخصوص سیسکو',
    \App\Classes\Constants::ACCOUNT3M2U_TEXT => ' حساب ۲ ماهه اقتصادی  ',
    \App\Classes\Constants::ACCOUNT3M10U_TEXT => ' حساب ۲ ماهه شرکتی  ',
    \App\Classes\Constants::ACCOUNT3M5U_TEXT => ' حساب ۲ ماهه خانوادگی  ',
    \App\Classes\Constants::FREE_TEXT   => ' دریافت حساب رایگان تست ',
    'register' => 'ثبت نام‌کاربری',
    Constants::RENEW_NAPS_TEXT => 'تمدید اکانت نپسترن',
    'refused' => 'انصراف',
    'tutorials' => 'آموزش اتصال و دانلود',
    'forward_to_gateway' => 'هدایت به درگاه پرداخت',
    'forward_to_wallet' => 'پرداخت از کیف پول',
    'canceled' => 'درخواست لغو شد',
    'transactions' => 'لیست تراکنش‌ها',
    'servers' => 'لیست سرورها',
    'support' => 'تماس با پشتیبانی',
    'restart' => 'شروع مجدد',
    'renew'   => 'تمدید اکانت',
    'no_unpaid_transaction' => 'تراکنش در انتظار پرداخت وجود ندارد',
    'clients' => 'نرم افزارها',
    'channel' => 'عضویت در کانال'
];